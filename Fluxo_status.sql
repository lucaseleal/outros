--Evolutivo
select lr.loan_request_id
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) and lr.loan_request_id not in (
	select lr.loan_request_id
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	join (select distinct status_timeline."Emprestimo" from backtests.status_timeline where status_timeline."StatusSaida" in ('Alçada 1','ACCEPTED','REJECTED','Cancelado')) tl_p on lr.loan_request_id = tl_p."Emprestimo"
	group by 1) and to_char(dp.opt_in_date,'Mon/yy') in ('Nov/18','Dez/18')

--Evolutivo
select dp.direct_prospect_id
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) and lr.loan_request_id not in (
	select dp.direct_prospect_id
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	join (select distinct status_timeline_lead."Lead" from backtests.status_timeline where status_timeline."StatusSaida" in ('Alçada 1','ACCEPTED','REJECTED','Cancelado')) tl_p on lr.loan_request_id = tl_p."Emprestimo"
	group by 1) and to_char(dp.opt_in_date,'Mon/yy') in ('Nov/18','Dez/18')

select * 
from backtests.status_timeline
where "Emprestimo" = 8027
	
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*)
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select distinct status_timeline."Emprestimo" from backtests.status_timeline where status_timeline."StatusSaida" in ('Alçada 1','ACCEPTED','REJECTED','Cancelado')) tl_p on lr.loan_request_id = tl_p."Emprestimo"
group by 1


