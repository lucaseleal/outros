select
	'analytics' as fonte,
	t11.*,
	case 
		when greatest(renovacao,filtro_pedido_afiliado,filtro_serasa_score_0_1,filtro_canais_esteiras) > 0 then 'Analise Manual'
		when faixa_v21 > 5 then 'Rejeitar'
		when faixa_v21 <= 5 then case
			when faturamento_comprovado > 200000 then 'Aprovar'
			when faturamento_comprovado <= 200000 then case
				when faturamento_comprovado / faturamento_informado < .2 then 'Rejeitar'
				when faturamento_comprovado / faturamento_informado < .4 then 'Aprovar com alteracao'
				when faturamento_comprovado / faturamento_informado >= .4 then 'Aprovar'
				else 'Analise Manual'
			end
			else 'Analise Manual'
		end
		else 'Analise Manual'
	end as decisao,
	case 
		when greatest(renovacao,filtro_pedido_afiliado,filtro_serasa_score_0_1,filtro_canais_esteiras) > 0 then null
		when faixa_v21 > 5 then null
		when faixa_v21 <= 5 then case
			when faturamento_comprovado > 200000 then valor_solicitado
			when faturamento_comprovado <= 200000 then case
				when faturamento_comprovado / faturamento_informado < .2 then null
				when faturamento_comprovado / faturamento_informado < .4 then valor_solicitado * .5
				when faturamento_comprovado / faturamento_informado >= .4 then valor_solicitado
				else null
			end
			else null
		end
		else null
	end as limite,
	case 
		when greatest(renovacao,filtro_pedido_afiliado,filtro_serasa_score_0_1,filtro_canais_esteiras) > 0 then null
		when faixa_v21 > 5 then null
		when faixa_v21 <= 5 then case
			when faturamento_comprovado > 200000 then interest_rate
			when faturamento_comprovado <= 200000 then case
				when faturamento_comprovado / faturamento_informado < .2 then null
				when faturamento_comprovado / faturamento_informado < .4 then interest_rate
				when faturamento_comprovado / faturamento_informado >= .4 then interest_rate
				else null
			end
			else null
		end
		else null
	end as taxa,
	case 
		when greatest(renovacao,filtro_pedido_afiliado,filtro_serasa_score_0_1,filtro_canais_esteiras) > 0 then null
		when faixa_v21 > 5 then null
		when faixa_v21 <= 5 then case
			when faturamento_comprovado > 200000 then prazo_solicitado
			when faturamento_comprovado <= 200000 then case
				when faturamento_comprovado / faturamento_informado < .2 then null
				when faturamento_comprovado / faturamento_informado < .4 then prazo_solicitado
				when faturamento_comprovado / faturamento_informado >= .4 then prazo_solicitado
				else null
			end
			else null
		end
		else null
	end as prazo
from(select
		t10.*,
	    CASE
	        WHEN round(prob_v21, 10) <= 0.16497855234668 THEN 1
	        WHEN round(prob_v21, 10) <= 0.215456199777354 THEN 2
	        WHEN round(prob_v21, 10) <= 0.259132970195966 THEN 3
	        WHEN round(prob_v21, 10) <= 0.307395130162076 THEN 4
	        WHEN round(prob_v21, 10) <= 0.348312135028167 THEN 5
	        WHEN round(prob_v21, 10) <= 0.393488415229748 THEN 6
	        WHEN round(prob_v21, 10) <= 0.460176099805381 THEN 7
	        WHEN round(prob_v21, 10) <= 0.506865123542299 THEN 8
	        WHEN round(prob_v21, 10) <= 0.585319253213502 THEN 9
	        WHEN round(prob_v21, 10) > 0.585319253213502 THEN 10
	        ELSE 99
	    END AS faixa_v21
	from(select
			t9.*,
		    exp(equacao) / (1::numeric + exp(equacao))::numeric AS prob_v21,
		    exp(equacao_sem_ext_sem_jud) / (1::numeric + exp(equacao_sem_ext_sem_jud)) AS prob_v21_s_ext_s_jud,
		    exp(equacao_melhor_possivel) / (1::numeric + exp(equacao_melhor_possivel)) AS prob_v21_melhor_possivel
		from(select                               
				t8.*,
				'-0.653864619905346'::numeric + new_processo_judicial_socios_1::numeric * 0.47631622865697 + bem_consultado_extendido::numeric * '-0.238154159610407'::numeric + bizu3_prob_woe_4cd * 0.707949760748082 + bizu2_prob_woe_4cd * 0.515497960211475 + extrato_score_prob_woe_4cd * 0.807945808661762 AS equacao,
				'-0.653864619905346'::numeric + new_processo_judicial_socios_1::numeric * 0.47631622865697 + bem_consultado_extendido::numeric * '-0.238154159610407'::numeric + bizu3_prob_woe_4cd * 0.707949760748082 + bizu2_prob_woe_4cd * 0.515497960211475 + '-0.976339'::numeric * 0.807945808661762 AS equacao_melhor_possivel,
				'-0.653864619905346'::numeric + 0::numeric * 0.47631622865697 + bem_consultado_extendido::numeric * '-0.238154159610407'::numeric + bizu3_prob_woe_4cd * 0.707949760748082 + bizu2_prob_woe_4cd * 0.515497960211475 + 0::numeric * 0.807945808661762 AS equacao_sem_ext_sem_jud
			from(select
					t1.lead_id,
					t1.TotalLawsuitsAsDefendant as processo_judicial_pf,
					coalesce(t2.reu_criminal,0) as reu_criminal_pf,
					(coalesce(t3.consulta_proprio,0) > 0)::int as consulta_proprio,
					(coalesce(t3.consulta_factoring,0) > 0)::int as consulta_factoring,
					(coalesce(t3.consulta_seguradora,0) > 0)::int as consulta_seguradora,
					(coalesce(t3.consulta_financeira,0) > 0)::int as consulta_financeira,
					(coalesce(t3.consulta_cobranca,0) > 0)::int as consulta_cobranca,
					(coalesce(t3.consulta_provedor_dado,0) > 0)::int as consulta_provedor_dado,
					(coalesce(t3.consulta_fornecedores,0) > 0)::int as consulta_fornecedores,
					(coalesce(t3.consulta_indefinido,0) > 0)::int as consulta_indefinido,
					(greatest(t4.count_imoveis,t4.count_inpi_marcas,t4.count_obras,t4.count_pat,t4.tem_aeronave,t4.tem_exportacao,t4.tem_licenca_ambiental,t4.total_veiculos) > 0)::int as materialidade_neoway,
					t5.bizu3,
					t5.extrato_score,
					CASE
				        WHEN ((coalesce(t3.consulta_proprio,0) > 0)::int +
							(coalesce(t3.consulta_factoring,0) > 0)::int +
							(coalesce(t3.consulta_seguradora,0) > 0)::int +
							(coalesce(t3.consulta_financeira,0) > 0)::int +
							(coalesce(t3.consulta_cobranca,0) > 0)::int +
							(coalesce(t3.consulta_provedor_dado,0) > 0)::int +
							(coalesce(t3.consulta_fornecedores,0) > 0)::int +
							(coalesce(t3.consulta_indefinido,0) > 0)::int) = 1 AND (coalesce(t3.consulta_seguradora,0) > 0)::int = 1 THEN 1
				        WHEN ((coalesce(t3.consulta_proprio,0) > 0)::int +
							(coalesce(t3.consulta_factoring,0) > 0)::int +
							(coalesce(t3.consulta_seguradora,0) > 0)::int +
							(coalesce(t3.consulta_financeira,0) > 0)::int +
							(coalesce(t3.consulta_cobranca,0) > 0)::int +
							(coalesce(t3.consulta_provedor_dado,0) > 0)::int +
							(coalesce(t3.consulta_fornecedores,0) > 0)::int +
							(coalesce(t3.consulta_indefinido,0) > 0)::int) = 1 AND (coalesce(t3.consulta_fornecedores,0) > 0)::int = 1 THEN 1
				        WHEN greatest(t4.count_imoveis,t4.count_inpi_marcas,t4.count_obras,t4.count_pat,t4.tem_aeronave,t4.tem_exportacao,t4.tem_licenca_ambiental,t4.total_veiculos) > 0 THEN 1
				        WHEN ((coalesce(t3.consulta_proprio,0) > 0)::int +
							(coalesce(t3.consulta_factoring,0) > 0)::int +
							(coalesce(t3.consulta_seguradora,0) > 0)::int +
							(coalesce(t3.consulta_financeira,0) > 0)::int +
							(coalesce(t3.consulta_cobranca,0) > 0)::int +
							(coalesce(t3.consulta_provedor_dado,0) > 0)::int +
							(coalesce(t3.consulta_fornecedores,0) > 0)::int +
							(coalesce(t3.consulta_indefinido,0) > 0)::int) = 2 AND (coalesce(t3.consulta_seguradora,0) > 0)::int = 1 AND (coalesce(t3.consulta_fornecedores,0) > 0)::int = 1 THEN 1
				        ELSE 0
				    END AS bem_consultado_extendido,
				    CASE
				        WHEN t1.TotalLawsuitsAsDefendant = 1 THEN 1
				        ELSE 0
				    END AS new_processo_judicial_socios_1,
				    CASE
				        WHEN round(t5.bizu3::numeric, 4) >= 0.479003 THEN 0.777707
				        WHEN round(t5.bizu3::numeric, 4) < 0.156818 THEN '-0.804405'::numeric
				        WHEN round(t5.bizu3::numeric, 4) < 0.228132 THEN '-0.515299'::numeric
				        WHEN round(t5.bizu3::numeric, 4) < 0.294745 THEN '-0.339263'::numeric
				        WHEN round(t5.bizu3::numeric, 4) < 0.361099 THEN 0::numeric
				        WHEN round(t5.bizu3::numeric, 4) < 0.479003 THEN 0.108772
				        ELSE 0::numeric
				    END AS bizu3_prob_woe_4cd,
				    CASE
				        WHEN round(dp.pd_bizu21::numeric, 4) >= 0.257088 THEN 0.863615
				        WHEN round(dp.pd_bizu21::numeric, 4) < 0.041741 THEN '-0.772649'::numeric
				        WHEN round(dp.pd_bizu21::numeric, 4) < 0.083654 THEN '-0.328508'::numeric
				        WHEN round(dp.pd_bizu21::numeric, 4) < 0.121170 THEN '-0.182299'::numeric
				        WHEN round(dp.pd_bizu21::numeric, 4) < 0.147472 THEN 0.160740
				        WHEN round(dp.pd_bizu21::numeric, 4) < 0.257088 THEN 0.233871
				        ELSE '-0.182299'::numeric
				    END AS bizu2_prob_woe_4cd,
				    CASE
				        WHEN round(t5.extrato_score::numeric, 4) >= 0.381121 THEN 0.464994
				        WHEN round(t5.extrato_score::numeric, 4) < 0.133363 THEN '-0.976339'::numeric
				        WHEN round(t5.extrato_score::numeric, 4) < 0.282413 THEN '-0.508016'::numeric
				        WHEN round(t5.extrato_score::numeric, 4) < 0.381121 THEN 0.126166
				        ELSE 0::numeric
				    END AS extrato_score_prob_woe_4cd,
				    t6.faturamento_comprovado,
				    round(t6.faturamento_informado::numeric,2) as faturamento_informado,
					t7.filtro_pedido_afiliado,
					t7.filtro_serasa_score_0_1,
					t7.filtro_canais_esteiras,
					dp.previous_loans_count as renovacao,
					lr.valor_solicitado,
					o.interest_rate,
					lr.prazo_solicitado
				from(select
						cc.origem_id,
						cc.documento,
						t2.lead_id,
						t2.loan_request_id,
						((jsonb_array_elements(cc.data->'Result') ->'Processes'->>'TotalLawsuitsAsDefendant')::int > 0)::int as TotalLawsuitsAsDefendant
					from public.credito_coleta cc
					join(select
							loan_request_id,
							max(lead_id) as lead_id,
							max(repeat('0', 11 - length(signer_cpf)) || signer_cpf) filter (where indice_socio = 1) as major_signer_cpf
						from(select
								lr.lead_id,
								lr.loan_request_id,
								row_number() over (partition by lr.loan_request_id order by coalesce(s.share_percentage,es.participacao) desc,age(lr.date_inserted,s.date_of_birth) desc) as indice_socio,
								coalesce(case when s.cpf = '' then null else s.cpf end,case when es.cpf = '' then null else es.cpf end, case when ser.cpf_socios = '' then null else ser.cpf_socios end,
									case when left(lr.vinculo,5) in ('S�cio','S�cia') or lr.vinculo = 'Presidente' then lr.cpf end) as signer_cpf
							from(select
									dp.direct_prospect_id as lead_id,
									lr.loan_request_id,
									dp.cnpj,
									dp.cpf,
									lr.date_inserted::date,
									dp.vinculo
								from direct_prospects dp
								join data_science.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
								join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
								where dp.direct_prospect_id  in (1576518)
								) as lr		
							left join(select 
									row_number() over (partition by lrs.loan_request_id,s.cpf order by 
										case when s.email is null then 1 else 0 end +
										case when s.civil_status is null then 1 else 0 end +
										case when s.profession is null then 1 else 0 end +
										case when s.rg is null then 1 else 0 end +
										case when s.issuing_body is null then 1 else 0 end +
										case when s.country_of_birth is null then 1 else 0 end +
										case when s.city_of_birth is null then 1 else 0 end +
										case when s.date_of_birth is null then 1 else 0 end) as indice,
									lrs.loan_request_id,
									dp.direct_prospect_id,
									s.cpf,
									s.share_percentage,
									s.date_of_birth,
									dp.cnpj
								from public.loan_requests_signers lrs
								join public.signers s on s.signer_id = lrs.signer_id
								left join public.signer_addresses sa on sa.address_id = s.address_id
								left join public.cities c on c.city_id = sa.city_id
								join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
								join public.offers o on o.offer_id = lr.offer_id
								join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
								where s.share_percentage > 0 and dp.direct_prospect_id  in (1576518)
								) s on s.loan_request_id = lr.loan_request_id
							full outer join (select
									es.cnpj,
									es.cpf,
									es.participacao,
									row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
								from public.empresas_socios es
								join public.direct_prospects dp on dp.cnpj = es.cnpj
								left join (select distinct 
										dp.cnpj
									from public.loan_requests_signers lrs
									join public.signers s on s.signer_id = lrs.signer_id
									join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
									join public.offers o on o.offer_id = lr.offer_id
									join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
									where s.share_percentage > 0) as t2 on t2.cnpj = es.cnpj
								where participacao > 0 and t2.cnpj is null and dp.direct_prospect_id  in (1576518)
								) es on es.cnpj = lr.cnpj 
							full outer join (SELECT 
									t1.direct_prospect_id,
									t1.cnpj,
									t1.cpf,
								    ((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text) ->> 'cpf')::text AS cpf_socios,
									(((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
								from credito_coleta cc
								join (select dp.direct_prospect_id,
										max(dp.cnpj) as cnpj,
										max(dp.cpf) as cpf,
										max(cc.id) AS max_id
									from credito_coleta cc
									join direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
									join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
									join public.loan_requests lr on lr.loan_request_id = o.offer_id
									left join public.empresas_socios es on es.cnpj = dp.cnpj and es.participacao > 0
									left join (select distinct 
											lrs.loan_request_id
										from public.loan_requests_signers lrs 
										join public.signers s on s.signer_id = lrs.signer_id
										where s.share_percentage > 0) as t2 on t2.loan_request_id = lr.loan_request_id
									WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) 
										AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
										and es.cnpj is null
										and t2.loan_request_id is null
										and dp.direct_prospect_id  in (1576518)
									GROUP BY 1) t1 ON cc.id = t1.max_id) ser on ser.direct_prospect_id = lr.lead_id and ser.share > 0
							where lr.cnpj is not null and coalesce(s.indice,es.indice,1) = 1 and lr.lead_id  in (1576518)
							) as t1
						group by 1) as t2 on t2.major_signer_cpf = cc.documento
					where tipo = 'BigBoostPerson') as t1
				left join(select
						documento,
						max(t1.lead_id) as lead_id,
						count(distinct t1.id_processo) as reu_criminal
					from(select
							cc.origem_id,
							cc.documento,
							t2.lead_id,
							jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->>'Number' as id_processo,
							jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->>'CourtType' as tribunal_processo,
							(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->>'Value')::numeric as valor_processo,
							(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->>'PublicationDate')::date as data_publicacao_processo,
							(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->>'NoticeDate')::date as data_notificacao_processo,
							jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->'Parties')->>'Doc' as doc_parte,
							jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->'Parties')->>'Name' as nome_parte,
							jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(cc.data->'Result') ->'Processes'->'Lawsuits')->'Parties')->>'Type' as tipo_parte
						from public.credito_coleta cc
						join(select
								loan_request_id,
								max(lead_id) as lead_id,
								max(repeat('0', 11 - length(signer_cpf)) || signer_cpf) filter (where indice_socio = 1) as major_signer_cpf
							from(select
									lr.lead_id,
									lr.loan_request_id,
									row_number() over (partition by lr.loan_request_id order by coalesce(s.share_percentage,es.participacao) desc,age(lr.date_inserted,s.date_of_birth) desc) as indice_socio,
									coalesce(case when s.cpf = '' then null else s.cpf end,case when es.cpf = '' then null else es.cpf end, case when ser.cpf_socios = '' then null else ser.cpf_socios end,
										case when left(lr.vinculo,5) in ('S�cio','S�cia') or lr.vinculo = 'Presidente' then lr.cpf end) as signer_cpf
								from(select
										dp.direct_prospect_id as lead_id,
										lr.loan_request_id,
										dp.cnpj,
										dp.cpf,
										lr.date_inserted::date,
										dp.vinculo
									from direct_prospects dp
									join(select
											o.direct_prospect_id,
											max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
											max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
											max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
											max(o.offer_id) as ultima_oferta
										from public.offers o
										left join public.loan_requests lr on lr.offer_id = o.offer_id
										group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = dp.direct_prospect_id
									join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
									join(select
											offer_id,
											max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
											max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
											max(loan_request_id) as ultimo_pedido
										from public.loan_requests
										group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
									join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
									where dp.direct_prospect_id  in (1576518)
									) as lr		
								left join(select 
										row_number() over (partition by lrs.loan_request_id,s.cpf order by 
											case when s.email is null then 1 else 0 end +
											case when s.civil_status is null then 1 else 0 end +
											case when s.profession is null then 1 else 0 end +
											case when s.rg is null then 1 else 0 end +
											case when s.issuing_body is null then 1 else 0 end +
											case when s.country_of_birth is null then 1 else 0 end +
											case when s.city_of_birth is null then 1 else 0 end +
											case when s.date_of_birth is null then 1 else 0 end) as indice,
										lrs.loan_request_id,
										dp.direct_prospect_id,
										s.cpf,
										s.share_percentage,
										s.date_of_birth,
										dp.cnpj
									from public.loan_requests_signers lrs
									join public.signers s on s.signer_id = lrs.signer_id
									left join public.signer_addresses sa on sa.address_id = s.address_id
									left join public.cities c on c.city_id = sa.city_id
									join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
									join public.offers o on o.offer_id = lr.offer_id
									join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
									where s.share_percentage > 0 and dp.direct_prospect_id  in (1576518)
									) s on s.loan_request_id = lr.loan_request_id
								full outer join (select
										es.cnpj,
										es.cpf,
										es.participacao,
										row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
									from public.empresas_socios es
									join public.direct_prospects dp on dp.cnpj = es.cnpj
									left join (select distinct 
											dp.cnpj
										from public.loan_requests_signers lrs
										join public.signers s on s.signer_id = lrs.signer_id
										join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
										join public.offers o on o.offer_id = lr.offer_id
										join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
										where s.share_percentage > 0) as t2 on t2.cnpj = es.cnpj
									where participacao > 0 and t2.cnpj is null and dp.direct_prospect_id  in (1576518)
									) es on es.cnpj = lr.cnpj 
								full outer join (SELECT 
										t1.direct_prospect_id,
										t1.cnpj,
										t1.cpf,
									    ((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text) ->> 'cpf')::text AS cpf_socios,
										(((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
									from credito_coleta cc
									join (select dp.direct_prospect_id,
											max(dp.cnpj) as cnpj,
											max(dp.cpf) as cpf,
											max(cc.id) AS max_id
										from credito_coleta cc
										join direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
										join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
										join public.loan_requests lr on lr.loan_request_id = o.offer_id
										left join public.empresas_socios es on es.cnpj = dp.cnpj and es.participacao > 0
										left join (select distinct 
												lrs.loan_request_id
											from public.loan_requests_signers lrs 
											join public.signers s on s.signer_id = lrs.signer_id
											where s.share_percentage > 0) as t2 on t2.loan_request_id = lr.loan_request_id
										WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) 
											AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
											and es.cnpj is null
											and t2.loan_request_id is null
											and dp.direct_prospect_id  in (1576518)
										GROUP BY 1) t1 ON cc.id = t1.max_id) ser on ser.direct_prospect_id = lr.lead_id and ser.share > 0
								where lr.cnpj is not null and coalesce(s.indice,es.indice,1) = 1 and lr.lead_id  in (1576518)
								) as t1
							group by 1) as t2 on t2.major_signer_cpf = cc.documento
						where tipo = 'BigBoostPerson') as t1
					where t1.tribunal_processo like '%CRIMINAL%' and t1.documento = t1.doc_parte and t1.tipo_parte in ('DEFENDANT','CLAIMED')
					group by 1) as t2 on t2.documento = t1.documento
				left join(select
						direct_prospect_id,
						coalesce(count(*) filter (where consulta_proprio = 1),0) as consulta_proprio,
						coalesce(count(*) filter (where consulta_factoring = 1 and consulta_proprio = 0),0) as consulta_factoring,
						coalesce(count(*) filter (where consulta_seguradora = 1 and greatest(consulta_proprio,consulta_factoring) = 0),0) as consulta_seguradora,
						coalesce(count(*) filter (where consulta_financeira = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora) = 0),0) as consulta_financeira,	
						coalesce(count(*) filter (where consulta_cobranca = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira) = 0),0) as consulta_cobranca,
						coalesce(count(*) filter (where consulta_provedor_dado = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca) = 0),0) as consulta_provedor_dado,
						coalesce(count(*) filter (where consulta_fornecedores = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado) = 0),0) as consulta_fornecedores,
						coalesce(count(*) filter (where greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado,consulta_fornecedores) = 0),0) as consulta_indefinido
					from (SELECT 
						direct_prospect_id,
						consultas,
					    CASE
					        WHEN ("position"(t2.consultas,'INVEST DIR'::text) > 0 OR 
					        	"position"(t2.consultas,'CREDITORIO'::text) > 0 OR 
					        	"position"(t2.consultas,'FUNDO DE INVE'::text) > 0 OR 
					        	"position"(t2.consultas,'SECURITIZADORA'::text) > 0 OR 
					        	"position"(t2.consultas,'FACTORING'::text) > 0 OR 
					        	("position"(t2.consultas,'FOMENTO'::text) > 0 and "position"(t2.consultas,'AGENCIA'::text) = 0) OR 
					           	"position"(t2.consultas,'FIDC'::text) > 0 OR 
					        	"position"(t2.consultas,'NOVA S R M'::text) > 0 OR 
					        	"position"(t2.consultas,'RED SA'::text) > 0 OR 
					        	"position"(t2.consultas,'DEL MONTE SERVICOS'::text) > 0 OR 
					        	"position"(t2.consultas,'123QRED'::text) > 0 OR 
					        	"position"(t2.consultas,'SERVICOS FINANCEIRO'::text) > 0 or
					        	"position"(t2.consultas,'ADIANTA PAGAMENTO '::text) > 0 OR
								"position"(t2.consultas,'ASAAS GESTAO'::text) > 0 OR
								"position"(t2.consultas,'BRA GESTORA '::text) > 0 OR
								"position"(t2.consultas,'DUNAS SERVICOS FINANCEIROS'::text) > 0 OR
								"position"(t2.consultas,'FEDERAL INVEST'::text) > 0 OR
								"position"(t2.consultas,'HOOME CREDIT'::text) > 0 OR
								"position"(t2.consultas,'INVISTA CREDITO'::text) > 0 OR
								"position"(t2.consultas,'LIDERFAC TECNOLOGIA'::text) > 0 OR
								"position"(t2.consultas,'INVEST DIREITOS'::text) > 0 OR
								"position"(t2.consultas,'PLATAFORMA ANTECIPA'::text) > 0 OR
								"position"(t2.consultas,'PREMIUM SERVICOS FINANCEIROS'::text) > 0 OR
								"position"(t2.consultas,'PRIX EMPRESARIAL'::text) > 0 OR
								"position"(t2.consultas,'TIRRENO'::text) > 0 OR
								"position"(t2.consultas,'ADIANTA'::text) > 0 OR
								"position"(t2.consultas,'VIA CAPITAL GESTAO'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_factoring,
					    CASE
					        WHEN ("position"(t2.consultas,'SEGURADORA'::text) > 0 OR
								"position"(t2.consultas,'AMIL ASSISTENCIA'::text) > 0 OR
								"position"(t2.consultas,'CONSORCIOS'::text) > 0 OR
								"position"(t2.consultas,'BARIGUI COMPANHIA HIPOTECARIA'::text) > 0 OR
								"position"(t2.consultas,'BERKLEY INTERNATIONAL'::text) > 0 OR
								"position"(t2.consultas,'BRADESCO AUTO '::text) > 0 OR
								"position"(t2.consultas,'CESCE BRASIL'::text) > 0 OR
								"position"(t2.consultas,'CIA DE ARRENDAMENTO MERCANTIL'::text) > 0 OR
								"position"(t2.consultas,'CONSORCIO'::text) > 0 OR
								"position"(t2.consultas,'EULER HERMES'::text) > 0 OR
								"position"(t2.consultas,'SEGURO'::text) > 0 OR
								"position"(t2.consultas,'PORTOSEG'::text) > 0 OR
								"position"(t2.consultas,'RODOBENS ADMINISTRACAO'::text) > 0 OR
								"position"(t2.consultas,'SAMETRADE OPERADORA'::text) > 0 OR
								"position"(t2.consultas,'SUL AMERICA SEG'::text) > 0 OR
								"position"(t2.consultas,'SULMED ASSISTENCIA'::text) > 0 OR
								"position"(t2.consultas,'GARANTIA AFIANCADORA'::text) > 0 OR
								"position"(t2.consultas,'ODONTOPREV'::text) > 0 OR
								"position"(t2.consultas,'UNIVIDA'::text) > 0 OR
								"position"(t2.consultas,'UNIMED'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_seguradora,
					    CASE
					        WHEN ("position"(t2.consultas,'COBRANCA'::text) > 0 or
					        	"position"(t2.consultas,'ASSECAD BRASIL'::text) > 0 or
					        	"position"(t2.consultas,'CONVENIO CADASTRAL'::text) > 0 or
					        	"position"(t2.consultas,'INFOR-LINE'::text) > 0 or
								"position"(t2.consultas,'OFFICE SOLUCOES &'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_cobranca,
					    CASE
					        WHEN ("position"(t2.consultas,'ASSERTIVA TECNOLOGIA'::text) > 0 OR
								"position"(t2.consultas,'BANCODOC'::text) > 0 OR
								"position"(t2.consultas,'BIG DATA SOLUCOES'::text) > 0 OR
								"position"(t2.consultas,'BOMPARA TECNOLOGIA'::text) > 0 OR
								"position"(t2.consultas,'CHEQUE-PRE'::text) > 0 OR
								"position"(t2.consultas,'DIRECT SMART DATA'::text) > 0 OR
								"position"(t2.consultas,'DUN & BRAD'::text) > 0 OR
								"position"(t2.consultas,'FD DO BRASIL'::text) > 0 OR
								"position"(t2.consultas,'OMNI'::text) > 0 OR
								"position"(t2.consultas,'METADADOS'::text) > 0 OR
								"position"(t2.consultas,'OBVIO BRASIL'::text) > 0 OR
								"position"(t2.consultas,'CANAL DA INTERNET'::text) > 0 OR
								"position"(t2.consultas,'CENIN CENTRO'::text) > 0 OR
								"position"(t2.consultas,'CHECKLOG'::text) > 0 OR
								"position"(t2.consultas,'COFACE BRASIL'::text) > 0 OR
								"position"(t2.consultas,'MR MARQUES SERVICES'::text) > 0 OR
								"position"(t2.consultas,'UPLEXIS'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_provedor_dado,
					    CASE
					        WHEN ("position"(t2.consultas,'CREDITO'::text) > 0 OR
								"position"(t2.consultas,'AGENCIA DE FOMENTO'::text) > 0 OR
								"position"(t2.consultas,'AGENCIA FOMENTO'::text) > 0 OR
								"position"(t2.consultas,'INVESTIMEN'::text) > 0 OR
								"position"(t2.consultas,'BANCO '::text) > 0 OR
								"position"(t2.consultas,'BANESE'::text) > 0 OR
								"position"(t2.consultas,'BCO NACIONAL'::text) > 0 OR
								"position"(t2.consultas,'CREDIT'::text) > 0 OR
								"position"(t2.consultas,'CAIXA ESTADUAL S/A AGENCIA'::text) > 0 OR
								"position"(t2.consultas,'CEF'::text) > 0 OR
								"position"(t2.consultas,'MUTUO'::text) > 0 OR
								"position"(t2.consultas,'CEN COOPERATIVAS CRED'::text) > 0 OR
								"position"(t2.consultas,'CENTRO DE APOIO AOS PEQUENOS'::text) > 0 OR
								"position"(t2.consultas,'UNICRED'::text) > 0 OR
								"position"(t2.consultas,'COOP CENTRAL BASE DE SERV DE RESPON'::text) > 0 OR
								"position"(t2.consultas,'COOPERATIVA CEN CRED'::text) > 0 OR
								"position"(t2.consultas,'COOPERATIVA CRED'::text) > 0 OR
								"position"(t2.consultas,'COOPERATIVA CREDITO'::text) > 0 OR
								"position"(t2.consultas,'COOPERATIVA POUPANCA '::text) > 0 OR
								"position"(t2.consultas,'CRED MUT'::text) > 0 OR
								"position"(t2.consultas,'CORRESPONDENTE'::text) > 0 OR
								"position"(t2.consultas,'CREDUNI'::text) > 0 OR
								"position"(t2.consultas,'CONSORCIOS'::text) > 0 OR
								"position"(t2.consultas,'SERV FINANCEIROS'::text) > 0 OR
								"position"(t2.consultas,'SERVICOS FINANCEIROS'::text) > 0 OR
								"position"(t2.consultas,'LENDING'::text) > 0 OR
								"position"(t2.consultas,'EMPRESTIMO'::text) > 0 OR
								"position"(t2.consultas,'NEXOOS'::text) > 0 OR
								"position"(t2.consultas,'TUTU DIGITAL'::text) > 0 OR
								"position"(t2.consultas,'CREDIUNI'::text) > 0 OR
								"position"(t2.consultas,'CARTOES CRED'::text) > 0 OR
								"position"(t2.consultas,'FINANC'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_financeira,
					    CASE
					        WHEN ("position"(t2.consultas,'INFORMATICA'::text) > 0 OR
								"position"(t2.consultas,'TELECOM'::text) > 0 OR
								"position"(t2.consultas,'3M DO BRASIL'::text) > 0 OR
								"position"(t2.consultas,'ARTIGOS'::text) > 0 OR
								"position"(t2.consultas,'DISTRIB'::text) > 0 OR
								"position"(t2.consultas,'VEICU'::text) > 0 OR
								"position"(t2.consultas,'CONFEC'::text) > 0 OR
								"position"(t2.consultas,'CALCA'::text) > 0 OR
								"position"(t2.consultas,'COMERC'::text) > 0 OR
								"position"(t2.consultas,'HOTE'::text) > 0 OR
								"position"(t2.consultas,'COMBU'::text) > 0 OR
								"position"(t2.consultas,'AUTO'::text) > 0 OR
								"position"(t2.consultas,'TRANSP'::text) > 0 OR
								"position"(t2.consultas,'FERR'::text) > 0 OR
								"position"(t2.consultas,'INDUSTR'::text) > 0 OR
								"position"(t2.consultas,'EMBALA'::text) > 0 OR
								"position"(t2.consultas,'PLAST'::text) > 0 OR
								"position"(t2.consultas,'ADUBO'::text) > 0 OR
								"position"(t2.consultas,'TECNOLOGIA'::text) > 0 OR
								"position"(t2.consultas,'CONSULT'::text) > 0 OR
								"position"(t2.consultas,'VIDR'::text) > 0 OR
								"position"(t2.consultas,'AGRIC'::text) > 0 OR
								"position"(t2.consultas,'PROD'::text) > 0 OR
								"position"(t2.consultas,'S/A'::text) > 0 OR
								"position"(t2.consultas,'ARAMADOS'::text) > 0 OR
								"position"(t2.consultas,'COMEX'::text) > 0 OR
								"position"(t2.consultas,'COM DE'::text) > 0 OR
								"position"(t2.consultas,'ATACAD'::text) > 0 OR
								"position"(t2.consultas,'EXPORT'::text) > 0 OR
								"position"(t2.consultas,'MAQUINA'::text) > 0 OR
								"position"(t2.consultas,'RECAUC'::text) > 0 OR
								"position"(t2.consultas,'IMOV'::text) > 0 OR
								"position"(t2.consultas,'LOCAC'::text) > 0 OR
								"position"(t2.consultas,'METAL'::text) > 0 OR
								"position"(t2.consultas,'ALUMI'::text) > 0 OR
								"position"(t2.consultas,'ELETR'::text) > 0 OR
								"position"(t2.consultas,'IMPORT'::text) > 0 OR
								"position"(t2.consultas,'AMBEV'::text) > 0 OR
								"position"(t2.consultas,'ASSOCI'::text) > 0 OR
								"position"(t2.consultas,'TEXTIL'::text) > 0 OR
								"position"(t2.consultas,'FARMA'::text) > 0 OR
								"position"(t2.consultas,'CHUMBA'::text) > 0 OR
								"position"(t2.consultas,'EQUIP'::text) > 0 OR
								"position"(t2.consultas,'PECAS'::text) > 0 OR
								"position"(t2.consultas,'COMPANHIA'::text) > 0 OR
								"position"(t2.consultas,'MODA'::text) > 0 OR
								"position"(t2.consultas,'SUPRI'::text) > 0 OR
								"position"(t2.consultas,'ACELORMITTAL'::text) > 0 OR
								"position"(t2.consultas,'ARMAZ'::text) > 0 OR
								"position"(t2.consultas,'CIMENTO'::text) > 0 OR
								"position"(t2.consultas,'SHOP'::text) > 0 OR
								"position"(t2.consultas,'QUIM'::text) > 0 OR
								"position"(t2.consultas,'MEDIC'::text) > 0 OR
								"position"(t2.consultas,'EMPREE'::text) > 0 OR
								"position"(t2.consultas,'CARNE'::text) > 0 OR
								"position"(t2.consultas,'ALIMENT'::text) > 0 OR
								"position"(t2.consultas,'& CIA'::text) > 0 OR
								"position"(t2.consultas,'IND E COM'::text) > 0 OR
								"position"(t2.consultas,'ETIQUE'::text) > 0 OR
								"position"(t2.consultas,'BIGCARG'::text) > 0 OR
								"position"(t2.consultas,'BIMBO'::text) > 0 OR
								"position"(t2.consultas,'ALARM'::text) > 0 OR
								"position"(t2.consultas,'IMOB'::text) > 0 OR
								"position"(t2.consultas,'DIESEL'::text) > 0 OR
								"position"(t2.consultas,'LOGIST'::text) > 0 OR
								"position"(t2.consultas,'BRF S/A'::text) > 0 OR
								"position"(t2.consultas,'CARGAS'::text) > 0 OR
								"position"(t2.consultas,'ARQUIT'::text) > 0 OR
								"position"(t2.consultas,'CONSTRU'::text) > 0 OR
								"position"(t2.consultas,'CABOS'::text) > 0 OR
								"position"(t2.consultas,'PNEU'::text) > 0 OR
								"position"(t2.consultas,'LOJISTA'::text) > 0 OR
								"position"(t2.consultas,'TRANSMI'::text) > 0 OR
								"position"(t2.consultas,'LUBRIFIC'::text) > 0 OR
								"position"(t2.consultas,'CARTONA'::text) > 0 OR
								"position"(t2.consultas,'CASA ALADIM'::text) > 0 OR
								"position"(t2.consultas,'TINT'::text) > 0 OR
								"position"(t2.consultas,'COMPENSADOS'::text) > 0 OR
								"position"(t2.consultas,'HIDRA'::text) > 0 OR
								"position"(t2.consultas,'CERAMICA'::text) > 0 OR
								"position"(t2.consultas,'AGRO'::text) > 0 OR
								"position"(t2.consultas,'CERVEJ'::text) > 0 OR
								"position"(t2.consultas,'FABRIL'::text) > 0 OR
								"position"(t2.consultas,'IND COM'::text) > 0 OR
								"position"(t2.consultas,'CLARO SA'::text) > 0 OR
								"position"(t2.consultas,'COLCHO'::text) > 0 OR
								"position"(t2.consultas,'GRAF'::text) > 0 OR
								"position"(t2.consultas,'COM E IND'::text) > 0 OR
								"position"(t2.consultas,'COML'::text) > 0 OR
								"position"(t2.consultas,'COMPRE FACIL'::text) > 0 OR
								"position"(t2.consultas,'SOFTWARE'::text) > 0 OR
								"position"(t2.consultas,'CONCRET'::text) > 0 OR
								"position"(t2.consultas,'FABRIC'::text) > 0 OR
								"position"(t2.consultas,'CONDOM'::text) > 0 OR
								"position"(t2.consultas,'PROJET'::text) > 0 OR
								"position"(t2.consultas,'CROMAG'::text) > 0 OR
								"position"(t2.consultas,'DDTOTAL'::text) > 0 OR
								"position"(t2.consultas,'CONDICI'::text) > 0 OR
								"position"(t2.consultas,'DHL WORLD'::text) > 0 OR
								"position"(t2.consultas,'MALHA'::text) > 0 OR
								"position"(t2.consultas,'DURATEX'::text) > 0 OR
								"position"(t2.consultas,'MOBILI'::text) > 0 OR
								"position"(t2.consultas,'MATER'::text) > 0 OR
								"position"(t2.consultas,'BOMBAS'::text) > 0 OR
								"position"(t2.consultas,'EDITOR'::text) > 0 OR
								"position"(t2.consultas,'ELASTIC'::text) > 0 OR
								"position"(t2.consultas,'PORCELAN'::text) > 0 OR
								"position"(t2.consultas,'ENERG'::text) > 0 OR
								"position"(t2.consultas,'ESTOF'::text) > 0 OR
								"position"(t2.consultas,'ESTRUTURA'::text) > 0 OR
								"position"(t2.consultas,'PRINT'::text) > 0 OR
								"position"(t2.consultas,'TURISM'::text) > 0 OR
								"position"(t2.consultas,'COMUNIC'::text) > 0 OR
								"position"(t2.consultas,'FERTILI'::text) > 0 OR
								"position"(t2.consultas,'TECIDOS'::text) > 0 OR
								"position"(t2.consultas,'NUTRIC'::text) > 0 OR
								"position"(t2.consultas,'FORMAS'::text) > 0 OR
								"position"(t2.consultas,'PETROL'::text) > 0 OR
								"position"(t2.consultas,'MOTO'::text) > 0 OR
								"position"(t2.consultas,'FRIGO'::text) > 0 OR
								"position"(t2.consultas,'FUNDICAO'::text) > 0 OR
								"position"(t2.consultas,'JOIA'::text) > 0 OR
								"position"(t2.consultas,'ELECTR'::text) > 0 OR
								"position"(t2.consultas,'SORVETE'::text) > 0 OR
								"position"(t2.consultas,'GERDAU'::text) > 0 OR
								"position"(t2.consultas,'PROVEDOR'::text) > 0 OR
								"position"(t2.consultas,'GOOGLE'::text) > 0 OR
								"position"(t2.consultas,'ADESIVO'::text) > 0 OR
								"position"(t2.consultas,'TREINA'::text) > 0 OR
								"position"(t2.consultas,'IMUNIZ'::text) > 0 OR
								"position"(t2.consultas,'COMPUT'::text) > 0 OR
								"position"(t2.consultas,'INNOVA'::text) > 0 OR
								"position"(t2.consultas,'ENSIN'::text) > 0 OR
								"position"(t2.consultas,'COM IMP MAT'::text) > 0 OR
								"position"(t2.consultas,'MOVEIS'::text) > 0 OR
								"position"(t2.consultas,'ARTEF'::text) > 0 OR
								"position"(t2.consultas,'BRINDE'::text) > 0 OR
								"position"(t2.consultas,'COMPONENTES'::text) > 0 OR
								"position"(t2.consultas,'LABOR'::text) > 0 OR
								"position"(t2.consultas,'LATICI'::text) > 0 OR
								"position"(t2.consultas,'LOCALIZA'::text) > 0 OR
								"position"(t2.consultas,'GERADORES'::text) > 0 OR
								"position"(t2.consultas,'BORR'::text) > 0 OR
								"position"(t2.consultas,'ACESSOR'::text) > 0 OR
								"position"(t2.consultas,'VESTUA'::text) > 0 OR
								"position"(t2.consultas,'MADEIR'::text) > 0 OR
								"position"(t2.consultas,'CONSTR'::text) > 0 OR
								"position"(t2.consultas,'MERCAD'::text) > 0 OR
								"position"(t2.consultas,'MECANICA'::text) > 0 OR
								"position"(t2.consultas,'SUCO'::text) > 0 OR
								"position"(t2.consultas,'GASTRO'::text) > 0 OR
								"position"(t2.consultas,'ENGEN'::text) > 0 OR
								"position"(t2.consultas,'COPIADO'::text) > 0 OR
								"position"(t2.consultas,'MOLAS'::text) > 0 OR
								"position"(t2.consultas,'CONTABILI'::text) > 0 OR
								"position"(t2.consultas,'COMEST'::text) > 0 OR
								"position"(t2.consultas,'REFRI'::text) > 0 OR
								"position"(t2.consultas,'SEMENTES'::text) > 0 OR
								"position"(t2.consultas,'ROLAMEN'::text) > 0 OR
								"position"(t2.consultas,'PESCADO'::text) > 0 OR
								"position"(t2.consultas,'PEPSICO'::text) > 0 OR
								"position"(t2.consultas,'POSTO'::text) > 0 OR
								"position"(t2.consultas,'RESINAS'::text) > 0 OR
								"position"(t2.consultas,'MAGAZINE'::text) > 0 OR
								"position"(t2.consultas,'DOCES'::text) > 0 OR
								"position"(t2.consultas,'RADIO'::text) > 0 OR
								"position"(t2.consultas,'CELUL'::text) > 0 OR
								"position"(t2.consultas,'ROBERT BOSH'::text) > 0 OR
								"position"(t2.consultas,'SANITARIOS'::text) > 0 OR
								"position"(t2.consultas,'RODOVIAR'::text) > 0 OR
								"position"(t2.consultas,'OPTIC'::text) > 0 OR
								"position"(t2.consultas,'CAMINHO'::text) > 0 OR
								"position"(t2.consultas,'SOUZA CRUZ'::text) > 0 OR
								"position"(t2.consultas,'TAMBASA'::text) > 0 OR
								"position"(t2.consultas,'TECELAGEM'::text) > 0 OR
								"position"(t2.consultas,'TECNIC'::text) > 0 OR
								"position"(t2.consultas,'SISTEMAS'::text) > 0 OR
								"position"(t2.consultas,'TOTVS'::text) > 0 OR
								"position"(t2.consultas,'REFRES'::text) > 0 OR
								"position"(t2.consultas,'UNIVEN HEALTHCARE'::text) > 0 OR
								"position"(t2.consultas,'MARMOR'::text) > 0 OR
								"position"(t2.consultas,'AROMA'::text) > 0 OR
								"position"(t2.consultas,'VINICO'::text) > 0 OR
								"position"(t2.consultas,'WHIRPOOL'::text) > 0 OR
								"position"(t2.consultas,'OTICA'::text) > 0 OR
								"position"(t2.consultas,'ARTES'::text) > 0 OR
								"position"(t2.consultas,'ARGAMASSA'::text) > 0 OR
								"position"(t2.consultas,'IMPRESSAO'::text) > 0 OR
								"position"(t2.consultas,'VIAGENS'::text) > 0 OR
								"position"(t2.consultas,'FABER CATELL'::text) > 0 OR
								"position"(t2.consultas,'INOXI'::text) > 0 OR
								"position"(t2.consultas,'BARBA'::text) > 0 OR
								"position"(t2.consultas,'COSMETIC'::text) > 0 OR
								"position"(t2.consultas,'CACHORRO'::text) > 0 OR
								"position"(t2.consultas,'BR MALLS'::text) > 0 OR
								"position"(t2.consultas,'CONTABIL'::text) > 0 OR
								"position"(t2.consultas,'COMPONENTS'::text) > 0 OR
								"position"(t2.consultas,'CEMEAP CEN MED'::text) > 0 OR
								"position"(t2.consultas,'DEBUTANTES'::text) > 0 OR
								"position"(t2.consultas,'CESTA BASICA'::text) > 0 OR
								"position"(t2.consultas,'MEIOS'::text) > 0 OR
								"position"(t2.consultas,'CIMCAL LTDA'::text) > 0 OR
								"position"(t2.consultas,'COLORCON'::text) > 0 OR
								"position"(t2.consultas,'COLSON'::text) > 0 OR
								"position"(t2.consultas,'PROPAGANDA'::text) > 0 OR
								"position"(t2.consultas,'CONCENTRADOS'::text) > 0 OR
								"position"(t2.consultas,'CONVES WEB'::text) > 0 OR
								"position"(t2.consultas,'CAFE'::text) > 0 OR
								"position"(t2.consultas,'COOPERVISION'::text) > 0 OR
								"position"(t2.consultas,'CORA PAGAMENTOS'::text) > 0 OR
								"position"(t2.consultas,'COMPONENTE'::text) > 0 OR
								"position"(t2.consultas,'DENTAL'::text) > 0 OR
								"position"(t2.consultas,'ENDUTEX BRASIL'::text) > 0 OR
								"position"(t2.consultas,'ENERBRAX'::text) > 0 OR
								"position"(t2.consultas,'ENGARRAFAMENTO'::text) > 0 OR
								"position"(t2.consultas,'PERFUMARIA'::text) > 0 OR
								"position"(t2.consultas,'VAREJISTA'::text) > 0 OR
								"position"(t2.consultas,'BATTERIES'::text) > 0 OR
								"position"(t2.consultas,'GSI CREOS'::text) > 0 OR
								"position"(t2.consultas,'HORTI'::text) > 0 OR
								"position"(t2.consultas,'I M P LTDA'::text) > 0 OR
								"position"(t2.consultas,'ESPORTE'::text) > 0 OR
								"position"(t2.consultas,'INGRAM'::text) > 0 OR
								"position"(t2.consultas,'INTERNATIONAL PAPER'::text) > 0 OR
								"position"(t2.consultas,'INSUMOS'::text) > 0 OR
								"position"(t2.consultas,'KNAUF DO BRASIL'::text) > 0 OR
								"position"(t2.consultas,'LAVANDERIA'::text) > 0 OR
								"position"(t2.consultas,'LANCHO'::text) > 0 OR
								"position"(t2.consultas,'LOSINOX'::text) > 0 OR
								"position"(t2.consultas,'ILUMINA'::text) > 0 OR
								"position"(t2.consultas,'REVESTIMENTO'::text) > 0 OR
								"position"(t2.consultas,'FRANQUIA'::text) > 0 OR
								"position"(t2.consultas,'PAPEIS'::text) > 0 OR
								"position"(t2.consultas,'MERCANTIL MOR'::text) > 0 OR
								"position"(t2.consultas,'JEANS'::text) > 0 OR
								"position"(t2.consultas,'MINERACAO'::text) > 0 OR
								"position"(t2.consultas,'FESTA'::text) > 0 OR
								"position"(t2.consultas,'LIXO'::text) > 0 OR
								"position"(t2.consultas,'NEOPRENE'::text) > 0 OR
								"position"(t2.consultas,'NESTLE'::text) > 0 OR
								"position"(t2.consultas,'MAT PARA'::text) > 0 OR
								"position"(t2.consultas,'PAO DE'::text) > 0 OR
								"position"(t2.consultas,'EDUCATION'::text) > 0 OR
								"position"(t2.consultas,'POUSADA'::text) > 0 OR
								"position"(t2.consultas,'GRAPH'::text) > 0 OR
								"position"(t2.consultas,'ENCOMENDAS'::text) > 0 OR
								"position"(t2.consultas,'SPORTS'::text) > 0 OR
								"position"(t2.consultas,'RANDSTAD'::text) > 0 OR
								"position"(t2.consultas,'RAPPI'::text) > 0 OR
								"position"(t2.consultas,'RECAPAGE'::text) > 0 OR
								"position"(t2.consultas,'FOOD'::text) > 0 OR
								"position"(t2.consultas,'NOBREAK'::text) > 0 OR
								"position"(t2.consultas,'ROBERT BOSCH'::text) > 0 OR
								"position"(t2.consultas,'EVENTOS'::text) > 0 OR
								"position"(t2.consultas,'RUSSER BRASIL'::text) > 0 OR
								"position"(t2.consultas,'OPERADOR'::text) > 0 OR
								"position"(t2.consultas,'GAS'::text) > 0 OR
								"position"(t2.consultas,'FIOS'::text) > 0 OR
								"position"(t2.consultas,'ANTENAS'::text) > 0 OR
								"position"(t2.consultas,'PISOS'::text) > 0 OR
								"position"(t2.consultas,'SARTORIUS'::text) > 0 OR
								"position"(t2.consultas,'COMPON ELET'::text) > 0 OR
								"position"(t2.consultas,'SODEXHO'::text) > 0 OR
								"position"(t2.consultas,'LIMPE'::text) > 0 OR
								"position"(t2.consultas,'SOMAFERTIL'::text) > 0 OR
								"position"(t2.consultas,'COMPRESS'::text) > 0 OR
								"position"(t2.consultas,'MOLDADOS'::text) > 0 OR
								"position"(t2.consultas,'CARTUCHOS'::text) > 0 OR
								"position"(t2.consultas,'TENCOREALTY'::text) > 0 OR
								"position"(t2.consultas,'TRIVALE'::text) > 0 OR
								"position"(t2.consultas,'UNIFORT LTDA'::text) > 0 OR
								"position"(t2.consultas,'UNISYS'::text) > 0 OR
								"position"(t2.consultas,'UNIVAR'::text) > 0 OR
								"position"(t2.consultas,'USECORP LTDA'::text) > 0 OR
								"position"(t2.consultas,'MANGUEIRAS'::text) > 0 OR
								"position"(t2.consultas,'VELLO DIGITAL'::text) > 0 OR
								"position"(t2.consultas,'UTILIDADES'::text) > 0 OR
								"position"(t2.consultas,'VIAPARK'::text) > 0 OR
								"position"(t2.consultas,'DECORA'::text) > 0 OR
								"position"(t2.consultas,'SAPATARIA'::text) > 0) THEN 1
					        ELSE 0
					    END AS consulta_fornecedores,
					    CASE
					        WHEN ("position"(t2.razao_social,t2.consultas) > 0) THEN 1
					        ELSE 0
					    END AS consulta_proprio
					   FROM ( SELECT t1.direct_prospect_id,
					            ((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas,
					            t1.razao_social
					           FROM credito_coleta cc
					           JOIN (SELECT 
										dp.direct_prospect_id,
										max(dp."name") as razao_social,
									    max(dp.cnpj) as cnpj,
									    max(dp.cpf) as cpf,
									    max(id) AS max_id
									FROM credito_coleta cc
									JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
									JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
									JOIN loan_requests lr ON lr.offer_id = o.offer_id
									WHERE (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
										and dp.direct_prospect_id  in (1576518)
									GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id
					        union all
					         SELECT t1.direct_prospect_id,
					            ((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas,
					            t1.razao_social
					           FROM credito_coleta cc
					           JOIN (SELECT 
										dp.direct_prospect_id,
										max(dp."name") as razao_social,
									    max(dp.cnpj) as cnpj,
									    max(dp.cpf) as cpf,
									    max(id) AS max_id
									FROM credito_coleta cc
									JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
									JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
									JOIN loan_requests lr ON lr.offer_id = o.offer_id
									WHERE (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
										and dp.direct_prospect_id  in (1576518)
									GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id
					            ) t2
						where consultas != 'CREDITLOOP CORRESPONDENTE BANCARIO' and consultas != ''
						) as t3 
					group by 1) as t3 on t3.direct_prospect_id = t1.lead_id
				left join(select
							dp.direct_prospect_id,
							cc.id,
							cc.data_coleta,
							row_number() over (partition by dp.direct_prospect_id order by cc.id desc) indice_coleta,
							(cc.data->'_metadata'->'empresas'->'_metadata'->'licencasAmbientais'->'source' is not null)::int as tem_licenca_ambiental,
							(cc.data->'_metadata'->'empresas'->'_metadata'->'aeronaves'->'source' is not null)::int as tem_aeronave,
							coalesce(jsonb_array_length(cc.data->'inpiMarcas'),'0')::bigint as count_inpi_marcas,
							coalesce((cc.data->'detran'->>'totalVeiculos'),'0')::bigint as total_veiculos,
							coalesce(jsonb_array_length(cc.data->'imoveis'),'0')::bigint as count_imoveis,
							coalesce((cc.data->'totalObras'->>'quantidadeArts'),'0')::bigint as count_obras,
							(coalesce(position('EXPORTACAO' in cc.data->>'indicadoresFinanceiros'),0) > 0)::int as tem_exportacao,
							coalesce(jsonb_array_length(cc.data->'pat'),'0')::bigint as count_pat
						from public.credito_coleta cc
						join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
						join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
						join public.loan_requests lr on lr.offer_id = o.offer_id
						where cc.tipo = 'Neoway' 
							and dp.direct_prospect_id  in (1576518)) as t4 on t4.direct_prospect_id = t1.lead_id and t4.indice_coleta = 1
				join(select 
						legacy_target_id,
						(data_output->'Bizu3'->>'Probability')::numeric as bizu3,
						(data_output->'StatementScore'->>'Probability')::numeric as extrato_score,
						(data_output->'ChecklistScore'->>'Score')::numeric as mesa_score
					from public.sync_credit_worthiness
					where legacy_target_id  in (1576518)) as t5 on t5.legacy_target_id = t1.lead_id
				join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
				join public.offers o on o.offer_id = lr.offer_id
				join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
				left join(select
						dp.direct_prospect_id,
						round((sum(t1.value) filter(where t1.tag in ('boletos', 'tbi_ted', 'dinheiro', 'sacado_governo', 'adquirencia', 'marketplace','input_manual') and t1.movimentacao = 'positivo') / 3)::numeric, 2) as faturamento_comprovado,
						max(dp.month_revenue) as faturamento_informado
					from data_science.extratos_intermediarios t1
					join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
					join public.offers o on o.offer_id = lr.offer_id
					join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
					where dp.direct_prospect_id  in (1576518)
					group by 1) as t6 on t6.direct_prospect_id = t1.lead_id
				left join (select 
						dp.direct_prospect_id,
						max(case when pp.cnpj is not null then 1 else 0 end) as filtro_pedido_afiliado,
						max(case when dp.serasa_6 <= 1 then 1 else 0 end) as filtro_serasa_score_0_1,
						max(case when t1.utm_source is not null then 1 else 0 end) as filtro_canais_esteiras
					from public.loan_requests lr
					join public.offers o on o.offer_id = lr.offer_id
					join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
					left join public.parceiros_parceiro pp on left(pp.cnpj,8) = left(dp.cnpj,8)
					left join data_science.t_processo_automacao_canais_excluidos as t1 on t1.utm_source = dp.utm_source and t1.exclusao_vigente
					where dp.direct_prospect_id  in (1576518)
					group by 1) as t7 on t7.direct_prospect_id = t1.lead_id) as t8) as t9) as t10) as t11
union all
select 
	'prod' as fonte,
	legacy_target_id,
	case upper(t2.reu_pf) when 'N�O' then 0 when 'SIM' then 1 else t2.reu_pf::int end as reu_pf,
	case upper(t2.reu_criminal_pf) when 'N�O' then 0 when 'SIM' then 1 else t2.reu_criminal_pf::int end as reu_criminal_pf,
	case upper(t2.consulta_proprio) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_proprio::int end as consulta_proprio,
	case upper(t2.consulta_factoring) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_factoring::int end as consulta_factoring,
	case upper(t2.consulta_seguradora) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_seguradora::int end as consulta_seguradora,
	case upper(t2.consulta_financeira) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_financeira::int end as consulta_financeira,
	case upper(t2.consulta_cobranca) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_cobranca::int end as consulta_cobranca,
	case upper(t2.consulta_provedor_dado) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_provedor_dado::int end as consulta_provedor_dado,
	case upper(t2.consulta_fornecedores) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_fornecedores::int end as consulta_fornecedores,
	case upper(t2.consulta_indefinido) when 'N�O' then 0 when 'SIM' then 1 else t2.consulta_indefinido::int end as consulta_indefinido,
	case upper(t2.materialidade_neoway) when 'N�O' then 0 when 'SIM' then 1 else t2.materialidade_neoway::int end as materialidade_neoway,
	round((t1.data_output->'Bizu3'->>'Probability')::numeric,2) as bizu3,
	round((t1.data_output->'StatementScore'->>'Probability')::numeric,2) as extrato_score,
	t4.bem_consultado_extendido,
	t4.new_processo_judicial_socios_1,
	t4.bizu3_prob_woe_4cd,
	t4.bizu2_prob_woe_4cd,
	t4.extrato_score_prob_woe_4cd,
	round(t5.faturamento_comprovado::numeric,2) as faturamento_comprovado,
	t5.faturamento_informado,
	null::numeric as filtro_pedido_afiliado,
	null::numeric as filtro_serasa_score_0_1,
	null::numeric as filtro_canais_esteiras,
	null::numeric as renovacao,
	null::numeric as valor_solicitado,
	null::numeric as interest_rate,
	null::numeric as prazo_solicitado,
	null::numeric as equacao,
	null::numeric as equacao_melhor_possivel,
	null::numeric as equacao_sem_ext_sem_jud,
	round((t1.data_output->'ChecklistScore'->>'Probability')::numeric,2) as mesa_score,
	null::numeric as prob_v21_s_ext_s_jud,
	null::numeric as prob_v21_melhor_possivel,
	(t1.data_output->'ChecklistScore'->>'Score')::numeric as mesa_faixa,
	replace(replace(replace(t3.parecer_decisao,'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) as parecer_decisao,
	t3.parecer_limite::numeric,
	t3.parecer_taxa::numeric,
	t3.parecer_prazo::numeric
from public.sync_credit_worthiness t1
join (select
		t1.lead_id,
		string_agg(resposta,',') filter (where tag = 'NewConsultaIndefinido') as consulta_indefinido,
		string_agg(resposta,',') filter (where tag = 'NewCollectionOffice') as consulta_cobranca,
		string_agg(resposta,',') filter (where tag = 'NewConsultaFinanceira') as consulta_financeira,
		string_agg(resposta,',') filter (where tag = 'NewConsultaFornecedores') as consulta_fornecedores,
		string_agg(resposta,',') filter (where tag = 'NewConsultaProvedorDado') as consulta_provedor_dado,
		string_agg(resposta,',') filter (where tag = 'NewConsultaSeguradora') as consulta_seguradora,
		string_agg(resposta,',') filter (where tag = 'ConsultaFactoring') as consulta_factoring,
		string_agg(resposta,',') filter (where tag = 'NewConsultaProprio') as consulta_proprio,
		string_agg(resposta,',') filter (where tag = 'MaterialidadeNeoway') as materialidade_neoway,
		string_agg(resposta,',') filter (where tag = 'ProblemasJuridicosReu') as reu_pf,
		string_agg(resposta,',') filter (where tag = 'NewProcessoCriminalReuPj') as reu_criminal_pj,
		string_agg(resposta,',') filter (where tag = 'NewProcessoCriminalReuSocioMajor') as reu_criminal_pf
	from(select
			ca.legacy_target_id as lead_id,
			(ca.data_output ->> 'CreationDate')::date as data_criacao, 
			checklist ->> 'QuestionnaireType' as Parte,
			checklist ->> 'Asking' as Pergunta,
			checklist ->> 'Order' as Ordem,
			checklist ->> 'Tag' as tag,
			case when answer->>'TypeOfAnswer' = 'DISCURSIVE' then answer ->> 'DiscursiveReply' else answer ->> 'AnswerText' end as Resposta,
			answer->>'TypeOfAnswer' as tipoResposta
		from
			(select 
				legacy_target_id,
				data_output,
				row_number() over (partition by legacy_target_id order by id desc) as indice_analise
			from public.sync_credit_analysis) ca,
				public.direct_prospects dp,
				jsonb_array_elements(data_output ->'CreditChecklist') checklist,
				jsonb_array_elements(checklist -> 'Answers') answer
		where 
			ca.legacy_target_id = dp.direct_prospect_id and ca.indice_analise = 1 and (answer->>'BooleanReply' = 'true' or (answer->>'DiscursiveReply' is not null and answer->>'DiscursiveReply' <> ''))
				and ca.legacy_target_id  in (1576518)) as t1			
	where tag in (
		'NewConsultaIndefinido',
		'NewCollectionOffice',
		'NewConsultaFinanceira',
		'NewConsultaFornecedores',
		'NewConsultaProvedorDado',
		'NewConsultaSeguradora',
		'ConsultaFactoring',
		'NewConsultaProprio',
		'MaterialidadeNeoway',
		'ProblemasJuridicosReu',
		'NewProcessoCriminalReuPj',
		'NewProcessoCriminalReuSocioMajor'
		)
	group by 1) as t2 on t2.lead_id = t1.legacy_target_id
join public.direct_prospects dp on dp.direct_prospect_id = t1.legacy_target_id
join (select 
		lead_id,
		parecer_data,
		is_bizbot,
		parecer_analista,
		parecer_decisao,
		parecer_limite,
		parecer_taxa,
		parecer_prazo
	from(select
			t1.legacy_target_id as lead_id,
			row_number() over (partition by t1.legacy_target_id order by parecer ->> 'AuthorityLevel' desc) as indice_analise,
		 	(parecer->'LastChange'->>'Date')::date as parecer_data,
		 	parecer->>'IsAutomatic' as is_bizbot,
		 	parecer->>'Username' as parecer_analista,
		 	parecer->>'CreditOpinionSuggestionType' as parecer_decisao,
		 	parecer->>'NewAmount' as parecer_limite,
		 	parecer->>'NewInterestRate' as parecer_taxa,
		 	parecer->>'NewTenure' as parecer_prazo,
		 	parecer->>'Status' as parecer_status
		from public.sync_credit_analysis as t1,
			jsonb_array_elements(t1.data_output->'CreditOpinions') as parecer) as t2
	where indice_analise = 1 and parecer_status = 'SUBMITTED' and lead_id  in (1576518)) as t3 on t3.lead_id = t1.legacy_target_id
left join (select 
		dp.direct_prospect_id,
		t1.*, 
		row_number() over (partition by t1.loan_request_id order by t1.criado_em desc) as indice
	from backtests.modelo_automacao_mesa_log t1
	join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
	join public.offers o on o.offer_id = lr.offer_id
	join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	where dp.direct_prospect_id  in (1576518)) as t4 on t4.direct_prospect_id = t1.legacy_target_id and t4.indice = 1
left join (select 
		dp.direct_prospect_id,
		t1.*,
		row_number() over (partition by t1.loan_request_id order by t1.criado_em desc) as indice		
	from backtests.automacao_mesa_politica_log t1
	join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
	join public.offers o on o.offer_id = lr.offer_id
	join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	where dp.direct_prospect_id in (1576518)) as t5 on t5.direct_prospect_id = t1.legacy_target_id and t5.indice = 1
where t1.legacy_target_id in (1576518)	




select distinct direct_prospect_id || ',',loan_application_id 
from public.direct_prospects dp 
where loan_application_id in ('9d943b3b-276f-4661-a821-ae2701112d9f',
'c638ec49-5a8b-43b9-aac2-ae290134d388',
'74bad9ab-8771-4124-8dcd-ae22016dd4e1',
'10df80ae-d0dc-437b-867b-ae2a00be8e23',
'380468c0-c041-4682-82a2-ae2700f96cd0',
'05844523-e514-4db6-baa8-ae2600d4d6c3',
'8724628f-5af0-48a9-8f5d-ae2a009a8b7c',
'b320a75e-d5fd-40e3-b48e-ae2701677a37',
'3dc76b63-e0af-4772-bb27-ae24017280c3',
'edd2c5c2-276c-499c-99cf-ae2a013afa3b',
'2f12416c-30b4-49bc-a31e-ae290113242d',
'f77d6f9b-c7cf-435e-9789-ae2a0111af3e',
'522a9382-1a00-467b-a0ee-ae2a012b5197',
'ec7df3f2-f32d-4ff1-93c8-ae2500f26c89',
'418ff1c0-c2c5-4428-8e25-ae2a00dfddcf',
'a6ad21a0-c8d2-4e16-b393-ae27015a6d50',
'9b4b99ab-40f7-432e-aa02-ae1d00c86fef',
'e612506e-9231-4edc-b45f-ae2a00cc8251',
'12fbf476-dd78-4de5-991a-ae29011c67a5',
'1f9b34d5-cc70-4b3c-b2cc-ae29011ef9f0',
'b8eb1177-318b-4635-a664-ae28010054e2',
'2f063018-e050-4a1f-be7a-ae2a00b9c023',
'7f63b21c-dc05-4f4e-a77e-ae1700e46677',
'f62a09bc-025b-4a8a-8451-ae2900e0bc48',
'f99eab02-dbf7-455c-8838-ae1a01026733',
'99601164-8f03-47a6-90bb-ae23012d3ad2',
'0a238fd0-c18f-40b0-b51a-ae2800f4774c',
'27506f06-4643-4be8-b7a7-ae27013ded77',
'3676c002-3584-4ce6-a0dd-ae2900ffe0da',
'f6e72a2b-eff8-4e23-abef-ae2100e7f6d6',
'16180577-f15b-4ac9-ba68-ae29014c49ff',
'b37c8da8-fd8e-474d-a2c7-ae24012dd4c3',
'aef72848-81ff-4a0b-9710-ae1c012609c4',
'432a4ab7-cdb6-44f4-8405-ae280118f2bf',
'ab134115-fafe-47cf-ba30-ae2600e0f762',
'25cbedc3-3e4a-4177-91ca-ae1f017d3ffb',
'3ffba728-a019-4662-9b25-adcb010fcb9c',
'3a854b54-62fd-493f-b62a-ae2700e11803',
'9a67806b-7ead-431f-ae4a-ae20012d8803',
'dbd4dbcd-7a8d-4f8b-be1a-ae170088b0f0',
'60fff70b-7898-4370-9ac4-ae15003c7b33',
'55002185-a1e1-41c3-97ea-ae1c0088b210',
'7231ca54-93da-486b-9754-ae2001644b99',
'5b62e30b-1a0e-4fac-ba72-ae2201889d1e',
'eeb55b52-f8b5-407c-a55a-ae24013d8c21',
'7b22f60d-8e02-49b7-aa2c-ae2100361875',
'd87e3a52-8d5b-4333-ad3c-ae2300efcd13',
'd185cce3-dc2c-4694-9e68-ae200155c01a',
'30858cd3-b5ba-4252-bbe5-ae2800f6ed63',
'06a947ce-c0b0-452c-8731-ae1d00fe6570',
'b77c0535-afe2-4006-8f19-ae1c0106e6aa',
'a2bc01d5-ec99-4a6e-a08f-ae28013c40a2',
'06a947ce-c0b0-452c-8731-ae1d00fe6570',
'6e692360-1b2e-425d-a363-ae29009fc672',
'0f160035-43a3-4f45-b6b5-ae2000846ae0',
'5892b35c-0f00-4b04-9065-ae29000b23f5',
'c30e70cc-8e66-415c-8405-ae280123ab73',
'6659ed31-1b26-430c-abc3-ae28013e1b19',
'227bf6f3-0334-4e27-b88a-ae27015cba0f',
'ef155373-6953-4ac7-9116-ae28013c7464',
'da79b743-2554-4000-b422-ae2700eab221',
'c32c25c9-c147-4112-a07f-ae2801475883',
'11363494-6c91-47f1-8346-ae1e00bdee34',
'ef155373-6953-4ac7-9116-ae28013c7464',
'b02cc35d-79f7-4e0d-8112-ae2801872b2c',
'92a16950-b732-4c39-998f-ae2800062f86',
'f63b9b1b-3928-4dfd-a742-ae2600dce074',
'ae9e40eb-1001-4f39-ab01-ae28013fa8f1',
'a6eb246e-24de-4d8a-b410-ae270157ab54',
'09d9da66-082a-4b58-b9ca-ae1f013f0c77',
'f8ea48a4-6ce7-46fa-97b5-ae2201512fda',
'b06e3b78-7c1c-4a63-8397-ae26015afb4d',
'0151b4a6-d9de-4628-90e9-ae2301305417',
'cf152962-a59f-47d5-88c6-ae21010dfeb4',
'52b99cf7-ecdd-4384-97a2-ae20013ebb40',
'f7869552-c122-438a-b512-ae1a014e12f3')

select *
from public.credito_coleta cc 
where origem_id = 1508039
