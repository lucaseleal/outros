select to_date(to_char(p.created_on,'Mon/yy'),'Mon/yy'),count(*)
from financial_connectors.payloads p
--join docparser_statements ds on ds.payload_id = p.id
where connector = 'doc-parser' and action = 'statement-parsed'
group by 1
order by 1

select to_date(to_char(p.created_on,'Mon/yy'),'Mon/yy'),count(*)
from financial_connectors.payloads p
--join docparser_statements ds on ds.payload_id = p.id
where connector = 'doc-parser' and action = 'statement-parsed'
group by 1
order by 1


select case when payload::jsonb ->> 'remote_id' = '' then 'e-mail integration' else 'webhook integration' end integration_type,
	count(*) filter (where created_on between '2019-08-12' and '2019-09-11') as aug_sep, 
	count(*) filter (where created_on between '2019-09-12' and '2019-10-11') as sep_oct, 
	count(*) filter (where created_on between '2019-10-12' and '2019-11-11') as oct_nov, 
	count(*) filter (where created_on between '2019-11-12' and '2019-12-11') as nov_dec
from financial_connectors.payloads 
where connector = 'doc-parser' and type = 'WEBHOOK_CALL'
group by 1
order by 1

select payload::jsonb -> 'docParserAccount' ->> 'id' as id,
	count(*) filter (where created_on between '2019-08-12' and '2019-09-11') as aug_sep, 
	count(*) filter (where created_on between '2019-09-12' and '2019-10-11') as sep_oct, 
	count(*) filter (where created_on between '2019-10-12' and '2019-11-11') as oct_nov, 
	count(*) filter (where created_on between '2019-11-12' and '2019-12-11') as nov_dec 
from financial_connectors.payloads p
--join docparser_statements ds on ds.payload_id = p.id
where connector = 'doc-parser' and action = 'parse-statement'
group by 1
order by 1


select distinct type
from financial_connectors.payloads p
--join docparser_statements ds on ds.payload_id = p.id
where connector = 'doc-parser' --and type = 'WEBHOOK_CALL'



select left((payload::jsonb -> 'document' ->> 'remote_id')::text,14)
from financial_connectors.payloads p
where connector = 'doc-parser' and action = 'parse-statement-result'
