select 
	dp.client_id empresa_id,
	dp.state empresa_estado,
	lr.loan_request_id as contrato_id,
	lr.loan_date as contrato_data,
	lr.taxa_final as contrato_taxa,
	li.monthly_cet as contrato_cet,
	lr.number_of_installments as contrato_termo_original,
	lt.collection_loan_current_term as contrato_termo_corrente,
	li.total_payment as contrato_valor_financiado,
	row_number () over (partition by lr.loan_request_id order by fp.vencimento_em) as parcela_numero_vencimento,
	fp.cobrado as parcela_valor,
	fp.vencimento_em as parcela_data_vencimento,
	row_number () over (partition by lr.loan_request_id order by fp.pago_em,fp.vencimento_em) as parcela_numero_pagamento,
	fp.pago_em as parcela_data_pagamento,
	fp.pago as parcela_valor_pago,
	greatest(fp.pago - fp.cobrado,0) as parcela_multa_atraso,
	coalesce(fb2.multa_pagamento_atrasado,0) + coalesce(fb2.juros_pagamento_atrasado,0) as parcela_multa_juros_atraso,
	fp.desconto as parcela_desconto,
	dense_rank() over (partition by lr.loan_request_id order by fpp.id) as plano_indice,
	fpp.type as plano_tipo,
	coalesce(fpp.creation_date,fpp.inicio_em)::date as plano_data_criacao,
	lt.lead_classe_bizux as contrato_classe_rating_antigo,
	dp.bizu_combo_zone_number as contrato_ranking_rating_novo,
	case 
		when dp.month_revenue * 12 <= 81000 then 'MEI'
		when dp.month_revenue * 12 <= 360000 then 'ME'
		when dp.month_revenue * 12 <= 4800000 then 'EPP'
		when dp.month_revenue * 12 <= 20000000 then 'EMP'
		when dp.month_revenue * 12 > 20000000 then 'EGP'
	end as empresa_faturamento_classe,
	coalesce(fb.balanco_final_pro_rata) as contrato_saldo_devedor_31_05,
	case 
		when coalesce(fb.portfolio_id,lr.portfolio_id) in (7,8) then 'FIDC'
		when coalesce(fb.portfolio_id,lr.portfolio_id) not in (7,8) then 'SECURITIZADORA'
	end as contrato_portfolio	
from public.financeiro_parcela fp 
join public.financeiro_planopagamento fpp on fpp.id = fp.plano_id 
join public.loan_requests lr on lr.loan_request_id = fpp.emprestimo_id 
join public.offers o on o.offer_id = lr.offer_id 
join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id 
join public.loan_infos li on li.loan_info_id = lr.loan_request_id 
join data_science.loan_tape lt on lt.loan_request_id = lr.loan_request_id 
left join public.financial_balance fb on fb.emprestimo_id = lr.loan_request_id and fb.dia_do_indicador = '2021-05-31'::date
left join public.financial_balance fb2 on fb2.emprestimo_id = lr.loan_request_id and fb2.dia_do_indicador = fp.pago_em::date
where fp.status not in ('Cancelada','EmEspera') and lr.loan_date between '2017-06-01'::date and '2021-05-31'::date and lr.portfolio_id not in (9,10,12,13)
--	and lr.loan_request_id = 100


select 
	date_trunc('month',loan_contract_date)::date,
--	lt.neoway_company_state,
--	loan_request_id,
	round(sum(loan_final_net_interest_rate::numeric * loan_gross_value) / sum(loan_gross_value),2)
from data_science.lead_tape lt 
where loan_is_loan = 1 
	and loan_contract_date between '2017-06-01'::date and '2021-05-31'::date 
--	and date_trunc('month',loan_contract_date)::date = '2021-05-01'
	and loan_portfolio_id not in ('9','10','12','13')
group by 1--,2

