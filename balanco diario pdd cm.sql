insert into lucas_leal.t_balanco_pdd_cm_full
(SELECT 
	bd.dia_do_indicador,
	date_part('isodow'::text, bd.dia_do_indicador)::integer AS dia_da_semana,
	bd.emprestimo_id,
	bd.balanco_final_pro_rata,
	CASE WHEN bd.renegociacoes = 0 AND aux_atraso.tipo = 'ORIGINAL'::text THEN bd.dias_atraso ELSE aux_atraso.atraso END AS atraso_cm,
	aux_atraso.tipo AS tipo_plano_cm,
	bd.renegociacoes,
	bd.juros_taxa,
	bd.taxa_interna_retorno,
	bd.portfolio_id,
	bd.status
FROM financial_balance bd
LEFT JOIN ( SELECT to_date(to_char(workdays.dia::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text) AS to_date,
		max(workdays.dia) AS max_dia
	FROM workdays
	GROUP BY (to_date(to_char(workdays.dia::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text))) w ON w.max_dia = bd.dia_do_indicador
JOIN ( SELECT t1.dia_do_indicador,
		t1.emprestimo_id,
		t1.tipo,
		GREATEST(COALESCE(t1.dia_do_indicador - min(t1.vencimento_em) FILTER (WHERE t1.data_pagamento IS NULL), 0), 0) AS atraso
	FROM (SELECT fp_original.dia_do_indicador,
			'ORIGINAL'::text AS tipo,
			fp_original.emprestimo_id,
			fp_original.vencimento_em,
			max(fp_original.cum_cobrado) AS cum_cobrado,
			min(fp_corrente.pago_em) AS data_pagamento,
			min(fp_corrente.cum_pago) AS total_pago
		FROM ( SELECT bd_1.dia_do_indicador,
				fpp.emprestimo_id,
				fp.numero,
				fp.vencimento_em,
				sum(case fp.status when 'PagoAdiantado' then fp.pago else fp.cobrado end) OVER (PARTITION BY bd_1.dia_do_indicador, fpp.emprestimo_id ORDER BY fp.vencimento_em) AS cum_cobrado
			FROM financeiro_planopagamento fpp
			JOIN financeiro_parcela fp ON fp.plano_id = fpp.id
			JOIN financial_balance bd_1 ON bd_1.emprestimo_id = fpp.emprestimo_id
			LEFT JOIN ( SELECT fpp_1.emprestimo_id,
					fpp_1.creation_date::date AS creation_date
				FROM financeiro_planopagamento fpp_1
				WHERE fpp_1.type::text = 'POSTPONING'::text AND (fpp_1.status::text <> ALL (ARRAY['Cancelado'::character varying, 'EmEspera'::character varying]::text[]))) fpp_post ON fpp_post.emprestimo_id = fpp.emprestimo_id AND fpp_post.creation_date < bd_1.dia_do_indicador
			WHERE fpp.type::text = 'ORIGINAL'::text AND fpp_post.emprestimo_id IS null and bd_1.dia_do_indicador > (select max(dia_do_indicador) from t_balanco_pdd_cm_full)) fp_original
		LEFT JOIN ( SELECT bd_1.dia_do_indicador,
			fpp.emprestimo_id,
			fp.pago_em,
			sum(fp.pago) OVER (PARTITION BY bd_1.dia_do_indicador, fpp.emprestimo_id ORDER BY fp.pago_em) AS cum_pago
		FROM financeiro_parcela fp
		JOIN financeiro_planopagamento fpp ON fpp.id = fp.plano_id
		JOIN financial_balance bd_1 ON bd_1.emprestimo_id = fpp.emprestimo_id
		WHERE fp.status::text ~~ 'Pago%'::text AND fp.pago_em < bd_1.dia_do_indicador and bd_1.dia_do_indicador > (select max(dia_do_indicador) from t_balanco_pdd_cm_full)) fp_corrente ON fp_corrente.emprestimo_id = fp_original.emprestimo_id AND fp_corrente.cum_pago >= fp_original.cum_cobrado AND fp_corrente.dia_do_indicador = fp_original.dia_do_indicador
		GROUP BY fp_original.dia_do_indicador, 'ORIGINAL'::text, fp_original.emprestimo_id, fp_original.vencimento_em
	UNION
		SELECT postponing.dia_do_indicador,
			'POSTPONING'::text AS tipo,
			postponing.emprestimo_id,
			postponing.vencimento_em,
			max(postponing.cum_cobrado) AS cum_cobrado,
			min(fp_postponing.pago_em) AS data_pagamento,
			min(fp_postponing.cum_pago) AS total_pago
		FROM ( SELECT bd_1.dia_do_indicador,
				fpp.emprestimo_id,
				fp.numero,
				fp.vencimento_em,
				sum(case fp.status when 'PagoAdiantado' then fp.pago else fp.cobrado end) OVER (PARTITION BY bd_1.dia_do_indicador, fp.plano_id ORDER BY fp.vencimento_em) AS cum_cobrado
			FROM financeiro_planopagamento fpp
			JOIN financeiro_parcela fp ON fp.plano_id = fpp.id
			JOIN financial_balance bd_1 ON bd_1.emprestimo_id = fpp.emprestimo_id
			WHERE fpp.type::text = 'POSTPONING'::text AND (fpp.status::text <> ALL (ARRAY['Cancelado'::character varying, 'EmEspera'::character varying]::text[])) AND fpp.creation_date::date < bd_1.dia_do_indicador
				 and bd_1.dia_do_indicador > (select max(dia_do_indicador) from t_balanco_pdd_cm_full)) postponing
		LEFT JOIN ( SELECT bd_1.dia_do_indicador,
				fpp.emprestimo_id,
				fp.pago_em,
				sum(fp.pago) OVER (PARTITION BY bd_1.dia_do_indicador, fpp.emprestimo_id ORDER BY fp.pago_em) AS cum_pago
			FROM financeiro_parcela fp
		JOIN financeiro_planopagamento fpp ON fpp.id = fp.plano_id
		JOIN financial_balance bd_1 ON bd_1.emprestimo_id = fpp.emprestimo_id
		JOIN ( SELECT financeiro_planopagamento.emprestimo_id,
				financeiro_planopagamento.creation_date
			FROM financeiro_planopagamento
			WHERE financeiro_planopagamento.type::text = 'POSTPONING'::text AND (financeiro_planopagamento.status::text <> ALL (ARRAY['Cancelado'::character varying, 'EmEspera'::character varying]::text[]))) fpp_post ON fpp_post.emprestimo_id = fpp.emprestimo_id
			WHERE fp.status::text ~~ 'Pago%'::text AND fp.pago_em > fpp_post.creation_date::date and bd_1.dia_do_indicador > (select max(dia_do_indicador) from t_balanco_pdd_cm_full)) fp_postponing ON fp_postponing.emprestimo_id = postponing.emprestimo_id AND fp_postponing.cum_pago >= postponing.cum_cobrado AND fp_postponing.dia_do_indicador = postponing.dia_do_indicador
			GROUP BY postponing.dia_do_indicador, 'POSTPONING'::text, postponing.emprestimo_id, postponing.vencimento_em
			) t1
		GROUP BY t1.dia_do_indicador, t1.emprestimo_id, t1.tipo) aux_atraso ON aux_atraso.emprestimo_id = bd.emprestimo_id AND aux_atraso.dia_do_indicador = bd.dia_do_indicador
where bd.dia_do_indicador > (select max(dia_do_indicador) from t_balanco_pdd_cm_full))

