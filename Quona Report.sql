--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
------------------------------------------------NOVO RELATORIO------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

--APR
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as "Month",
	(1 + sum(coalesce(lr.taxa_final,o.interest_rate) / 100 * li.total_payment) / sum(li.total_payment)) ^12 -1 as "APR",
	(1 + sum(li.monthly_cet / 100 * li.total_payment) / sum(li.total_payment)) ^12 -1 as "Total APR (w/ tax and commission)"
--------------------------
from offers o
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and lr.contrato_assinado_url is not null and (to_char(lr.loan_date,'yyyy') || '-' || to_char(lr.loan_date,'Q') || '-01')::date < 
	(to_char(current_date,'yyyy') || '-' || to_char(current_date,'Q') || '-01')::date
group by 1
order by 1

--APR e2020
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as "Month",
	(1 + sum(coalesce(lr.taxa_final,o.interest_rate) / 100 * li.total_payment) / sum(li.total_payment)) ^12 -1 as "APR",
	(1 + sum(li.monthly_cet / 100 * li.total_payment) / sum(li.total_payment)) ^12 -1 as "Total APR (w/ tax and commission)"
--------------------------
from offers o
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and lr.contrato_assinado_url is not null and (to_char(lr.loan_date,'yyyy') || '-' || to_char(lr.loan_date,'Q') || '-01')::date < 
	(to_char(current_date,'yyyy') || '-' || to_char(current_date,'Q') || '-01')::date and lr.portfolio_id = '9'
group by 1
order by 1

--Loan portfolio
select w.mes as "Month",
	sum(balanco_final_pro_rata) as "Gross Loan Portfolio"
from public.financial_balance
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy') as mes,max(dia) as dia from workdays group by 1) w on w.dia = dia_do_indicador
where case when renegociacoes = 0 then dias_atraso else dias_atraso_max end <= 360 
group by 1
order by 1
	
--FPD
select 
	mes as "Month",
	case when current_date - max(original_1st_pmt_due_to) > 30 then coalesce(sum(original_total_payment),0) end as total_p_plus_i,
	case when current_date - max(original_1st_pmt_due_to) > 30 then coalesce(sum(original_total_payment) filter (where fpd_by > 30),0) end as total_fpd_30,
	case when current_date - max(original_1st_pmt_due_to) > 60 then coalesce(sum(original_total_payment) filter (where fpd_by > 60),0) end as total_fpd_60,
	case when current_date - max(original_1st_pmt_due_to) > 90 then coalesce(sum(original_total_payment) filter (where fpd_by > 90),0) end as total_fpd_90
from(select 
		lr.loan_request_id,
		lr.loan_date,
		to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as mes,
		coalesce(atraso.atraso,	coalesce(first_paid_inst_paid_on,current_date) - original_1st_pmt_due_to -1) as fpd_by,
		Financeiro_original.original_total_payment,
		Financeiro_original.original_1st_pmt_due_to
	from loan_requests lr
	join (select fpp.emprestimo_id, 
			sum(fp.cobrado) as original_total_payment,
			max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to
		from financeiro_planopagamento fpp  
		join financeiro_parcela fp on fp.plano_id = fpp.id
		where fpp."type" = 'ORIGINAL'
		group by 1) as Financeiro_original on Financeiro_original.emprestimo_id = lr.loan_request_id
	left join(select
			mh.emprestimo_id,
			mh.maxatraso as atraso
		from metabase.historico mh
		join(select
				fpp.emprestimo_id,
				coalesce(min(fp.pago_em),current_date) as dt_pgto
			from public.financeiro_planopagamento fpp
			left join public.financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as pgto on pgto.emprestimo_id = mh.emprestimo_id and pgto.dt_pgto = mh.dt) as atraso on atraso.emprestimo_id = lr.loan_request_id
	left join(select 
			lr.loan_request_id, 
			min(fp.pago_em) as first_paid_inst_paid_on
		from loan_requests lr  
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		group by 1) as Financeiro on Financeiro.loan_request_id = lr.loan_request_id	
	where lr.status = 'ACCEPTED' and lr.contrato_assinado_url is not null and original_1st_pmt_due_to < current_date) as t2
group by 1
order by 1

--PAR
select
	w.mes as "Month",
	coalesce(sum(fbd.balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end > 30),0) / sum(fbd.balanco_final_pro_rata) as "% PAR 30",
	coalesce(sum(fbd.balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end > 60),0) / sum(fbd.balanco_final_pro_rata) as "% PAR 60",
	coalesce(sum(fbd.balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end > 90),0) / sum(fbd.balanco_final_pro_rata) as "% PAR 90"
from public.financial_balance fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy') as mes,max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
--join (select to_char(dia,'yyyy') || '_Q' || to_char(dia,'Q') as "quarter",max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end <= 180
group by 1
order by 1

--Write-off
select 
	w.mes as "Month",
	coalesce(sum(fbd.balanco_final_pro_rata) filter (where case when fbd.renegociacoes > 0 then fbd.dias_atraso_max else fbd.dias_atraso end > 360),0) as "Write Off",
	coalesce(sum(fbd.balanco_final_pro_rata) filter (where case when fbd.renegociacoes > 0 then fbd.dias_atraso_max else fbd.dias_atraso end > 360),0) /
	sum(fbd.balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end <= 360) as "% Write Off"
from public.financial_balance fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy') as mes,max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
group by 1
order by 1

--Sole Proprietors
select 
	to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as "Month",
	count(*) filter (where signers.count_partners = 1) as sole_proprietor_company_borrowed,
	count(*) as loans_originated
--------------------------
from loan_requests lr
join(select lr.loan_request_id,count(*) as count_partners
	from signers s
	join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
	join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
	where s.share_percentage > 0
	group by 1) as signers on signers.loan_request_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and lr.contrato_assinado_url is not null and (to_char(lr.loan_date,'yyyy') || '-' || to_char(lr.loan_date,'Q') || '-01')::date < 
	(to_char(current_date,'yyyy') || '-' || to_char(current_date,'Q') || '-01')::date
group by 1
order by 1


