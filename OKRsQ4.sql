
--LUCAS
--B MACRO
--B.1 Aumentar a origina��o em 73% atingindo R$150 milh�es 
select 
	round(sum(lt.loan_gross_value)::numeric/1000000,2) as origination
from data_science.loan_tape lt
where loan_contract_date >= '2021-10-01' and loan_contract_date < '2022-01-01'
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )

--B.2 Aumentar o n�mero de empr�stimos 30D de 340 para 500
select 
	count(*) as units
from data_science.loan_tape lt
where loan_contract_date >= CURRENT_DATE - 30  and loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )

--B.3 Aumentar a taxa m�dia 30D de 2,79% para 2,99%
select 
	round(sum(lt.loan_gross_value * lt.loan_final_net_interest_rate)::numeric / sum(lt.loan_gross_value),2) as avg_ir
from data_science.loan_tape lt
where loan_contract_date >= CURRENT_DATE - 30  and loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )

--B.4 Aumentar a aprova��o Lead to Loan 30D de 0,55% para 0,75%
select 
	100 * cast( sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE and loan_is_loan = 1 then 1 else 0 end) as real)/ sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as conversion_units,
	100 * cast(sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE and loan_is_loan = 1 then lt.loan_gross_value else 0 end) as real)/sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as conversion_dollars
from data_science.lead_tape lt
where ( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' ) and lead_is_mca = 0
	and lead_outbound_loan_application_id is null and lt.loan_is_renewal = 0

--B.5 Aumentar o Share de Emprestimos BNDES de 40% para 60%
select 
	sum(lt.loan_gross_value) filter (where lt.loan_portfolio_id = '14') as origination_fidc_bndes, 
	count(*) filter (where lt.loan_portfolio_id = '14') as units_fidc_bndes,
	sum(lt.loan_gross_value) filter (where lt.loan_portfolio_id = '14') / sum(lt.loan_gross_value)::numeric as perc_origination_fidc_bndes, 
	count(*) filter (where lt.loan_portfolio_id = '14') / count(*)::numeric as perc_units_fidc_bndes
from data_science.loan_tape lt 
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE 
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )

--1	Aumentar a rentabilidade dos empr�stimos	
--1.1 Aumentar o EP de 13,5% para 16%
select 
	sum(ltf.loan_final_net_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_rate,
	sum(ltf.loan_final_gross_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_gross_rate,
    sum(ltf.loan_original_final_term * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_term,
    sum(case when ltf.loan_is_renewal = 0 then 
    		case when lt.lead_biz_prime_matriz = 1 then 0.085 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 2 then 0.126 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 3 then 0.139 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 4 then 0.165 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 5 then 0.144 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 6 then 0.194 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 7 then 0.224 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 8 then 0.149 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 9 then 0.239 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 10 then 0.289 * ltf.collection_original_p_plus_i 
      else 0.165 * ltf.collection_original_p_plus_i 
      end 
      when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end)
      / sum(ltf.collection_original_p_plus_i ) as el_combo,
	sum( case when ltf.loan_is_renewal = 0 then 
      case when lt.lead_mesa_score_ranking is null then 0.154 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 1 then 0.071 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 2 then 0.141 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 3 then 0.124 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 4 then 0.168 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 5 then 0.161 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 6 then 0.265 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 7 then 0.213 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 8 then 0.256 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 9 then 0.314 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 10 then 0.384 * ltf.collection_original_p_plus_i 
      else 0.154 * ltf.collection_original_p_plus_i 
      end when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end) / sum(ltf.collection_original_p_plus_i ) as el_mesa,
      sum( ltf.loan_gross_value) as loans_money,
      count( ltf.loan_gross_value) as loans_unit,
      sum( ltf.collection_original_p_plus_i ) as p_plus_i
from data_science.loan_tape ltf  
left join data_science.lead_tape lt on ltf.lead_id = lt.lead_id
where ltf.loan_contract_date >= '2021-10-01' and ltf.loan_contract_date < '2022-01-01' 
	and( ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020' )

--1.2 Manter a convers�o de O2L prime acima de 13%
select 
	case when sum(lt.final_offer_max_value ) = 0 then 0 else sum( case when (lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE ) then lt.loan_gross_value else 0 end )::numeric / sum(lt.final_offer_max_value)::numeric end as conv_o2l_value,
	case when count(lt.final_offer_max_value) = 0 then 0 else count (*) filter (where lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE  )::numeric / count(lt.final_offer_max_value)::numeric end as conv_o2l_unit
from data_science.lead_tape lt 
left join direct_prospects dp on dp.direct_prospect_id = lt.lead_id
where lead_opt_in_date::date < CURRENT_DATE -15 and lead_opt_in_date::date >= CURRENT_DATE -45
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.initial_offer_id is not null
	and lt.lead_outbound_loan_application_id is null
	and lead_biz_prime_matriz <= 5 
	and loan_is_renewal = 0
	
	
--1.3 Aumentar o ticket m�dio de renova��o 30D em 35% de R$63k para R$85k
select 
	sum(lt.loan_gross_value) as origination, count(*) as units, sum(lt.loan_gross_value) / count(*) as avg_ticket,
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir
from data_science.loan_tape lt
where loan_contract_date >= CURRENT_DATE - 30 and loan_contract_date < CURRENT_DATE
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.loan_is_renewal > 0 
	
	
--1.4 Diminuir o custo de dados para novos clientes de 1% para 0,7% 
--	?????
	
	
--1.5 Diminuir o FPD30 90D de 4,0% para 3,5%
select 
	(sum( case when ltf.collection_pmt_of_over30 = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) )  as fpd_money,
	(sum( case when ltf.collection_pmt_of_over30 = 1 then 1.0 else 0.0 end ) / count(*) )  as fpd_unit,
	count(*) as size
from lucas_leal.loan_tape_full ltf 
where ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020'  
	and ( ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) <= 120 and ( CURRENT_DATE  - ltf.loan_1st_pmt_due_date ) >= 30 )
	
	
--1.6 Diminuir o FPD30 sem contato em dinheiro 90D de 0,8% para 0,6%
select 
(sum( case when ltf.collection_pmt_of_over30 = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money,	 
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_cc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 0 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_sc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is NULL then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_null,
(sum( case when ltf.collection_pmt_of_over30 = 1 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 1 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_cc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 0 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_sc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is null then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_null,
sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is null then 1.0 else 0.0 end )  as non_classified,
count(*) as size,max(ltf.loan_contract_date ), min(ltf.loan_contract_date )
from lucas_leal.loan_tape_full ltf 
left join wellerson_silva.t_anexo_sem_contato_cob sc on sc.loan_request_id = ltf.loan_request_id 
where ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020'  
	and ( ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) <= 120 and ( CURRENT_DATE  - ltf.loan_1st_pmt_due_date ) >= 30 )


--2	Aumentar a aprova��o no funil em units	
--2.1	Aumentar a aprova��o de Lead to Offer (ex outbound) 30D de 4,3% para 6,5%
select 
	100* cast( sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then 1 else 0 end) as real)/ sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as conversion_units
--	100* cast(sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then lt.initial_offer_max_value else 0 end) as real)/sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as conversion_dollars
from data_science.lead_tape lt
where ( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.lead_outbound_loan_application_id is null
	
--2.2	Aumentar a aprova��o de All Docs to Loan 30D de 47,5% para 55%
select 
	100 * cast( sum( case when lt.loan_is_loan = 1  then 1 else 0 end) as real)/ sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as conversion_units
from data_science.lead_tape lt
where lead_is_caas = 0 and lead_is_mca = 0
	and lt.lead_opt_in_date::date between CURRENT_DATE - 45 and CURRENT_DATE - 16


	
--2.3	Diminuir a rejei��o na mesa 30D de novos clientes em units de 49% para 38%	
select 
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado') then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then 1 else 0 end) as reject_ratio_2_units
from data_science.lead_tape lt
where lead_is_caas = 0 and lead_is_mca = 0
	and lt.lead_opt_in_date::date between CURRENT_DATE - 45 and CURRENT_DATE - 16
	and lt.loan_is_renewal = 0
	
--3	Aumentar a origina��o de clientes near-prime
--3.1	Diminuir a rejei��o na mesa 30D de novos clientes near-prime de 77% para 50%
select 
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado')
	    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then 1 else 0 end) as reject_ratio_2_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    	then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then lt.loan_requested_value else 0 end) as reject_ratio_2_dollar
	from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.lead_opt_in_date >= CURRENT_DATE - 45 and lt.lead_opt_in_date < CURRENT_DATE - 15
	and lt.lead_biz_prime_matriz > 5 
	and lt.loan_is_renewal = 0
	
--3.2	Aumentar a convers�o O2L de near-prime de 4,8% para 10%
select 
	sum(lt.loan_gross_value) filter (where loan_is_loan = 1 and loan_contract_date < current_date)::numeric 
		/ sum(lt.final_offer_max_value)::numeric as conv_o2l_value
from data_science.lead_tape lt 
where lead_opt_in_date::date < CURRENT_DATE -15
	and lead_opt_in_date::date >= CURRENT_DATE -45
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.initial_offer_id is not null
	and lt.lead_outbound_loan_application_id is null 
	and lead_biz_prime_matriz > 5 
	and loan_is_renewal = 0
	
--3.3	Atingir uma rentabilidade m�nima de 20% nos empr�stimos near-prime	
select 
	sum(ltf.loan_final_net_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_rate,
	sum(ltf.loan_final_gross_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_gross_rate,
    sum(ltf.loan_original_final_term * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_term,
    sum(case when ltf.loan_is_renewal = 0 then 
    		case when lt.lead_biz_prime_matriz = 1 then 0.085 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 2 then 0.126 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 3 then 0.139 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 4 then 0.165 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 5 then 0.144 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 6 then 0.194 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 7 then 0.224 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 8 then 0.149 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 9 then 0.239 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 10 then 0.289 * ltf.collection_original_p_plus_i 
      else 0.165 * ltf.collection_original_p_plus_i 
      end 
      when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end)
      / sum(ltf.collection_original_p_plus_i ) as el_combo,
      sum( case when ltf.loan_is_renewal = 0 then 
      case when lt.lead_mesa_score_ranking is null then 0.154 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 1 then 0.071 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 2 then 0.141 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 3 then 0.124 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 4 then 0.168 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 5 then 0.161 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 6 then 0.265 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 7 then 0.213 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 8 then 0.256 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 9 then 0.314 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 10 then 0.384 * ltf.collection_original_p_plus_i 
      else 0.154 * ltf.collection_original_p_plus_i 
      end when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end) / sum(ltf.collection_original_p_plus_i ) as el_mesa,
      sum( ltf.loan_gross_value) as loans_money,
      count( ltf.loan_gross_value) as loans_unit,
      sum( ltf.collection_original_p_plus_i ) as p_plus_i
from data_science.loan_tape ltf  left join data_science.lead_tape lt on ltf.lead_id = lt.lead_id
where ltf.loan_contract_date >= '2021-10-01' and ltf.loan_contract_date < '2022-01-01' 
	and( ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020' )
	and lt.lead_biz_prime_matriz > 5 
	and ltf.loan_is_renewal = 0	
	
--4	Aumentar a produ��o no FIDC BNDES
--4.1	Aumentar a origina��o 30D do FIDC BNDES de 12 para 33 milh�es
select 
	sum(lt.loan_gross_value) as origination
from data_science.loan_tape lt left join data_science.lead_tape lt2 on lt2.lead_id = lt.lead_id
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.loan_portfolio_id = '14'
	
--4.2	Aumentar a origina��o 30D do FIDC BNDES de 70 para 130 units
select 
	count(*) as units
from data_science.loan_tape lt left join data_science.lead_tape lt2 on lt2.lead_id = lt.lead_id
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.loan_portfolio_id = '14'

--4.3	Aumentar o ticket m�dio 30D do FIDC BNDES de R$156k para R$250k
select 
	sum(lt.loan_gross_value) / count(*) as avg_ticket
from data_science.loan_tape lt left join data_science.lead_tape lt2 on lt2.lead_id = lt.lead_id
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.loan_portfolio_id = '14'

--4.4	Aumentar a taxa de juros m�dia 30D do FIDC BNDES de 2,46% para 2,60%
select 
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir
from data_science.loan_tape lt left join data_science.lead_tape lt2 on lt2.lead_id = lt.lead_id
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE
	and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
	and lt.loan_portfolio_id = '14'





/* Origination Q4*/
select 
	sum(lt.loan_gross_value) as origination, count(*) as units, sum(lt.loan_gross_value) / count(*) as avg_ticket,
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir
from data_science.loan_tape lt
where loan_contract_date >= '2021-10-01' and loan_contract_date < '2022-01-01'
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )


/* Units 30D & Interest Rate 30 D */
select 
	sum(lt.loan_gross_value) as origination, count(*) as units, sum(lt.loan_gross_value) / count(*) as avg_ticket,
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir
from data_science.loan_tape lt
where loan_contract_date >= CURRENT_DATE - 30  and loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )


/* Expected Profitability */
select 
	sum(ltf.loan_final_net_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_rate,
	sum(ltf.loan_final_gross_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_gross_rate,
    sum(ltf.loan_original_final_term * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_term,
    sum(case when ltf.loan_is_renewal = 0 then 
    		case when lt.lead_biz_prime_matriz = 1 then 0.085 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 2 then 0.126 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 3 then 0.139 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 4 then 0.165 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 5 then 0.144 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 6 then 0.194 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 7 then 0.224 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 8 then 0.149 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 9 then 0.239 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 10 then 0.289 * ltf.collection_original_p_plus_i 
      else 0.165 * ltf.collection_original_p_plus_i 
      end 
      when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end)
      / sum(ltf.collection_original_p_plus_i ) as el_combo,
      sum( case when ltf.loan_is_renewal = 0 then 
      case when lt.lead_mesa_score_ranking is null then 0.154 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 1 then 0.071 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 2 then 0.141 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 3 then 0.124 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 4 then 0.168 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 5 then 0.161 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 6 then 0.265 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 7 then 0.213 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 8 then 0.256 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 9 then 0.314 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 10 then 0.384 * ltf.collection_original_p_plus_i 
      else 0.154 * ltf.collection_original_p_plus_i 
      end when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end) / sum(ltf.collection_original_p_plus_i ) as el_mesa,
      sum( ltf.loan_gross_value) as loans_money,
      count( ltf.loan_gross_value) as loans_unit,
      sum( ltf.collection_original_p_plus_i ) as p_plus_i
from data_science.loan_tape ltf  left join data_science.lead_tape lt on ltf.lead_id = lt.lead_id
where ltf.loan_contract_date >= '2021-10-01' and ltf.loan_contract_date < '2022-01-01' 
and( ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020' )

/* Conversao Funil 30D*/
select 
	case when  lt.lead_outbound_loan_application_id is not null then 'oubound' when lt.loan_is_renewal > 0 then 'renewal' else 'outbound' end as type,
	sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as leads, 
	sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as amount_requested,
	sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE then 1 else 0 end) as loans, 
	sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE then lt.loan_gross_value else 0 end) as origination,
	100* cast( sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE then 1 else 0 end) as real)/ sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as conversion_units,
	100* cast(sum( case when lt.loan_contract_date >= CURRENT_DATE - 30 and lt.loan_contract_date < CURRENT_DATE then lt.loan_gross_value else 0 end) as real)/sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as conversion_dollars
from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
group by 1

/* FPD30 90D*/
select 
	(case when ( ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) <= 120 and ( CURRENT_DATE  - ltf.loan_1st_pmt_due_date ) >= 30 ) then 1 
	when ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) < 30 then -1 else 0  end) as fpd_cohort,
	 (sum( case when ltf.collection_pmt_of_over30 = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) )  as fpd_money,
(sum( case when ltf.collection_pmt_of_over30 = 1 then 1.0 else 0.0 end ) / count(*) )  as fpd_unit,
count(*) as size,max(ltf.loan_contract_date ), min(ltf.loan_contract_date )
from lucas_leal.loan_tape_full ltf where ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020'  group by 1 order by 1

/* FPD30 Sem contato 90D*/
select 
(case when ( ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) <= 120 and ( CURRENT_DATE  - ltf.loan_1st_pmt_due_date ) >= 30 ) then 1 
	when ( CURRENT_DATE - ltf.loan_1st_pmt_due_date ) < 30 then -1 else 0  end) as fpd_cohort,
(sum( case when ltf.collection_pmt_of_over30 = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money,	 
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 1 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_cc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 0 then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_sc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is NULL then collection_original_p_plus_i else 0 end ) / sum( collection_original_p_plus_i  ) ) * 100 as fpd_money_null,
(sum( case when ltf.collection_pmt_of_over30 = 1 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 1 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_cc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato = 0 then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_sc,
(sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is null then 1.0 else 0.0 end ) / count(*) ) * 100 as fpd_unit_null,
sum( case when ltf.collection_pmt_of_over30 = 1 and sc.tem_contato is null then 1.0 else 0.0 end )  as non_classified,
count(*) as size,max(ltf.loan_contract_date ), min(ltf.loan_contract_date )
from lucas_leal.loan_tape_full ltf left join wellerson_silva.t_anexo_sem_contato_cob sc on sc.loan_request_id = ltf.loan_request_id 
where ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020'  
group by 1
order by 1

/* Ticket Medio Renewal*/
select 
	case when lt.loan_is_renewal > 0 then 1 else 0 end as renewal,
	sum(lt.loan_gross_value) as origination, count(*) as units, sum(lt.loan_gross_value) / count(*) as avg_ticket,
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir
from data_science.loan_tape lt
where loan_contract_date >= CURRENT_DATE - 30 and loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
group by 1


/* Conversao Lead to Offer 30D*/
select 
	case when  lt.lead_outbound_loan_application_id is null then 'regular' else 'outbound' end as type,
	sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as leads, 
	sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as amount_requested,
	sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then 1 else 0 end) as offers, 
	sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then lt.initial_offer_max_value else 0 end) as offers_value,
	100* cast( sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then 1 else 0 end) as real)/ sum(case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then 1 else 0 end) as conversion_units,
	100* cast(sum( case when lt.initial_offer_date >= CURRENT_DATE - 30 and lt.initial_offer_date < CURRENT_DATE then lt.initial_offer_max_value else 0 end) as real)/sum( case when lt.lead_opt_in_date >= CURRENT_DATE - 30 and lt.lead_opt_in_date < CURRENT_DATE then lt.lead_application_amount else 0 end) as conversion_dollars
from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
group by 1


/* Conversao AllDocs to Loan 30D*/
select 
	sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as all_docs, 
	sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as amount_requested,
	sum( case when lt.loan_is_loan = 1 then 1 else 0 end) as loans, 
	sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as origination,
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then 1 else 0 end) as rejected, 
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then lt.loan_requested_value else 0 end) as rejected_value,
	100 * cast( sum( case when lt.loan_is_loan = 1  then 1 else 0 end) as real)/ sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as conversion_units,
	100 * cast( sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as conversion_dollars,
	100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as lost_market_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as lost_market,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as reject_ratio_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as reject_ratio_dollar,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado')
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then 1 else 0 end) as reject_ratio_2_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then lt.loan_requested_value else 0 end) as reject_ratio_2_dollar
	from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.lead_opt_in_date >= CURRENT_DATE - 45  and lt.lead_opt_in_date < CURRENT_DATE - 15 


/* Rejeicao na mesa all */
select 
	sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as all_docs, 
	sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as amount_requested,
	sum( case when lt.loan_is_loan = 1 then 1 else 0 end) as loans, 
	sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as origination,
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then 1 else 0 end) as rejected, 
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then lt.loan_requested_value else 0 end) as rejected_value,
	100 * cast( sum( case when lt.loan_is_loan = 1  then 1 else 0 end) as real)/ sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as conversion_units,
	100 * cast( sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as conversion_dollars,
	100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as lost_market_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as lost_market,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as reject_ratio_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as reject_ratio_dollar,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado')
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then 1 else 0 end) as reject_ratio_2_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then lt.loan_requested_value else 0 end) as reject_ratio_2_dollar
	from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.lead_opt_in_date >= CURRENT_DATE - 45 and lt.lead_opt_in_date < CURRENT_DATE - 15
and lt.loan_is_renewal = 0


/* Rejeicao na mesa near-prime */
select 
	sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as all_docs, 
	sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as amount_requested,
	sum( case when lt.loan_is_loan = 1 then 1 else 0 end) as loans, 
	sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as origination,
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then 1 else 0 end) as rejected, 
	sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado' then lt.loan_requested_value else 0 end) as rejected_value,
	100 * cast( sum( case when lt.loan_is_loan = 1  then 1 else 0 end) as real)/ sum(case when  lt.loan_request_is_all_docs = 1 then 1 else 0 end) as conversion_units,
	100 * cast( sum( case when lt.loan_is_loan = 1 then lt.loan_gross_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as conversion_dollars,
	100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as lost_market_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'OfertaPerdida' or lt.lead_status = 'PedidoExpirado' or
    lt.lead_status = 'PedidoPerdido' or lt.lead_status = 'Proposta Enviada' or lt.lead_status = 'PedidoCancelado' or lt.lead_status = 'OfertaExpirada'
    ) then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as lost_market,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then 1 else 0 end) as reject_ratio_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 then lt.loan_requested_value else 0 end) as reject_ratio_dollar,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado')
    then 1 else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then 1 else 0 end) as reject_ratio_2_units,
    100 * cast( sum( case when lt.loan_request_is_all_docs = 1 and lt.lead_status = 'PedidoRejeitado'
    then lt.loan_requested_value else 0 end) as real)/sum( case when lt.loan_request_is_all_docs = 1 and (lt.lead_status = 'PedidoRejeitado' or lt.lead_status = 'PedidoAprovado') then lt.loan_requested_value else 0 end) as reject_ratio_2_dollar
	from data_science.lead_tape lt
where 1 = 1
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.lead_opt_in_date >= CURRENT_DATE - 45 and lt.lead_opt_in_date < CURRENT_DATE - 15
and lt.lead_biz_prime_matriz > 5 and lt.loan_is_renewal = 0


/* conversion o2L near-prime*/
select 
	case when  dp.outbound_loan_app_id is not null then 'outbound'
	when lead_biz_prime_matriz <= 5 and loan_is_renewal = 0 then 'new_prime'
    when lead_biz_prime_matriz > 5 and loan_is_renewal = 0 then 'new_near_prime'
	when loan_is_renewal > 0 then 'renew'
	end as funnel,
	case when sum(lt.final_offer_max_value ) = 0 then 0 else sum( case when (lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE ) then lt.loan_gross_value else 0 end )::numeric / sum(lt.final_offer_max_value)::numeric end as conv_o2l_value,
	case when count(lt.final_offer_max_value) = 0 then 0 else count (*) filter (where lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE  )::numeric / count(lt.final_offer_max_value)::numeric end as conv_o2l_unit,
	sum( case when (lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE  ) then lt.loan_gross_value else 0 end )::numeric as loans_value,
	count (*) filter (where lt.loan_is_loan = 1 and lt.loan_contract_date < CURRENT_DATE  )::numeric as loans_unit,
	case when sum(lt.final_offer_max_value ) = 0 then 0 else sum( case when (lt.loan_request_id is not null and lt.loan_request_date < CURRENT_DATE  ) then lt.loan_requested_value else 0 end )::numeric / sum(lt.final_offer_max_value)::numeric end as conv_o2r_value,
	case when count(lt.final_offer_max_value) = 0 then 0 else count (*) filter (where lt.loan_request_id is not null  and lt.loan_request_date < CURRENT_DATE )::numeric / count(lt.final_offer_max_value)::numeric end as conv_o2r_unit,
	case when sum(lt.final_offer_max_value ) = 0 then 0 else sum( case when (lt.loan_request_is_all_docs = 1 and lt.loan_request_date < CURRENT_DATE ) then lt.loan_requested_value else 0 end )::numeric / sum(lt.final_offer_max_value)::numeric end as conv_o2d_value,
	case when count(lt.final_offer_max_value) = 0 then 0 else count (*) filter (where lt.loan_request_is_all_docs = 1  and lt.loan_request_date < CURRENT_DATE  )::numeric / count(lt.final_offer_max_value)::numeric end as conv_o2d_unit,
	count(*)
from data_science.lead_tape lt left join direct_prospects dp on dp.direct_prospect_id = lt.lead_id
where lead_opt_in_date::date<CURRENT_DATE -15/*fim janela temporal*/
and lead_opt_in_date::date>=CURRENT_DATE -45/*inicio janela temporal*/
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.initial_offer_id is not null
group by 1 order by 10 desc


/* Expected Profitability Near Prime*/
select 
	sum(ltf.loan_final_net_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_rate,
	sum(ltf.loan_final_gross_interest_rate * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_gross_rate,
    sum(ltf.loan_original_final_term * ltf.loan_gross_value ) / sum(ltf.loan_gross_value ) as avg_term,
    sum(case when ltf.loan_is_renewal = 0 then 
    		case when lt.lead_biz_prime_matriz = 1 then 0.085 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 2 then 0.126 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 3 then 0.139 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 4 then 0.165 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 5 then 0.144 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 6 then 0.194 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 7 then 0.224 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 8 then 0.149 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 9 then 0.239 * ltf.collection_original_p_plus_i 
      when lt.lead_biz_prime_matriz = 10 then 0.289 * ltf.collection_original_p_plus_i 
      else 0.165 * ltf.collection_original_p_plus_i 
      end 
      when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end)
      / sum(ltf.collection_original_p_plus_i ) as el_combo,
      sum( case when ltf.loan_is_renewal = 0 then 
      case when lt.lead_mesa_score_ranking is null then 0.154 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 1 then 0.071 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 2 then 0.141 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 3 then 0.124 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 4 then 0.168 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 5 then 0.161 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 6 then 0.265 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 7 then 0.213 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 8 then 0.256 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 9 then 0.314 * ltf.collection_original_p_plus_i 
      when lt.lead_mesa_score_ranking = 10 then 0.384 * ltf.collection_original_p_plus_i 
      else 0.154 * ltf.collection_original_p_plus_i 
      end when ltf.loan_is_renewal > 0 then
      case when lt.lead_behaviour_rn_menos1_score <= 0.08 then 0.10 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.3 then 0.12 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score <= 0.7 then 0.2 * ltf.collection_original_p_plus_i 
       when lt.lead_behaviour_rn_menos1_score > 0.7 then 0.3 * ltf.collection_original_p_plus_i 
      end end) / sum(ltf.collection_original_p_plus_i ) as el_mesa,
      sum( ltf.loan_gross_value) as loans_money,
      count( ltf.loan_gross_value) as loans_unit,
      sum( ltf.collection_original_p_plus_i ) as p_plus_i
from data_science.loan_tape ltf  left join data_science.lead_tape lt on ltf.lead_id = lt.lead_id
where ltf.loan_contract_date >= '2021-10-01' and ltf.loan_contract_date < '2022-01-01' 
and( ltf.lead_marketing_channel != 'magalu' and ltf.lead_marketing_channel != 'sebrae' and ltf.lead_marketing_channel != 'estimulo2020' )
and lt.lead_biz_prime_matriz > 5 and ltf.loan_is_renewal = 0


/* Origination BNDES*/
select 
	 sum(lt.loan_gross_value) as origination, count(*) as units, sum(lt.loan_gross_value) / count(*) as avg_ticket,
	sum(lt.loan_gross_value * lt.loan_final_net_interest_rate) / sum(lt.loan_gross_value) as avg_ir, sum(lt.loan_gross_value * lt2.initial_offer_interest_rate ) / sum(lt.loan_gross_value) as avg_orig_ir
from data_science.loan_tape lt left join data_science.lead_tape lt2 on lt2.lead_id = lt.lead_id
where lt.loan_contract_date >= CURRENT_DATE - 30  and lt.loan_contract_date < CURRENT_DATE
and( lt.lead_marketing_channel != 'magalu' and lt.lead_marketing_channel != 'sebrae' and lt.lead_marketing_channel != 'estimulo2020' )
and lt.loan_portfolio_id = '14'







select 
	lt.loan_cohort_loan,
	(lt.lead_informed_month_revenue >= 250000)::int as is_big_guys,
	sum(lt.loan_final_net_interest_rate * lt.loan_gross_value ) / sum(lt.loan_gross_value) as avg_rate,
	sum(lt.loan_final_gross_interest_rate * lt.loan_gross_value ) / sum(lt.loan_gross_value ) as avg_gross_rate,
    sum(lt.loan_original_final_term * lt.loan_gross_value ) / sum(lt.loan_gross_value ) as avg_term,
    sum(case lt.lead_biz_prime_matriz 
			when 1 then 0.085 * lt.collection_original_p_plus_i 
			when 2 then 0.126 * lt.collection_original_p_plus_i 
			when 3 then 0.139 * lt.collection_original_p_plus_i 
			when 4 then 0.165 * lt.collection_original_p_plus_i 
			when 5 then 0.144 * lt.collection_original_p_plus_i 
			when 6 then 0.194 * lt.collection_original_p_plus_i 
			when 7 then 0.224 * lt.collection_original_p_plus_i 
			when 8 then 0.149 * lt.collection_original_p_plus_i 
			when 9 then 0.239 * lt.collection_original_p_plus_i 
			when 10 then 0.289 * lt.collection_original_p_plus_i 
		end) / sum(lt.collection_original_p_plus_i) as el_combo,
	sum(case lt.lead_mesa_score_ranking
			when 1 then 0.071 * lt.collection_original_p_plus_i 
			when 2 then 0.141 * lt.collection_original_p_plus_i 
			when 3 then 0.124 * lt.collection_original_p_plus_i 
			when 4 then 0.168 * lt.collection_original_p_plus_i 
			when 5 then 0.161 * lt.collection_original_p_plus_i 
			when 6 then 0.265 * lt.collection_original_p_plus_i 
			when 7 then 0.213 * lt.collection_original_p_plus_i 
			when 8 then 0.256 * lt.collection_original_p_plus_i 
			when 9 then 0.314 * lt.collection_original_p_plus_i 
			when 10 then 0.384 * lt.collection_original_p_plus_i 
		end) / sum(lt.collection_original_p_plus_i) as el_mesa,
	sum(case bg.rating_bndes_consolidado 
			when 'A1' then 0.03 * lt.collection_original_p_plus_i 
			when 'A2' then 0.03 * lt.collection_original_p_plus_i 
			when 'A3' then 0.14 * lt.collection_original_p_plus_i 
			when 'A4' then 0.14 * lt.collection_original_p_plus_i 
			when 'B1' then 0.15 * lt.collection_original_p_plus_i 
			when 'B2' then 0.15 * lt.collection_original_p_plus_i 
			when 'B3' then 0.17 * lt.collection_original_p_plus_i 
			when 'B4' then 0.31 * lt.collection_original_p_plus_i 
		end) / sum(lt.collection_original_p_plus_i) as el_rating_bndes,
	sum(case bg.rating_bndes_consolidado 
			when 'A1' then 0.03 * lt.collection_original_p_plus_i 
			when 'A2' then 0.05 * lt.collection_original_p_plus_i 
			when 'A3' then 0.13 * lt.collection_original_p_plus_i 
			when 'A4' then 0.14 * lt.collection_original_p_plus_i 
			when 'B1' then 0.15 * lt.collection_original_p_plus_i 
			when 'B2' then 0.17 * lt.collection_original_p_plus_i 
			when 'B3' then 0.19 * lt.collection_original_p_plus_i 
			when 'B4' then 0.31 * lt.collection_original_p_plus_i 
		end) / sum(lt.collection_original_p_plus_i) as el_rating_bndes2,
      sum(lt.loan_gross_value) as loans_money,
      count(lt.loan_gross_value) as loans_unit,
      sum(lt.collection_original_p_plus_i) as p_plus_i
from data_science.lead_tape lt
left join analytics_engineering.big_guys2 bg on bg.lead_id = lt.lead_id
where lt.lead_is_caas = 0 and lt.lead_is_mca = 0 and lt.loan_is_renewal = 0 and lt.loan_is_loan = 1
group by 1,2


