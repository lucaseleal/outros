select
-- COLUNAS POLITICA
	dp.direct_prospect_id as atual_lead_id,	
--	dp.workflow as atual_lead_status,
--	dpu.direct_prospect_id as ultimo_lead_id,	
--	count_renegotiation as ultimo_collection_count_restructures,
--	dpu.value as ultimo_loan_net_value,
--	paid_inst as ultimo_collection_number_of_installments_paid,
--	current_term as ultimo_collection_loan_current_term,
--	paid_inst::float / current_term as ultimo_perc_pago_current, 
--	dt_ult_pgto as ultimo_collection_date_of_last_payment,
--	case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end as ultimo_collection_loan_curr_late_by,
-- COLUNAS SCORE
	dp.serasa_6 as atual_lead_experian_score6,
	coalesce(dpd_soft.max_late_pmt1,0) + coalesce(dpd_soft.max_late_pmt2,0) + coalesce(dpd_soft.max_late_pmt3,0) + coalesce(dpd_soft.max_late_pmt4,0) + coalesce(dpd_soft.max_late_pmt5,0) + coalesce(dpd_soft.max_late_pmt6,0) + coalesce(dpd_soft.max_late_pmt7,0) + coalesce(dpd_soft.max_late_pmt8,0) + coalesce(dpd_soft.max_late_pmt9,0) + coalesce(dpd_soft.max_late_pmt10,0) + coalesce(dpd_soft.max_late_pmt11,0) + coalesce(dpd_soft.max_late_pmt12,0) + coalesce(dpd_soft.max_late_pmt13,0) + coalesce(dpd_soft.max_late_pmt14,0) + coalesce(dpd_soft.max_late_pmt15,0) + coalesce(dpd_soft.max_late_pmt16,0) + coalesce(dpd_soft.max_late_pmt17,0) + coalesce(dpd_soft.max_late_pmt18,0) + coalesce(dpd_soft.max_late_pmt19,0) + coalesce(dpd_soft.max_late_pmt20,0) + coalesce(dpd_soft.max_late_pmt21,0) + coalesce(dpd_soft.max_late_pmt22,0) + coalesce(dpd_soft.max_late_pmt23,0) + coalesce(dpd_soft.max_late_pmt24,0) as collection_sum_payment_history_curve,
	case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when coalesce(scr_hist_pf_atual.long_term_debt_PF_Curr,scr_modal_pf_atual.CarteiraCredito_PF_Curr,0) = 0 then 0 when coalesce(scr_hist_pf_atual.long_term_debt_PF_11M,scr_modal_pf_atual.CarteiraCredito_PF_11M) = 0 then 2019 else coalesce(scr_hist_pf_atual.long_term_debt_PF_Curr,scr_modal_pf_atual.CarteiraCredito_PF_Curr) / coalesce(scr_hist_pf_atual.long_term_debt_PF_11M,scr_modal_pf_atual.CarteiraCredito_PF_11M) - 1 end as scr_var_rel_12M_long_term_debt_pf,
	case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when scr_modal_pf_atual.emprestimos_cheque_especial_PF_Curr is null or scr_modal_pf_atual.emprestimos_cheque_especial_PF_Curr = 0 then 0 when scr_modal_pf_atual.emprestimos_cheque_especial_PF_5M = 0 then 2019 else scr_modal_pf_atual.emprestimos_cheque_especial_PF_Curr / scr_modal_pf_atual.emprestimos_cheque_especial_PF_5M - 1 end as atual_scr_var_rel_6M_overdraft_debt_pf,
	case when dp.scr_data is null then null when scr_modal_pj_atual.emprestimos_PJ_Curr is null then 0 else (scr_modal_pj_atual.emprestimos_PJ_Curr - scr_modal_pj_atual.emprestimos_PJ_11M) / dp.month_revenue / 12 * 1000 end as atual_scr_var_abs_12M_all_loans_PJ_over_revenue,	
	case case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when scr_modal_pf_atual.emprestimos_cheque_especial_PF_Curr is null then 0 else scr_modal_pf_atual.months_sth_emprestimos_cheque_especial_PF / (case when scr_modal_pf_atual.qtd_meses_modalidade_PF <= 1 then 1 else scr_modal_pf_atual.qtd_meses_modalidade_PF-1 end) end when 0 then 0 else case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when scr_modal_pf_atual.emprestimos_cheque_especial_PF_Curr is null then 0 else scr_modal_pf_atual.months_sth_emprestimos_cheque_especial_PF / (case when scr_modal_pf_atual.qtd_meses_modalidade_PF <= 1 then 1 else scr_modal_pf_atual.qtd_meses_modalidade_PF-1 end) end / 
		regexp_replace(regexp_replace(case when scr_hist_pf_ultimo.teve_scr_pf_tirado = 0 then null when scr_modal_pf_ultimo.emprestimos_cheque_especial_PF_Curr is null then 0 else scr_modal_pf_ultimo.months_sth_emprestimos_cheque_especial_PF / (case when scr_modal_pf_ultimo.qtd_meses_modalidade_PF <= 1 then 1 else scr_modal_pf_ultimo.qtd_meses_modalidade_PF-1 end) end::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_month_consecutive_dive_overdraft_pf_debt_over_total_months,
	case case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when coalesce(scr_hist_pf_atual.long_term_debt_PF_Curr,scr_modal_pf_atual.CarteiraCredito_PF_Curr) is null then 0 else coalesce(scr_hist_pf_atual.Meses_Aumento_DividaPF,scr_modal_pf_atual.Meses_Aumento_CarteiraCredito_PF) / (case when coalesce(scr_hist_pf_atual.qtd_meses_escopo_PF,scr_modal_pf_atual.qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(scr_hist_pf_atual.qtd_meses_escopo_PF,scr_modal_pf_atual.qtd_meses_modalidade_PF) - 1 end) end when 0 then 0 else case when scr_hist_pf_atual.teve_scr_pf_tirado = 0 then null when coalesce(scr_hist_pf_atual.long_term_debt_PF_Curr,scr_modal_pf_atual.CarteiraCredito_PF_Curr) is null then 0 else coalesce(scr_hist_pf_atual.Meses_Aumento_DividaPF,scr_modal_pf_atual.Meses_Aumento_CarteiraCredito_PF) / (case when coalesce(scr_hist_pf_atual.qtd_meses_escopo_PF,scr_modal_pf_atual.qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(scr_hist_pf_atual.qtd_meses_escopo_PF,scr_modal_pf_atual.qtd_meses_modalidade_PF) - 1 end) end / 
		regexp_replace(regexp_replace(case when scr_hist_pf_ultimo.teve_scr_pf_tirado = 0 then null when coalesce(scr_hist_pf_ultimo.long_term_debt_PF_Curr,scr_modal_pf_ultimo.CarteiraCredito_PF_Curr) is null then 0 else coalesce(scr_hist_pf_ultimo.Meses_Aumento_DividaPF,scr_modal_pf_ultimo.Meses_Aumento_CarteiraCredito_PF) / (case when coalesce(scr_hist_pf_ultimo.qtd_meses_escopo_PF,scr_modal_pf_ultimo.qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(scr_hist_pf_ultimo.qtd_meses_escopo_PF,scr_modal_pf_ultimo.qtd_meses_modalidade_PF) - 1 end) end::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_months_rise_long_term_debt_pf_over_total_months
from direct_prospects dp
join (select
		dp.direct_prospect_id,
		lr.loan_request_id,
		dp.cnpj,
		dp.previous_loans_count,
		lr.value
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	where lr.status = 'ACCEPTED') as dpu on dpu.cnpj = dp.cnpj and dpu.previous_loans_count + 1 = dp.previous_loans_count--------------------------TABLE DPD SOFT
join(select lr.loan_request_id,
		max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt1_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then current_date - pmt1_venc else fp.pago_em - pmt1_venc end) end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt2_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then current_date - pmt2_venc else fp.pago_em - pmt2_venc end) end) end) end)) as max_late_pmt2,
		max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt3_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then current_date - pmt3_venc else fp.pago_em - pmt3_venc end) end) end) end)) as max_late_pmt3,
		max((case when fp.numero_ajustado <> 4 then null else (case when ParcSubst = 4 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt4_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then current_date - pmt4_venc else fp.pago_em - pmt4_venc end) end) end) end)) as max_late_pmt4,
		max((case when fp.numero_ajustado <> 5 then null else (case when ParcSubst = 5 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt5_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then current_date - pmt5_venc else fp.pago_em - pmt5_venc end) end) end) end)) as max_late_pmt5,
		max((case when fp.numero_ajustado <> 6 then null else (case when ParcSubst = 6 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt6_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then current_date - pmt6_venc else fp.pago_em - pmt6_venc end) end) end) end)) as max_late_pmt6,
		max((case when fp.numero_ajustado <> 7 then null else (case when ParcSubst = 7 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt7_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt7_venc else null end) else (case when fp.pago_em is null then current_date - pmt7_venc else fp.pago_em - pmt7_venc end) end) end) end)) as max_late_pmt7,
		max((case when fp.numero_ajustado <> 8 then null else (case when ParcSubst = 8 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt8_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt8_venc else null end) else (case when fp.pago_em is null then current_date - pmt8_venc else fp.pago_em - pmt8_venc end) end) end) end)) as max_late_pmt8,
		max((case when fp.numero_ajustado <> 9 then null else (case when ParcSubst = 9 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt9_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt9_venc else null end) else (case when fp.pago_em is null then current_date - pmt9_venc else fp.pago_em - pmt9_venc end) end) end) end)) as max_late_pmt9,
		max((case when fp.numero_ajustado <> 10 then null else (case when ParcSubst = 10 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt10_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt10_venc else null end) else (case when fp.pago_em is null then current_date - pmt10_venc else fp.pago_em - pmt10_venc end) end) end) end)) as max_late_pmt10,
		max((case when fp.numero_ajustado <> 11 then null else (case when ParcSubst = 11 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt11_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt11_venc else null end) else (case when fp.pago_em is null then current_date - pmt11_venc else fp.pago_em - pmt11_venc end) end) end) end)) as max_late_pmt11,
		max((case when fp.numero_ajustado <> 12 then null else (case when ParcSubst = 12 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt12_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt12_venc else null end) else (case when fp.pago_em is null then current_date - pmt12_venc else fp.pago_em - pmt12_venc end) end) end) end)) as max_late_pmt12,
		max((case when fp.numero_ajustado <> 13 then null else (case when ParcSubst = 13 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt13_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt13_venc else null end) else (case when fp.pago_em is null then current_date - pmt13_venc else fp.pago_em - pmt13_venc end) end) end) end)) as max_late_pmt13,
		max((case when fp.numero_ajustado <> 14 then null else (case when ParcSubst = 14 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt14_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt14_venc else null end) else (case when fp.pago_em is null then current_date - pmt14_venc else fp.pago_em - pmt14_venc end) end) end) end)) as max_late_pmt14,
		max((case when fp.numero_ajustado <> 15 then null else (case when ParcSubst = 15 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt15_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt15_venc else null end) else (case when fp.pago_em is null then current_date - pmt15_venc else fp.pago_em - pmt15_venc end) end) end) end)) as max_late_pmt15,
		max((case when fp.numero_ajustado <> 16 then null else (case when ParcSubst = 16 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt16_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt16_venc else null end) else (case when fp.pago_em is null then current_date - pmt16_venc else fp.pago_em - pmt16_venc end) end) end) end)) as max_late_pmt16,
		max((case when fp.numero_ajustado <> 17 then null else (case when ParcSubst = 17 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt17_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt17_venc else null end) else (case when fp.pago_em is null then current_date - pmt17_venc else fp.pago_em - pmt17_venc end) end) end) end)) as max_late_pmt17,
		max((case when fp.numero_ajustado <> 18 then null else (case when ParcSubst = 18 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt18_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt18_venc else null end) else (case when fp.pago_em is null then current_date - pmt18_venc else fp.pago_em - pmt18_venc end) end) end) end)) as max_late_pmt18,
		max((case when fp.numero_ajustado <> 19 then null else (case when ParcSubst = 19 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt19_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt19_venc else null end) else (case when fp.pago_em is null then current_date - pmt19_venc else fp.pago_em - pmt19_venc end) end) end) end)) as max_late_pmt19,
		max((case when fp.numero_ajustado <> 20 then null else (case when ParcSubst = 20 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt20_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt20_venc else null end) else (case when fp.pago_em is null then current_date - pmt20_venc else fp.pago_em - pmt20_venc end) end) end) end)) as max_late_pmt20,
		max((case when fp.numero_ajustado <> 21 then null else (case when ParcSubst = 21 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt21_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt21_venc else null end) else (case when fp.pago_em is null then current_date - pmt21_venc else fp.pago_em - pmt21_venc end) end) end) end)) as max_late_pmt21,
		max((case when fp.numero_ajustado <> 22 then null else (case when ParcSubst = 22 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt22_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt22_venc else null end) else (case when fp.pago_em is null then current_date - pmt22_venc else fp.pago_em - pmt22_venc end) end) end) end)) as max_late_pmt22,
		max((case when fp.numero_ajustado <> 23 then null else (case when ParcSubst = 23 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt23_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt23_venc else null end) else (case when fp.pago_em is null then current_date - pmt23_venc else fp.pago_em - pmt23_venc end) end) end) end)) as max_late_pmt23,
		max((case when fp.numero_ajustado <> 24 then null else (case when ParcSubst = 24 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt24_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt24_venc else null end) else (case when fp.pago_em is null then current_date - pmt24_venc else fp.pago_em - pmt24_venc end) end) end) end)) as max_late_pmt24,
		max(fp.pago_em) as dt_ult_pgto
	from loan_requests lr
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join (select direct_prospect_id, previous_loans_count, cnpj from direct_prospects) as aux on aux.cnpj = dp.cnpj and aux.previous_loans_count - 1 = dp.previous_loans_count
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
		from financeiro_parcela fp 
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
		where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
	join(select fpp.emprestimo_id,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fpp.id = min_plano.min_id) as pmt2_orig_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 4 and fp.status not in ('Cancelada','EmEspera')) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 5 and fp.status not in ('Cancelada','EmEspera')) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 6 and fp.status not in ('Cancelada','EmEspera')) as pmt6_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 7 and fp.status not in ('Cancelada','EmEspera')) as pmt7_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 8 and fp.status not in ('Cancelada','EmEspera')) as pmt8_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 9 and fp.status not in ('Cancelada','EmEspera')) as pmt9_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 10 and fp.status not in ('Cancelada','EmEspera')) as pmt10_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 11 and fp.status not in ('Cancelada','EmEspera')) as pmt11_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 12 and fp.status not in ('Cancelada','EmEspera')) as pmt12_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 13 and fp.status not in ('Cancelada','EmEspera')) as pmt13_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 14 and fp.status not in ('Cancelada','EmEspera')) as pmt14_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 15 and fp.status not in ('Cancelada','EmEspera')) as pmt15_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 16 and fp.status not in ('Cancelada','EmEspera')) as pmt16_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 17 and fp.status not in ('Cancelada','EmEspera')) as pmt17_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 18 and fp.status not in ('Cancelada','EmEspera')) as pmt18_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 19 and fp.status not in ('Cancelada','EmEspera')) as pmt19_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 20 and fp.status not in ('Cancelada','EmEspera')) as pmt20_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 21 and fp.status not in ('Cancelada','EmEspera')) as pmt21_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 22 and fp.status not in ('Cancelada','EmEspera')) as pmt22_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 23 and fp.status not in ('Cancelada','EmEspera')) as pmt23_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 24 and fp.status not in ('Cancelada','EmEspera')) as pmt24_venc,
			sum(fp.cobrado) filter (where fpp.id = min_plano.min_id) as original_p_and_i
		from financeiro_planopagamento fpp
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera') 
		and aux.direct_prospect_id = 375629
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = dpu.loan_request_id
--------------------------TABLE PARCELAS
join(select lr.loan_request_id,
		count(*) filter (where fp.numero_ajustado <> fp.numero and fp.pago_em is not null) as count_paga_fora_de_ordem,	
		count(*) as current_term,
		min(fp.pago_em) as first_paid_inst_paid_on,
		min(fp.pago_em) filter (where fp.numero_ajustado = 2) as second_paid_inst_paid_on,
		sum(fp.pago) filter (where fp.status like 'Pago%') as total_paid_amount, 
		sum(fp.cobrado) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as total_late_amount, 
		max(fp.cobrado) filter (where fp.status = 'Ativo') as pmt_atual,
		sum(fp.cobrado) as curr_p_and_i,
		max(original_p_and_i) as original_p_and_i,
		sum(fp.cobrado) filter (where fp.pago_em is null) as EAD, 
		count(*) filter (where fp.status like 'Pago%') as paid_inst,
		count(*) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as late_inst, 
		count(*) filter (where fp.vencimento_em < current_date) as due_inst,	
		count(*) filter (where fp.vencimento_em >= current_date) as to_come_inst,	
		min(fp.vencimento_em) filter (where fp.pago_em is null) as current_inst_due_to,
		sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 180) as total_amount_over_180,
		max(original_1st_pmt_due_to) as original_1st_pmt_due_to,
		max(original_2nd_pmt_due_to) as original_2nd_pmt_due_to,
		max(ParcSubst) as primeira_parcela_renegociada,
		min(fp.numero_ajustado) filter (where fp.pago_em is null) as parc_em_aberto
	from loan_requests lr
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join (select direct_prospect_id, previous_loans_count, cnpj from direct_prospects) as aux on aux.cnpj = dp.cnpj and aux.previous_loans_count - 1 = dp.previous_loans_count
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
		from financeiro_parcela fp 
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
		where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
		group by 1) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA BUSCAR INFORMAÇÕES DO PLANO ORIGINAL
	join(select fpp.emprestimo_id, 
			sum(fp.cobrado) as original_p_and_i, 
			max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to, 
			max(fp.vencimento_em) filter (where fp.numero = 2) as original_2nd_pmt_due_to
		from financeiro_planopagamento fpp  
		join (select emprestimo_id, min(id) as plano_id from financeiro_planopagamento group by 1) as fpp_orig on fpp_orig.plano_id = fpp.id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera')
		and aux.direct_prospect_id = 375629
	group by 1) as parcelas on parcelas.loan_request_id = dpu.loan_request_id
--------------------------TABLE PLANO PAGAMENTO
join(select lr.loan_request_id, 
		max(case when fpp.status = 'Pago' then 1 else 0 end) as fully_paid,
		count(*) filter (where fpp.parcelas_qtd = 1 and fpp.status = 'Pago') as antecipou_emprestimo,
		count(*) - 1 as count_renegotiation,
		count(*) filter (where fpp.status not in ('Cancelado','EmEspera')) - 1 as count_unbroken_renegotiations,
		count(*) filter (where fpp.status = 'Cancelado') as count_broken_renegotiation,
		min(fpp.inicio_em) filter (where fpp.inicio_em > lr.loan_date + interval '5 days') as data_primeira_renegociacao
	from loan_requests lr  
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join (select direct_prospect_id, previous_loans_count, cnpj from direct_prospects) as aux on aux.cnpj = dp.cnpj and aux.previous_loans_count - 1 = dp.previous_loans_count
	where aux.direct_prospect_id = 375629
	group by 1) as planopagamento on planopagamento.loan_request_id = dpu.loan_request_id
--------------------------TABLE SCR HISTORICO PF - ULTIMO
left join(select 
	direct_prospect_id,
	teve_scr_pf_tirado,
	Qtd_meses_escopo_pf,
	Long_Term_Debt_pf_Curr::float,
	Long_Term_Debt_pf_1M::float,
	Long_Term_Debt_pf_2M::float,
	Long_Term_Debt_pf_3M::float,
	Long_Term_Debt_pf_4M::float,
	Long_Term_Debt_pf_5M::float,
	Long_Term_Debt_pf_6M::float,
	Long_Term_Debt_pf_7M::float,
	Long_Term_Debt_pf_8M::float,
	Long_Term_Debt_pf_9M::float,
	Long_Term_Debt_pf_10M::float,
	Long_Term_Debt_pf_11M::float,
	case when Long_Term_Debt_pf_22M > Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M > Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M > Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M > Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M > Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M > Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M > Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M > Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M > Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M > Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M > Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M > Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M > Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M > Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M > Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M > Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M > Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M > Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M > Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M > Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M > Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M > Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr > Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Aumento_Dividapf
from(select 
	direct_prospect_id,
	max(teve_scr_pf_tirado) as teve_scr_pf_tirado,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pf,
	sum(Debtpf) filter (where data = data_referencia) as Long_Term_Debt_pf_Curr,
	sum(Debtpf) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_pf_1M,
	sum(Debtpf) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_pf_2M,
	sum(Debtpf) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_pf_3M,
	sum(Debtpf) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_pf_4M,
	sum(Debtpf) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_pf_5M,
	sum(Debtpf) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_pf_6M,
	sum(Debtpf) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_pf_7M,
	sum(Debtpf) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_pf_8M,
	sum(Debtpf) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_pf_9M,
	sum(Debtpf) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_pf_10M,
	sum(Debtpf) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_pf_11M,
	sum(Debtpf) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_pf_12M,
	sum(Debtpf) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_pf_13M,
	sum(Debtpf) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_pf_14M,
	sum(Debtpf) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_pf_15M,
	sum(Debtpf) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_pf_16M,
	sum(Debtpf) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_pf_17M,
	sum(Debtpf) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_pf_18M,
	sum(Debtpf) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_pf_19M,
	sum(Debtpf) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_pf_20M,
	sum(Debtpf) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_pf_21M,
	sum(Debtpf) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_pf_22M,
	sum(Debtpf) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_pf_23M
	from(select direct_prospect_id,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float Debtpf,
			case when coalesce(max_date,min_date) is null then 0 else 1 end as teve_scr_pf_tirado
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cpf,
				max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) filter (where coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta)::date <= lr.loan_date) as max_date,
				min(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) as min_date
			from credito_coleta cc
			join direct_prospects dp on cc.documento = dp.cpf
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			join (select direct_prospect_id, previous_loans_count, cnpj from direct_prospects) as aux on aux.cnpj = dp.cnpj and aux.previous_loans_count - 1 = dp.previous_loans_count
			where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED' 
				and aux.direct_prospect_id = 375629
			group by 1,2,3) as t1 on documento_tipo = 'CPF' and tipo = 'SCR' and t1.cpf = cc.documento and coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id = coalesce(max_date,min_date)) as t2
	group by 1) as t3) as SCR_Hist_PF_ultimo on SCR_Hist_PF_ultimo.direct_prospect_id = dpu.direct_prospect_id
--------------------------TABLE SCR MODALIDADES PF - ULTIMO
left join(select 
	direct_prospect_id,
	qtd_meses_modalidade_pf,
	emprestimos_cheque_especial_pf_Curr::float,
	emprestimos_cheque_especial_pf_1M::float,
	emprestimos_cheque_especial_pf_2M::float,
	emprestimos_cheque_especial_pf_3M::float,
	emprestimos_cheque_especial_pf_4M::float,
	emprestimos_cheque_especial_pf_5M::float,
	emprestimos_cheque_especial_pf_6M::float,
	emprestimos_cheque_especial_pf_7M::float,
	emprestimos_cheque_especial_pf_8M::float,
	emprestimos_cheque_especial_pf_9M::float,
	emprestimos_cheque_especial_pf_10M::float,
	emprestimos_cheque_especial_pf_11M::float,
	case when emprestimos_cheque_especial_pf_1M is null then null else
	case when emprestimos_cheque_especial_pf_Curr >= emprestimos_cheque_especial_pf_1M or emprestimos_cheque_especial_pf_1M is null then 0 else
	case when emprestimos_cheque_especial_pf_1M >= emprestimos_cheque_especial_pf_2M or emprestimos_cheque_especial_pf_2M is null then 1 else
	case when emprestimos_cheque_especial_pf_2M >= emprestimos_cheque_especial_pf_3M or emprestimos_cheque_especial_pf_3M is null then 2 else
	case when emprestimos_cheque_especial_pf_3M >= emprestimos_cheque_especial_pf_4M or emprestimos_cheque_especial_pf_4M is null then 3 else
	case when emprestimos_cheque_especial_pf_4M >= emprestimos_cheque_especial_pf_5M or emprestimos_cheque_especial_pf_5M is null then 4 else
	case when emprestimos_cheque_especial_pf_5M >= emprestimos_cheque_especial_pf_6M or emprestimos_cheque_especial_pf_6M is null then 5 else
	case when emprestimos_cheque_especial_pf_6M >= emprestimos_cheque_especial_pf_7M or emprestimos_cheque_especial_pf_7M is null then 6 else
	case when emprestimos_cheque_especial_pf_7M >= emprestimos_cheque_especial_pf_8M or emprestimos_cheque_especial_pf_8M is null then 7 else
	case when emprestimos_cheque_especial_pf_8M >= emprestimos_cheque_especial_pf_9M or emprestimos_cheque_especial_pf_9M is null then 8 else
	case when emprestimos_cheque_especial_pf_9M >= emprestimos_cheque_especial_pf_10M or emprestimos_cheque_especial_pf_10M is null then 9 else
	case when emprestimos_cheque_especial_pf_10M >= emprestimos_cheque_especial_pf_11M or emprestimos_cheque_especial_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cheque_especial_pf,
	CarteiraCredito_PF_Curr::float,
	CarteiraCredito_PF_1M::float,
	CarteiraCredito_PF_2M::float,
	CarteiraCredito_PF_3M::float,
	CarteiraCredito_PF_4M::float,
	CarteiraCredito_PF_5M::float,
	CarteiraCredito_PF_6M::float,
	CarteiraCredito_PF_7M::float,
	CarteiraCredito_PF_8M::float,
	CarteiraCredito_PF_9M::float,
	CarteiraCredito_PF_10M::float,
	CarteiraCredito_PF_11M::float,
	case when CarteiraCredito_PF_10M > CarteiraCredito_PF_11M then 1 else 0 end +
	case when CarteiraCredito_PF_9M > CarteiraCredito_PF_10M then 1 else 0 end +
	case when CarteiraCredito_PF_8M > CarteiraCredito_PF_9M then 1 else 0 end +
	case when CarteiraCredito_PF_7M > CarteiraCredito_PF_8M then 1 else 0 end +
	case when CarteiraCredito_PF_6M > CarteiraCredito_PF_7M then 1 else 0 end +
	case when CarteiraCredito_PF_5M > CarteiraCredito_PF_6M then 1 else 0 end +
	case when CarteiraCredito_PF_4M > CarteiraCredito_PF_5M then 1 else 0 end +
	case when CarteiraCredito_PF_3M > CarteiraCredito_PF_4M then 1 else 0 end +
	case when CarteiraCredito_PF_2M > CarteiraCredito_PF_3M then 1 else 0 end +
	case when CarteiraCredito_PF_1M > CarteiraCredito_PF_2M then 1 else 0 end +
	case when CarteiraCredito_PF_Curr > CarteiraCredito_PF_1M then 1 else 0 end::float as Meses_Aumento_CarteiraCredito_PF
from(select direct_prospect_id,
	count(distinct to_date) filter (where to_date <= data_referencia) as qtd_meses_modalidade_PF,
	--------------------------------EMPRESTIMOS -> CHEQUE ESPECIAL
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_11M,
	--------------------------------CARTEIRA CREDITO
	sum(valor) filter (where to_date = data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_11M
	from(select 
			direct_prospect_id, 
			data_referencia,
			regexp_replace(lv1, '\d{2}\s-\s', '') as lv1,
			replace(replace(replace(replace(regexp_replace(lv2, '\d{4}\s-\s', ''),'  ',' - '),' -- ',' - '),' - ',' - '),'interfinanceiros','Interfinanceiros') as lv2, 
			to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float
		from(select 
				direct_prospect_id, 
				data_referencia,
				Lv1, 
				nome as Lv2, 
				(jsonb_populate_recordset(null::meu2, serie)).*
			from (select 
					direct_prospect_id, 
					data_referencia,
					nome as Lv1, 
					(jsonb_populate_recordset(null::meu, detalhes)).* 
				from (select 
						direct_prospect_id, 
						to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,
						data_referencia,
						(jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*
					from (select 
							direct_prospect_id, 
							data,
							case when extract(day from loan_date) >= 16 then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') end as data_referencia
						from credito_coleta cc
						join(select 
								dp.direct_prospect_id, 
								lr.loan_date,
								cpf,
								max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) filter (where coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta)::date <= lr.loan_date) as max_date,
								min(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) as min_date
							from credito_coleta cc
							join direct_prospects dp on cc.documento = dp.cpf
							join offers o on o.direct_prospect_id = dp.direct_prospect_id
							join loan_requests lr on lr.offer_id = o.offer_id 
							join (select direct_prospect_id, previous_loans_count, cnpj from direct_prospects) as aux on aux.cnpj = dp.cnpj and aux.previous_loans_count - 1 = dp.previous_loans_count
							where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED' 
								and aux.direct_prospect_id = 375629
							group by 1,2,3) as t1 on documento_tipo = 'CPF' and tipo = 'SCR' and t1.cpf = cc.documento and coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id = coalesce(max_date,min_date)) as t2) as t3) as t4) as t5) as t6 group by 1) as t7) as SCR_Modal_PF_ULTIMO on SCR_Modal_PF_ULTIMO.direct_prospect_id = dpu.direct_prospect_id
--------------------------TABLE SCR HISTORICO PF - ATUAL
left join(select 
	direct_prospect_id,
	teve_scr_pf_tirado,
	Qtd_meses_escopo_pf,
	Long_Term_Debt_pf_Curr::float,
	Long_Term_Debt_pf_1M::float,
	Long_Term_Debt_pf_2M::float,
	Long_Term_Debt_pf_3M::float,
	Long_Term_Debt_pf_4M::float,
	Long_Term_Debt_pf_5M::float,
	Long_Term_Debt_pf_6M::float,
	Long_Term_Debt_pf_7M::float,
	Long_Term_Debt_pf_8M::float,
	Long_Term_Debt_pf_9M::float,
	Long_Term_Debt_pf_10M::float,
	Long_Term_Debt_pf_11M::float,
	case when Long_Term_Debt_pf_22M > Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M > Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M > Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M > Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M > Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M > Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M > Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M > Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M > Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M > Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M > Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M > Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M > Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M > Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M > Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M > Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M > Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M > Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M > Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M > Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M > Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M > Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr > Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Aumento_Dividapf
from(select direct_prospect_id,
	max(teve_scr_pf_tirado) as teve_scr_pf_tirado,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pf,
	sum(Debtpf) filter (where data = data_referencia) as Long_Term_Debt_pf_Curr,
	sum(Debtpf) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_pf_1M,
	sum(Debtpf) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_pf_2M,
	sum(Debtpf) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_pf_3M,
	sum(Debtpf) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_pf_4M,
	sum(Debtpf) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_pf_5M,
	sum(Debtpf) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_pf_6M,
	sum(Debtpf) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_pf_7M,
	sum(Debtpf) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_pf_8M,
	sum(Debtpf) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_pf_9M,
	sum(Debtpf) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_pf_10M,
	sum(Debtpf) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_pf_11M,
	sum(Debtpf) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_pf_12M,
	sum(Debtpf) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_pf_13M,
	sum(Debtpf) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_pf_14M,
	sum(Debtpf) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_pf_15M,
	sum(Debtpf) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_pf_16M,
	sum(Debtpf) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_pf_17M,
	sum(Debtpf) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_pf_18M,
	sum(Debtpf) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_pf_19M,
	sum(Debtpf) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_pf_20M,
	sum(Debtpf) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_pf_21M,
	sum(Debtpf) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_pf_22M,
	sum(Debtpf) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_pf_23M
	from(select direct_prospect_id,
			case when extract(day from ref_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float Debtpf,
			case when case when direct_prospect_id = max_lead_id then max_date_aberto else coalesce(max_date_fechado,min_date_fechado) end is null then 0 else 1 end as teve_scr_pf_tirado
		from credito_coleta cc
		join(select 
				dp.direct_prospect_id, 
				coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day' as ref_date,
				cpf,
				max_lead_id,
				max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) filter (where coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_date_aberto,
				max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) filter (where coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_date_fechado,
				min(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) as min_date_fechado
			from credito_coleta cc
			join direct_prospects dp on cc.documento = dp.cpf
				left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			join (select cnpj, max(direct_prospect_id) as max_lead_id from direct_prospects group by 1) as max_lead on max_lead.cnpj = dp.cnpj
			where documento_tipo = 'CPF' and tipo = 'SCR'
				and dp.direct_prospect_id = 379996
			group by 1,2,3,4) as t1 on documento_tipo = 'CPF' and tipo = 'SCR' and t1.cpf = cc.documento and coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id = case when direct_prospect_id = max_lead_id then max_date_aberto else coalesce(max_date_fechado,min_date_fechado) end) as t2
	group by 1) as t3) as SCR_Hist_PF_atual on SCR_Hist_PF_atual.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR MODALIDADES PF - ATUAL
left join(select 
	direct_prospect_id,
	qtd_meses_modalidade_pf,
	emprestimos_cheque_especial_pf_Curr::float,
	emprestimos_cheque_especial_pf_1M::float,
	emprestimos_cheque_especial_pf_2M::float,
	emprestimos_cheque_especial_pf_3M::float,
	emprestimos_cheque_especial_pf_4M::float,
	emprestimos_cheque_especial_pf_5M::float,
	emprestimos_cheque_especial_pf_6M::float,
	emprestimos_cheque_especial_pf_7M::float,
	emprestimos_cheque_especial_pf_8M::float,
	emprestimos_cheque_especial_pf_9M::float,
	emprestimos_cheque_especial_pf_10M::float,
	emprestimos_cheque_especial_pf_11M::float,
	case when emprestimos_cheque_especial_pf_1M is null then null else
	case when emprestimos_cheque_especial_pf_Curr >= emprestimos_cheque_especial_pf_1M or emprestimos_cheque_especial_pf_1M is null then 0 else
	case when emprestimos_cheque_especial_pf_1M >= emprestimos_cheque_especial_pf_2M or emprestimos_cheque_especial_pf_2M is null then 1 else
	case when emprestimos_cheque_especial_pf_2M >= emprestimos_cheque_especial_pf_3M or emprestimos_cheque_especial_pf_3M is null then 2 else
	case when emprestimos_cheque_especial_pf_3M >= emprestimos_cheque_especial_pf_4M or emprestimos_cheque_especial_pf_4M is null then 3 else
	case when emprestimos_cheque_especial_pf_4M >= emprestimos_cheque_especial_pf_5M or emprestimos_cheque_especial_pf_5M is null then 4 else
	case when emprestimos_cheque_especial_pf_5M >= emprestimos_cheque_especial_pf_6M or emprestimos_cheque_especial_pf_6M is null then 5 else
	case when emprestimos_cheque_especial_pf_6M >= emprestimos_cheque_especial_pf_7M or emprestimos_cheque_especial_pf_7M is null then 6 else
	case when emprestimos_cheque_especial_pf_7M >= emprestimos_cheque_especial_pf_8M or emprestimos_cheque_especial_pf_8M is null then 7 else
	case when emprestimos_cheque_especial_pf_8M >= emprestimos_cheque_especial_pf_9M or emprestimos_cheque_especial_pf_9M is null then 8 else
	case when emprestimos_cheque_especial_pf_9M >= emprestimos_cheque_especial_pf_10M or emprestimos_cheque_especial_pf_10M is null then 9 else
	case when emprestimos_cheque_especial_pf_10M >= emprestimos_cheque_especial_pf_11M or emprestimos_cheque_especial_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cheque_especial_pf,
	CarteiraCredito_PF_Curr::float,
	CarteiraCredito_PF_1M::float,
	CarteiraCredito_PF_2M::float,
	CarteiraCredito_PF_3M::float,
	CarteiraCredito_PF_4M::float,
	CarteiraCredito_PF_5M::float,
	CarteiraCredito_PF_6M::float,
	CarteiraCredito_PF_7M::float,
	CarteiraCredito_PF_8M::float,
	CarteiraCredito_PF_9M::float,
	CarteiraCredito_PF_10M::float,
	CarteiraCredito_PF_11M::float,
	case when CarteiraCredito_PF_10M > CarteiraCredito_PF_11M then 1 else 0 end +
	case when CarteiraCredito_PF_9M > CarteiraCredito_PF_10M then 1 else 0 end +
	case when CarteiraCredito_PF_8M > CarteiraCredito_PF_9M then 1 else 0 end +
	case when CarteiraCredito_PF_7M > CarteiraCredito_PF_8M then 1 else 0 end +
	case when CarteiraCredito_PF_6M > CarteiraCredito_PF_7M then 1 else 0 end +
	case when CarteiraCredito_PF_5M > CarteiraCredito_PF_6M then 1 else 0 end +
	case when CarteiraCredito_PF_4M > CarteiraCredito_PF_5M then 1 else 0 end +
	case when CarteiraCredito_PF_3M > CarteiraCredito_PF_4M then 1 else 0 end +
	case when CarteiraCredito_PF_2M > CarteiraCredito_PF_3M then 1 else 0 end +
	case when CarteiraCredito_PF_1M > CarteiraCredito_PF_2M then 1 else 0 end +
	case when CarteiraCredito_PF_Curr > CarteiraCredito_PF_1M then 1 else 0 end::float as Meses_Aumento_CarteiraCredito_PF
from(select direct_prospect_id, divida_atual_pf,
	count(distinct to_date) filter (where to_date <= data_referencia) as qtd_meses_modalidade_PF,
	--------------------------------EMPRESTIMOS -> CHEQUE ESPECIAL
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as max_emprestimos_cheque_especial_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Empréstimos' and lv2 = 'Cheque Especial') as Ever_emprestimos_cheque_especial_PF,
	--------------------------------CARTEIRA CREDITO
	sum(valor) filter (where to_date = data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','Coobrigação (E)','Créditos a Liberar (G)','Limite de Crédito (H)','Risco Indireto (I)')) as Ever_CarteiraCredito_PF
	from(select direct_prospect_id, data_referencia,divida_atual_pf,regexp_replace(lv1, '\d{2}\s-\s', '') as lv1,replace(replace(replace(replace(regexp_replace(lv2, '\d{4}\s-\s', ''),'  ',' - '),' -- ',' - '),' - ',' - '),'interfinanceiros','Interfinanceiros') as lv2, to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float
		from(select direct_prospect_id, data_referencia,divida_atual_pf,Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).*
			from (select direct_prospect_id, data_referencia,case when data_base = data_referencia then divida_atual_pf else null end as divida_atual_pf,nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).* 
				from (select direct_prospect_id, (data->'indicadores'->>'dividaAtual')::float as divida_atual_pf, to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,data_referencia,(jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*
				from (select direct_prospect_id, data,case when extract(day from ref_date) >= 16 then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') end as data_referencia
					from credito_coleta cc
					join(select 
							dp.direct_prospect_id, 
							coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day' ref_date,
							cpf,
							max_lead_id,
							max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) as max_date_aberto,
							max(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) filter (where coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_date_fechado,
							min(coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id) as min_date_fechado
						from credito_coleta cc
						join direct_prospects dp on cc.documento = dp.cpf
							left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
							from direct_prospects dp
							left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
							left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
							left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
							group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
						left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						join (select cnpj, max(direct_prospect_id) as max_lead_id from direct_prospects group by 1) as max_lead on max_lead.cnpj = dp.cnpj
						where documento_tipo = 'CPF' and tipo = 'SCR' 
							and dp.direct_prospect_id = 375629
						group by 1,2,3,4) as t1 on documento_tipo = 'CPF' and tipo = 'SCR' and t1.cpf = cc.documento and coalesce(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS'),data_coleta) + interval '1 millisecond' * id = case when direct_prospect_id = max_lead_id then max_date_aberto else coalesce(max_date_fechado,min_date_fechado) end) as t2) as t3) as t4) as t5) as t6 group by 1,2) as t7) as SCR_Modal_PF_atual on SCR_Modal_PF_atual.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR MODALIDADES PJ - ATUAL
left join(select 
	direct_prospect_id,
	emprestimos_PJ_Curr::float,
	emprestimos_PJ_1M::float,
	emprestimos_PJ_2M::float,
	emprestimos_PJ_3M::float,
	emprestimos_PJ_4M::float,
	emprestimos_PJ_5M::float,
	emprestimos_PJ_6M::float,
	emprestimos_PJ_7M::float,
	emprestimos_PJ_8M::float,
	emprestimos_PJ_9M::float,
	emprestimos_PJ_10M::float,
	emprestimos_PJ_11M::float
from(select 
		direct_prospect_id,
		--------------------------------EMPRESTIMOS
		sum(valor) filter (where index = 1 and lv1 = 'Empréstimos') as emprestimos_PJ_Curr,
		sum(valor) filter (where index = 2 and lv1 = 'Empréstimos') as emprestimos_PJ_1M,
		sum(valor) filter (where index = 3 and lv1 = 'Empréstimos') as emprestimos_PJ_2M,
		sum(valor) filter (where index = 4 and lv1 = 'Empréstimos') as emprestimos_PJ_3M,
		sum(valor) filter (where index = 5 and lv1 = 'Empréstimos') as emprestimos_PJ_4M,
		sum(valor) filter (where index = 6 and lv1 = 'Empréstimos') as emprestimos_PJ_5M,
		sum(valor) filter (where index = 7 and lv1 = 'Empréstimos') as emprestimos_PJ_6M,
		sum(valor) filter (where index = 8 and lv1 = 'Empréstimos') as emprestimos_PJ_7M,
		sum(valor) filter (where index = 9 and lv1 = 'Empréstimos') as emprestimos_PJ_8M,
		sum(valor) filter (where index = 10 and lv1 = 'Empréstimos') as emprestimos_PJ_9M,
		sum(valor) filter (where index = 11 and lv1 = 'Empréstimos') as emprestimos_PJ_10M,
		sum(valor) filter (where index = 12 and lv1 = 'Empréstimos') as emprestimos_PJ_11M
	from(select 
			direct_prospect_id, 
			regexp_replace(lv1, '\d{2}\s-\s', '') as lv1,
			replace(replace(replace(replace(regexp_replace(lv2, '\d{4}\s-\s', ''),'  ',' - '),' -- ',' - '),' - ',' - '),'interfinanceiros','Interfinanceiros') as lv2, 
			to_date(data,'mm/yyyy'), 
			case when valor is null then 0 else valor end::float,
			row_number() over (partition by direct_prospect_id, lv1,lv2 order by to_date(data,'mm/yyyy') desc) as index			
		from(select 
				direct_prospect_id, 
				Lv1, 
				nome as Lv2, 
				(jsonb_populate_recordset(null::meu2, serie)).*
			from (select 
					direct_prospect_id, 
					nome as Lv1, 
					(jsonb_populate_recordset(null::meu, detalhes)).* 
				from (select 
						direct_prospect_id, 
						(jsonb_populate_recordset( NULL ::lucas, "data" #> '{por_modalidade}')).*
					from (select 
							direct_prospect_id, 
							scr_data as "data"
						from direct_prospects
						where direct_prospect_id = 375629
						) as t1) as t2) as t3) as t4) as t5 group by 1) as t6) as SCR_Modal_Pj_atual on SCR_Modal_Pj_atual.direct_prospect_id = dp.direct_prospect_id
---------------------------
where dp.direct_prospect_id = 375629


