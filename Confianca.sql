select lr.loan_request_id as "ID do pedido", 
	max(dp."name") as "Nome da empresa",
	max(o.date_inserted)::date as "Data da oferta",
	max(o.max_value) as "Valor da oferta",
	max(lr.date_inserted)::date as "Data do pedido",
	max(lr.valor_solicitado) AS "Valor solicitado",
	max(lr.loan_date) "Data do contrato",
	max(lr.value) as "Valor do contrato",
	min(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then cp.criado_em end)::date as "Alçada 1 - data",
	max(case when cp.tipo = 'Analise' then case criado_por when 25 then 'Robson' when 39 then 'Luigi' when 55 then 'Gabriel' when 52 then 'Andre' when 81 then 'Isabel' when 67 then 'BizBot' end end) as "Alçada 1 - analista",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then cp.tipo end) as "Alçada 1 - tipo",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then cp.status end) as "Alçada 1 - status",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then confianca end) as "Alçada 1 - confiança",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then limite end) as "Alçada 1 - limite",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then cp.juros end)::decimal(10,2) as "Alçada 1 - taxa",
	max(case when cp.criado_por in (25,39,55,52,81,67) and cp.tipo = 'Analise' then cp.prazo end)::decimal(10,2) as "Alçada 1 - prazo",
	max(case when criado_por = 25 and cp.tipo = 'Revisao' then 'Robson' when criado_por =  38 then 'Rodrigo' end) as "Alçada 2 - analista",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.tipo end) as "Alçada 2 - tipo",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.status end) as "Alçada 2 - status",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then confianca end) as "Alçada 2 - confiança",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then limite end) as "Alçada 2 - limite",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.juros end)::decimal(10,2) as "Alçada 2 - taxa",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.prazo end)::decimal(10,2) as "Alçada 2 - prazo",
	max(case criado_por when 27 then 'Cristiano' when 26 then 'Chicão' end) as "Alçada 3 - analista",
	max(case when cp.criado_por in (26,27) then cp.tipo end) as "Alçada 3 - tipo",
	max(case when cp.criado_por in (26,27) then cp.status end) as "Alçada 3 - status",
	max(case when cp.criado_por in (26,27) then confianca end) as "Alçada 3 - confiança",
	max(case when cp.criado_por in (26,27) then limite end) as "Alçada 3 - limite",
	max(case when cp.criado_por in (26,27) then cp.juros end)::decimal(10,2) as "Alçada 3 - taxa",
	max(case when cp.criado_por in (26,27) then cp.prazo end)::decimal(10,2) as "Alçada 3 - prazo",
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst  = 1 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst  = 2 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2,
	max(case when fp.numero <> 3 then null else case when ParcSubst  = 3 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt3,
	max(case when fp.numero <> 4 then null else case when ParcSubst  = 4 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt4,
	max(case when fp.numero <> 5 then null else case when ParcSubst  = 5 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt5,
	max(case when fp.numero <> 6 then null else case when ParcSubst  = 6 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt6,
	max(case when fp.numero <> 7 then null else case when ParcSubst  = 7 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt7,
	max(case when fp.numero <> 8 then null else case when ParcSubst  = 8 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt7,
	max(case when fp.numero <> 9 then null else case when ParcSubst  = 9 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt9,
	max(case when fp.numero <> 10 then null else case when ParcSubst  = 10 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt10,
	max(case when fp.numero <> 11 then null else case when ParcSubst  = 11 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt11,
	max(case when fp.numero <> 12 then null else case when ParcSubst  = 12 and VencParcSubst is not null then fpp.inicio_em - VencParcSubst else
		case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt12
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join credito_parecer cp on cp.emprestimo_id = lr.loan_request_id
left join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
left join financeiro_parcela fp on fp.plano_id = fpp.id
left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
	from financeiro_parcela fp
	join financeiro_planopagamento fpp on fpp.id = fp.plano_id
	where fp.status = 'Cancelada'
	group by 1,2) as t1 on t1.emprestimo_id = fpp.emprestimo_id
where coalesce(fp.status,'0') <> 'Cancelada' --and VencParcSubst is not null
group by 1
order by 1 desc

select distinct emprestimo_id
from credito_parecer

--------------------------------
select * from (	
	select lr.loan_request_id, lr.date_inserted, lr.value, dp.bizu_score::int, lr.status,
	max(case when cp.criado_por in (25,39) then cp.confianca else null end) as NotaAlç1,
	max(case when cp.criado_por in (25,39) then cp.limite else null end) as LimiteAlç1,
	max(case when cp.criado_por in (25,39) then cp.status else null end) as CallAlç1,
	max(case when cp.criado_por =38 then cp.confianca else null end) as NotaAlç2,
	max(case when cp.criado_por =38 then cp.limite else null end) as LimiteAlç2,
	max(case when cp.criado_por =38then cp.status else null end) as CallAlç2
	from credito_parecer cp
	join loan_requests lr on lr.loan_request_id = cp.emprestimo_id
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	where lr.status ='REJECTED'
	group by 1,2,3,4,5) as t1
where CallAlç1 <> 'Rejeitar' and CallAlç1 is not null and CallAlç2 = 'Rejeitar'

--DECISÕES DAS ALÇADAS 
select lr.loan_request_id,
	dp."name",
	o.date_inserted as DataOferta,
	lr.loan_date DataContrato,
	o.max_value as ValorOferta,
	lr.value as ValorContrato,
	max(case when criado_por = 25 then confianca else null end) as NotaRobson, 
	max(case when criado_por = 25 then limite else null end) as LimiteRobson, 
	max(case when criado_por = 39 then confianca else null end) as NotaLuigi, 
	max(case when criado_por = 39 then limite else null end) as LimiteLuigi, 
	max(case when criado_por = 55 then confianca else null end) as NotaGabriel, 
	max(case when criado_por = 55 then limite else null end) as LimiteGabriel, 
	max(case when criado_por = 52 then confianca else null end) as NotaAndre, 
	max(case when criado_por = 52 then limite else null end) as LimiteAndre, 
	max(case when criado_por = 38 then confianca else null end) as NotaRodrigo, 
	max(case when criado_por = 38 then limite else null end) as LimiteRodrigo, 
	max(case when criado_por = 27 then confianca else null end) NotaCristiano,
	max(case when criado_por = 27 then limite else null end) LimiteCristiano,
	max(case when criado_por = 26 then confianca else null end) NotaChicao,
	max(case when criado_por = 26 then limite else null end) LimiteChicao,
	count(distinct fpp.id) as count_renegotiation,
	max(fp.numero) as termo_corrente,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2,
	max(case when fp.numero <> 3 then null else case when ParcSubst = 3 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt3,
	max(case when fp.numero <> 4 then null else case when ParcSubst = 4 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt4,
	max(case when fp.numero <> 5 then null else case when ParcSubst = 5 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt5,
	max(case when fp.numero <> 6 then null else case when ParcSubst = 6 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt6,
	max(case when fp.numero <> 7 then null else case when ParcSubst = 7 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt7,
	max(case when fp.numero <> 8 then null else case when ParcSubst = 8 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt8,
	max(case when fp.numero <> 9 then null else case when ParcSubst = 9 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt9,
	max(case when fp.numero <> 10 then null else case when ParcSubst = 10 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt10,
	max(case when fp.numero <> 11 then null else case when ParcSubst = 11 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt11,
	max(case when fp.numero <> 12 then null else case when ParcSubst = 12 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt12
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	left join credito_parecer cp on cp.emprestimo_id = lr.loan_request_id
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
	where fp.status not in ('Cancelada','EmEspera') and lr.status = 'ACCEPTED'
	group by 1,2,3,4,5,6
	
	
	
--APENAS NOTAS DAS ALÇADAS
select dp.direct_prospect_id, 
	min(case when cp.tipo = 'Analise' then cp.criado_em end)::date as "Alçada 1 - data",
	max(case when cp.tipo = 'Analise' then case criado_por when 25 then 'Robson' when 39 then 'Luigi' when 55 then 'Gabriel' when 52 then 'Andre' when 81 then 'Isabel' when 67 then 'BizBot'  else 'Outro' end end) as "Alçada 1 - analista",
	max(case when cp.tipo = 'Analise' then cp.tipo end) as "Alçada 1 - tipo",
	max(case when cp.tipo = 'Analise' then cp.status end) as "Alçada 1 - status",
	max(case when cp.tipo = 'Analise' then confianca end) as "Alçada 1 - confiança",
	max(case when cp.tipo = 'Analise' then limite end) as "Alçada 1 - limite",
	max(case when cp.tipo = 'Analise' then cp.juros end)::decimal(10,2) as "Alçada 1 - taxa",
	max(case when cp.tipo = 'Analise' then cp.prazo end)::decimal(10,2) as "Alçada 1 - prazo",
	max(case when criado_por = 25 and cp.tipo = 'Revisao' then 'Robson' when criado_por =  38 then 'Rodrigo' end) as "Alçada 2 - analista",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.tipo end) as "Alçada 2 - tipo",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.status end) as "Alçada 2 - status",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then confianca end) as "Alçada 2 - confiança",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then limite end) as "Alçada 2 - limite",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.juros end)::decimal(10,2) as "Alçada 2 - taxa",
	max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') or cp.criado_por = 38 then cp.prazo end)::decimal(10,2) as "Alçada 2 - prazo",
	max(case criado_por when 27 then 'Cristiano' when 26 then 'Chicão' end) as "Alçada 3 - analista",
	max(case when cp.criado_por in (26,27) then cp.tipo end) as "Alçada 3 - tipo",
	max(case when cp.criado_por in (26,27) then cp.status end) as "Alçada 3 - status",

	case when max(case when (cp.criado_por = 25 and cp.tipo = 'Revisao') then confianca) is null then 
	
	max(case when cp.criado_por in (26,27,30) then confianca end) as "Alçada 3 - confiança",

	
	
	max(case when cp.criado_por in (26,27) then limite end) as "Alçada 3 - limite",
	max(case when cp.criado_por in (26,27) then cp.juros end)::decimal(10,2) as "Alçada 3 - taxa",
	max(case when cp.criado_por in (26,27) then cp.prazo end)::decimal(10,2) as "Alçada 3 - prazo"
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join credito_parecer cp on cp.emprestimo_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1
order by 1 desc
