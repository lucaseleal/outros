--Queries em execu�ao
select 
	(current_timestamp - query_start),
	pid,
	query 
from pg_stat_activity 
where state='active' 
order by 1 desc;

select count(*)
from mv_lead_tape mlt 

--fdp que t� bloqueando as queries
select 
	(current_timestamp - query_start),
	pid, 
	pg_blocking_pids(pid) as blocked_by, 
	query as blocked_query
from pg_stat_activity 
where pg_blocking_pids(pid)::text != '{}';

--descobrir user de fdp
select *
from pg_stat_activity
where pid = 31793

SELECT c.oid,c.*,d.description,pg_catalog.pg_get_expr(c.relpartbound, c.oid) as partition_expr,  pg_catalog.pg_get_partkeydef(c.oid) as partition_key 
FROM pg_catalog.pg_class c
LEFT OUTER JOIN pg_catalog.pg_description d ON d.objoid=c.oid AND d.objsubid=0 AND d.classoid='pg_class'::regclass
WHERE c.relnamespace=$1 AND c.relkind not in ('i','I','c')

--matar fdp
select pg_terminate_backend(13344);

SELECT dependent_ns.nspname as dependent_schema
, dependent_view.relname as dependent_view 
, source_ns.nspname as source_schema
, source_table.relname as source_table
, pg_attribute.attname as column_name
FROM pg_depend 
JOIN pg_rewrite ON pg_depend.objid = pg_rewrite.oid 
JOIN pg_class as dependent_view ON pg_rewrite.ev_class = dependent_view.oid 
JOIN pg_class as source_table ON pg_depend.refobjid = source_table.oid 
JOIN pg_attribute ON pg_depend.refobjid = pg_attribute.attrelid 
    AND pg_depend.refobjsubid = pg_attribute.attnum 
JOIN pg_namespace dependent_ns ON dependent_ns.oid = dependent_view.relnamespace
JOIN pg_namespace source_ns ON source_ns.oid = source_table.relnamespace
WHERE 
source_ns.nspname = 'lucas_leal'
AND source_table.relname = 'mv_cnd_pgfn_coleta'
AND pg_attribute.attnum > 0 
--AND pg_attribute.attname = 'new_processo_judicial_socios'
ORDER BY 1,2;

--Procura tabela com nome da coluna
select t.table_schema,
       t.table_name
from information_schema.tables t
inner join information_schema.columns c on c.table_name = t.table_name 
                                and c.table_schema = t.table_schema
where c.column_name = 'last_name'
      and t.table_schema not in ('information_schema', 'pg_catalog')
      and t.table_type = 'BASE TABLE'
order by t.table_schema;

