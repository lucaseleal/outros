create table t_bndes_infosimples_fgts (loan_app_id text, cnpj text, data_output jsonb,loan_request_id bigint)

select 
	dp.loan_application_id,
	dp.cnpj,
	lt.loan_request_id 
from data_science.loan_tape lt 
join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
where loan_contract_date >= '2021-07-01'


select 
	cnpj,
	loan_app_id ,
	loan_request_id ,
	count(*) filter (where data_output->> 'code' = '200') as n_certo,
	count(*) filter (where data_output->> 'code' != '200') as n_errado,
	count(*) filter (where tipo = 'nova') as n_nova,
	count(*) filter (where tipo = '2avia') as n_2via,
	count(*) n_total
from lucas_leal.t_bndes_infosimples_pgfn
group by 1,2,3


select 
	lead_cohort_lead,
	count(*) filter (where lead_status = 'Negado Bizu') as negado_bizu,
	count(*) as total,
	count(*) filter (where lead_status = 'Negado Bizu')::numeric / count(*) as perc_negado_bizu
from data_science.lead_tape lt 
where lead_informed_month_revenue >= 250000
group by 1

select 
	lead_biz_prime_matriz,
	count(*) filter (where lead_status = 'Negado Bizu') as negado_bizu,
	count(*) as total,
	count(*) filter (where lead_status = 'Negado Bizu')::numeric / count(*) as perc_negado_bizu
from data_science.lead_tape lt 
where lead_cohort_lead >= '2021-06-01' and lead_informed_month_revenue >= 3000000
group by 1


select 
	*
from(select 
		lt.loan_request_id ,
		lt.neoway_government_debt,
		tbip.data_output -> 'data'->>'certidao' as certidao,
		row_number () over (partition by tbip.loan_request_id order by tipo) as indice,
		tbip.data_output ->> 'code' as codigo_retorno,
		tbip.data_output -> 'data'->>'validade' as validade
	from data_science.lead_tape lt 
	join lucas_leal.t_bndes_infosimples_pgfn tbip on tbip.loan_request_id = lt.loan_request_id) as t1
where indice = 1


select data_output -> 'data' ->> 'conseguiu_emitir_certidao_negativa',count(*)
from lucas_leal.t_bndes_infosimples_mte
group by 1


select data_output ->> 'code',count(*)
from lucas_leal.t_bndes_infosimples_fgts
group by 1


select data_output -> 'data' ->> 'situacao',count(*)
from lucas_leal.t_bndes_infosimples_fgts
group by 1

select cnpj, data_output ->> 'code'
from lucas_leal.t_bndes_infosimples_fgts
where data_output -> 'data' ->> 'situacao' is null


select 
	data_output ->> 'code',
	count(*)
from lucas_leal.t_bndes_infosimples_fgts t1
--join (select cnpj,data_output , row_number () over (partition by tbip.loan_request_id order by tipo) as indice) as  from lucas_leal.t_bndes_infosimples_pgfn t2 on t2.cnpj = t1.cnpj
group by 1


select *
from t_bndes_infosimples_pgfn tbip 
where data_output -> 'code' = '200'
limit 10



select 
	lead_id,
	loan_cohort_loan,
	infosimples_cnd_pgfn,
	infosimples_cnd_mte,
	infosimples_cnd_fgts,
	(neoway_government_debt > 0)::int has_neoway_pgfn
from data_science.lead_tape lt 
join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
join (select distinct cnpj from lucas_leal.t_bndes_infosimples_pgfn) tbip on tbip.cnpj = dp.cnpj 
where lt.loan_contract_date >= '2021-07-01'

select 
	loan_request_is_approved ,
	count(*)
from data_science.lead_tape lt 
group by 1


select data_output ->> 'code',count(*)
from lucas_leal.t_bndes_infosimples_pgfn tbip 
group by 1


select *
from(
select 
	cnpj,
	data_output->>'code',
	data_output -> 'data' ->> 'certidao',
	data_output -> 'data' ->> 'tipo',
	row_number() over (partition by cnpj order by data_output->>'code') as indice
from t_bndes_infosimples_pgfn t1
where tipo in ('nova','2avia')
	and cnpj in ('32825767000146',
'36905195000166',
'14308401000118',
'35715968000189',
'13378935000158',
'37540214000160',
'05388412000164',
'10543355000180',
'37099748000101',
'37006916000169',
'17779487000155',
'17617257000190',
'23846809000134',
'24838389000107',
'04366573000194',
'10948791000130',
'13485075000151',
'21327827000184',
'12073346000107',
'13131221000140',
'31139721000100',
'33795604000120',
'21572030000142',
'36807463000107',
'21089175000197',
'22557473000127',
'35808770000140',
'13369413000190',
'36144846000142',
'08334749000187',
'10829336000115',
'17606139000186',
'21933896000131',
'30209107000105',
'37344810000175',
'22839238000148',
'31485455000178',
'08486210000143',
'29209618000148',
'28512710000110',
'33011292000117',
'12338921000148',
'11082505000169',
'01888349000174',
'24584952000168',
'17861986000197',
'23167441000188',
'26168519000177',
'36242178000196',
'17524726000126',
'06094102000108',
'16708269000167',
'14097663000180',
'11001109000160',
'13327554000140',
'17050418000106',
'24607207000197',
'18454855000158',
'06197380000182',
'21617921000178',
'25318172000139',
'01693339000183',
'37439179000198',
'23806109000116',
'28631985000172',
'18758641000175',
'40412082000104',
'03395424000190',
'23349377000156',
'14607529000182',
'26445526000179',
'27013781000105',
'07632965000146',
'36640131000180',
'10575107000111',
'23970235000101',
'06922933000112',
'17272043000129',
'36338982000173',
'37702159000168',
'17780359000121',
'14248873000122',
'35509648000172',
'01961452000100',
'26471471000171',
'25282291000189',
'20684021000180',
'12386331000190',
'27191877000163',
'35344798000173',
'31162774000142',
'33479392000172',
'00322454000189',
'12552066000173',
'36827170000191',
'27915839000106',
'18317629000125',
'35258560000125',
'34181236000193',
'37061042000142',
'11203157000130',
'27739586000167',
'13569354000101',
'11147051000167',
'80796436000178',
'28242458000176',
'33777238000187',
'17954671000194',
'29216954000118',
'14229713000136',
'00872222000102',
'21894487000173',
'09000920000184',
'10804476000139',
'30356340000110',
'36600664000139',
'19327955000186',
'34806200000158',
'35739888000163',
'09942529000107',
'07397701000155',
'35061897000148',
'20940112000130',
'35523883000107',
'06262957000192',
'05755760000122',
'05671893000110',
'03601949000135',
'21205178000149',
'21152959000112',
'33327562000101',
'23042866000160',
'11835178000179',
'11733006000194',
'17304456000148',
'29034530000132',
'28377142000191',
'31449642000104',
'10422540000117',
'17022862000118',
'33690820000101',
'12733071000182',
'27891599000157',
'21045564000110',
'13081099000145',
'35123871000187',
'34462076000150',
'35449876000102',
'31671199000103',
'12360165000153',
'12269886000152',
'34576616000127',
'27988523000144',
'25361207000112',
'18632342000190',
'12536376000102',
'25261380000149',
'16307244000151',
'53254850000182',
'08367462000153',
'24302892000143',
'02235497000152',
'33274570000129',
'10818080000140',
'30822690000125',
'26510591000130',
'07516041000184',
'13268952000132',
'10782301000178',
'01440077000145',
'03821107000199',
'36994592000151',
'03000305000191',
'20911081000199',
'27937543000196',
'12846856000161',
'27114284000101',
'33662653000195',
'30281260000143',
'17216843000122',
'22883653000107',
'09088809000191',
'19080544000139',
'23553063000170',
'17198935000127',
'33070562000160',
'23386437000100',
'14800200000133',
'22745551000117',
'32134677000109',
'14265578000184',
'26649410000151',
'18010829000130',
'10878943000175',
'20017819000179',
'13968287000190',
'23808215000139',
'21162149000147',
'37606923000100',
'23117587000119',
'03541629000137',
'07996683000128',
'19263180000122',
'26826111000145',
'27402920000192',
'24271554000191',
'20026659000124',
'28666751000160',
'11383952000158',
'11522713000131',
'35192543000132',
'25123504000120',
'17888483000105',
'35211332000108',
'21794181000145',
'17809790000153',
'20126937000115',
'11634671000120',
'10538319000129',
'37088434000103',
'01686260000125',
'14507557000128',
'16714886000175',
'27100219000119',
'28305380000191',
'14904894000159',
'23343314000192',
'00317633000128',
'35397930000105',
'17700574000175',
'26250166000150',
'09094169000122',
'02414810000110',
'18513167000111',
'11013771000130',
'28476230000140',
'03551749000115',
'34469472000100',
'38016405000190',
'32302728000164',
'10485351000193',
'50977891000109',
'10296571000179',
'28780434000170',
'31217063000128',
'29045172000163',
'20268062000196',
'19635948000141',
'38194055000152',
'36380502000132',
'28683400000168',
'20753130000102',
'63423859000123',
'29307338000172',
'28696346000195',
'22516010000117',
'07212283000184',
'09365929000199',
'09113404000166',
'26958539000141',
'17143308000199',
'35364380000128',
'37933771000141',
'36543217000195',
'10337424000108',
'02094635000120',
'09572305000142',
'04220044000188',
'22719757000172',
'17887313000106',
'07951096000112',
'12603165000137',
'25154476000108',
'25075811000182',
'31859405000103',
'35504494000126',
'00285607000165',
'13273589000143',
'28577075000159',
'03826777000106',
'33055487000169',
'18050087000177',
'15219339000150',
'04723881000120',
'32622491000107',
'12097381000158',
'30169962000130',
'10700853000190',
'29890786000141',
'25054380000178',
'28934563000176',
'13305599000113',
'29539852000133',
'27646673000170',
'22910732000151',
'29865428000189',
'37497213000180',
'31269972000100',
'15378075000188',
'35508524000172',
'17609591000100',
'14376678000188',
'23544623000120',
'24758660000102',
'15000519000147',
'07645877000189',
'33139192000170',
'06064705000159',
'31870197000143',
'07062863000132',
'24894466000146',
'21833682000193',
'17198119000113',
'33670859000167',
'19766415000107',
'26153474000167',
'10775261000137',
'31376576000181',
'32932100000142',
'12475424000191',
'26968783000195',
'34083999000100',
'12918733000199',
'36533889000110',
'03488852000168',
'14028890000154',
'29605081000135',
'38053589000169',
'12099373000140',
'27120850000180',
'10489468000145',
'35276029000185',
'18596929000190',
'27336931000111',
'37694543000166',
'26943888000190',
'24436543000114',
'72583891000167',
'01814070000146',
'26889127000105',
'48123913000121',
'07271874000122',
'24321565000139',
'30378230000150',
'27540211000173',
'15418351000194',
'13328310000181',
'27043790000149',
'24794809000109',
'08171744000180',
'26273634000101',
'25432546000142',
'16699396000147',
'20886138000147',
'36487989000157',
'17450825000100',
'35090202000156',
'29187961000139',
'27218121000160',
'35561577000157',
'24198555000158')) as t1 where indice = 1


	select 
	    dp.loan_application_id,
	    dp.cnpj,
	    lt.loan_request_id,
	    (lt.lead_opt_in_date::date - '6 months'::interval)::date as dt_inicio,
	    (lt.lead_opt_in_date::date + '15 days'::interval)::date as dt_fim
	from data_science.lead_tape lt 
	join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
	left join lucas_leal.t_bndes_infosimples_pgfn as t1 on t1.loan_app_id = dp.loan_application_id 
	where loan_is_loan = 1 
		and loan_cohort_loan between '2019-01-01' and '2021-06-01'
		and lead_is_caas = 0 
		and lead_is_mca = 0 
		and t1.loan_app_id is null 
		and (lead_informed_month_revenue > 30000 or (lead_informed_month_revenue > 50000 and neoway_is_mei = 1)) 
		and loan_net_value >= 30000 
		and loan_final_gross_interest_rate < 3.99
--		and lt.lead_id = 393569

		
select 
	mlt.loan_request_id,
	mlt.lead_opt_in_date::date,
	data_output ->> 'code' as codigo,
	coalesce(data_output -> 'data'->>'certidao',certidoes ->> 'tipo','Indisponivel') as certidao,
	to_date(certidoes ->> 'data_emissao','dd/MM/yyyy') as data_emissao,
	to_date(certidoes ->> 'data_validade','dd/MM/yyyy') as data_validade,
	to_date(certidoes ->> 'data_validade','dd/MM/yyyy') - mlt.lead_opt_in_date::date,	
	row_number() over (partition by mlt.loan_request_id order by to_date(certidoes ->> 'data_emissao','dd/MM/yyyy') desc) as indice
from t_bndes_infosimples_pgfn t1
join mv_lead_tape mlt on mlt.loan_request_id = t1.loan_request_id 
left join jsonb_array_elements(data_output-> 'data' -> 'certidoes') as certidoes on true
where tipo ~~ 'backtest%'

select 
	t1.loan_request_id ,
	(count(*) filter (where coalesce(data_output -> 'data'->>'tipo',certidoes ->> 'tipo') in ('Negativa','Positiva com efeitos de negativa')
		and case when t1.tipo ~~ 'backtest%' then coalesce(to_date(certidoes ->> 'data_validade','dd/MM/yyyy'),'2999-01-01'::date) >= dp.opt_in_date::date and coalesce(to_date(certidoes ->> 'data_emissao','dd/MM/yyyy'),'1900-01-01') <= dp.opt_in_date::date else true end))::int as tem_cnd_pgfn
from lucas_leal.t_bndes_infosimples_pgfn t1
join public.direct_prospects dp on dp.loan_application_id = t1.loan_app_id 
left join jsonb_array_elements(t1.data_output-> 'data' -> 'certidoes') as certidoes on true
group by 1


grant select on t_bndes_infosimples_pgfn to ricardo_filho

update t_bndes_infosimples_pgfn t1
set tipo = 'nova'
where tipo = 'backtest2' and data_output -> 'header' -> 'parameters' ->> 'preferencia_emissao' = 'nova'

select 
		loan_app_id,
		row_number () over (partition by loan_request_id order by tipo) as indice
	from lucas_leal.t_bndes_infosimples_pgfn

    select 
        dp.loan_application_id,
        dp.cnpj,
        lt.loan_request_id 
    from data_science.lead_tape lt 
    join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
    left join lucas_leal.t_bndes_infosimples_mte as t1 on t1.loan_app_id = dp.loan_application_id 
    where lt.loan_cohort_loan = '2021-09-01' and t1.loan_app_id is null and lt.lead_is_caas = 0 and lt.lead_is_mca = 0 and lt.lead_is_outras_esteiras = 0 and lt.loan_is_loan = 1

    select 
        dp.loan_application_id,
        dp.cnpj,
        lt.loan_request_id 
    from data_science.lead_tape lt 
    join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
    left join lucas_leal.t_bndes_infosimples_fgts as t1 on t1.loan_app_id = dp.loan_application_id 
    where lt.loan_cohort_loan = '2021-09-01' and t1.loan_app_id is null and lt.lead_is_caas = 0 and lt.lead_is_mca = 0 and lt.lead_is_outras_esteiras = 0 and lt.loan_is_loan = 1

		row_number() over (partition by lt.lead_behaviour_rn_menos1_id order by lt.loan_is_loan desc,lt.lead_id desc) as indice_lead,

		
select 
--    dp.loan_application_id,
--    dp.cnpj,
--    lt.loan_request_id,
--    (lt.lead_opt_in_date::date - '6 months'::interval)::date as dt_inicio,
--    (lt.lead_opt_in_date::date + '5 days'::interval)::date as dt_fim	    
lt.
from data_science.lead_tape lt 
join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
left join lucas_leal.t_bndes_infosimples_pgfn as t1 on t1.loan_app_id = dp.loan_application_id 
where loan_is_loan = 0 and lead_cohort_lead between '2019-01-01' and '2021-09-01'
    and lead_is_caas = 0 
    and lead_is_mca = 0 
    and ((neoway_is_mei =  0 and lead_informed_month_revenue > 30000) or (neoway_is_mei = 1 and lead_informed_month_revenue > 50000)) 
    and lead_application_amount >= 30000  
    and lead_status = 'Negado Bizu'
    and lead_biz_prime_matriz < 9
--    and t1.loan_app_id is null
		
select *
from t_bndes_infosimples_pgfn
where loan_request_id = 0

select 
--	loan_cohort_loan ,
--	ceiling(perc_pago_units*100/10)*10 as perc_pago_units,
	collection_loan_fully_paid,
	count(*)
--*
from(
	select 
	    dp.loan_application_id,
	    dp.cnpj,
		lt.lead_id,
		row_number() over (partition by lt.lead_client_id order by lt.lead_id desc) as indice_lead,
		collection_loan_fully_paid,
		collection_loan_amount_paid / collection_original_p_plus_i as perc_pago_valor,
		collection_number_of_installments_paid::numeric / collection_loan_current_term as perc_pago_units,
		lead_client_id,
		loan_is_renewal,
		loan_cohort_loan,
		collection_loan_curr_late_by,
		collection_loan_max_late_by 
	from mv_lead_tape lt 
	join public.direct_prospects dp on dp.direct_prospect_id = lt.lead_id
	where loan_is_loan = 1
		and lt.lead_is_caas = 0 
		and lt.lead_is_mca = 0 
		and collection_loan_curr_late_by <= 5
--		and collection_loan_max_late_by <= 90
		and loan_cohort_loan >= '2019-01-01'
) as t1
where indice_lead = 1 and (/*perc_pago_valor >= .5 or */perc_pago_units >= .5 or collection_loan_fully_paid = 1) 
group by 1--,2

select lead_id,lead_behaviour_rn_menos1_score 
from mv_lead_tape lt 
where lt.lead_is_caas = 0 and lt.lead_is_mca = 0 and loan_is_renewal > 0 and lead_behaviour_rn_menos1_score is null
limit 10

select * from (
select 
	loan_app_id,
	row_number() over (partition by loan_app_id order by t1.data_output->>'code',to_date(certidoes ->> 'data_emissao','dd/MM/yyyy') desc) as indice_coleta,
	coalesce(data_output -> 'data'->>'tipo',certidoes ->> 'tipo') as tipo
from lucas_leal.t_bndes_infosimples_pgfn t1
join public.direct_prospects dp on dp.loan_application_id = t1.loan_app_id 
left join jsonb_array_elements(t1.data_output-> 'data' -> 'certidoes') as certidoes on true
where t1.tipo not like 'backtest%' or (coalesce(to_date(certidoes ->> 'data_validade','dd/MM/yyyy'),'2999-01-01'::date) >= dp.opt_in_date::date and coalesce(to_date(certidoes ->> 'data_emissao','dd/MM/yyyy'),'1900-01-01') <= dp.opt_in_date::date)
) as t1 where indice_coleta = 1

--where loan_request_id = 0--t1.tipo in ('nova','2avia')
--group by 1

