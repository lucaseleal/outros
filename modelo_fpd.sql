select experian_consulta_financeiras 
from data_science.lead_tape lt 
where lead_id = 1017894


select
	t1.lead_id,
	t1.neoway_check_materialidade,
	t1.lead_age_revenue_discrepancy,
	t1.loan_bank_id,
	t1.experian_consulta_propria_empresa,
	t1.experian_consulta_factoring,
	t1.experian_consulta_seguradoras,
	t1.experian_consulta_financeiras,
	t1.experian_consulta_escritorios_cobranca,
	t1.experian_consulta_provedores_dados,
	t1.experian_consulta_fornecedores,
	t1.experian_consulta_indefinido,
	t1.signers_majorshareholder_age - t1.neoway_company_age < 24 as socio_menos_24a,
	case t3.is_shareholder when 1 then 'socio direto' when 0 then 'nao socio' end as new_identidade_relacao_solicitante,
	t4.score_emailage > 500 as new_identidade_email_fraude,
--	replace(upper(lucas_leal.remove_acentos(t19.street)),' ','') as balcao_rua,
--	t19.zip_code as balcao_cep,
--	replace(upper(lucas_leal.remove_acentos(t2.signer_street_address)),' ','') as signer_street_address,
--	t2.signer_zip_code_address,
	(greatest(t1.experian_consulta_financeiras,t1.experian_consulta_escritorios_cobranca,0) > 0 and greatest(t1.experian_consulta_factoring,t1.experian_consulta_fornecedores,t1.experian_consulta_indefinido,t1.experian_consulta_provedores_dados,t1.experian_consulta_seguradoras,0) = 0) as new_apenas_consulta_financeira,
	t1.neoway_estimate_number_of_employees > 0 as new_tem_empregados,
	(coalesce((t1.experian_consulta_seguradoras > 0)::int,0) +
		coalesce((t1.experian_consulta_fornecedores > 0)::int,0) +
		coalesce((t1.neoway_estimate_number_of_employees > 0)::int,0) +
		coalesce((t9.imposto_saida > 0)::int,0) +
		coalesce((t9.salario_saida > 0)::int,0) +
		coalesce(t1.neoway_check_materialidade::int,0)) as new_score_materialidade,
	case when coalesce(t8.long_term_debt_pf_curr,q2.long_term_debt_pf_curr) = 0 then 'Sem carteira'
		when coalesce(t8.long_term_debt_pf_curr,q2.long_term_debt_pf_curr) * 1000 / t1.lead_informed_month_revenue >= 0.1 then 'Adequado ao porte'
		when coalesce(t8.long_term_debt_pf_curr,q2.long_term_debt_pf_curr) * 1000 / t1.lead_informed_month_revenue < 0.1 then 'Inferior ao porte'
	end as new_socio_major_carteira_credito,
	case when left(t20.vinculo,5) in ('Sócio','Sócia') or t20.vinculo = 'Presidente' then 'Socio'
		when t20.vinculo in ('Administrador','Procurador') or (t20.vinculo = 'Outros' and t20.lead_marketing_channel_group != 'agent') then 'Admin/procurador'
		else 'Outros'
	end as new_solicitante_vinculo_admin,
	greatest((greatest(t1.experian_consulta_fornecedores ,0) > 0 and greatest(t1.experian_consulta_factoring,t1.experian_consulta_indefinido,t1.experian_consulta_provedores_dados ,t1.experian_consulta_seguradoras ,t1.experian_consulta_financeiras ,t1.experian_consulta_escritorios_cobranca ,t1.experian_consulta_propria_empresa ,0) = 0)::int ,
		(greatest(t1.experian_consulta_seguradoras ,0) > 0 and greatest(t1.experian_consulta_factoring ,t1.experian_consulta_indefinido ,t1.experian_consulta_fornecedores ,t1.experian_consulta_provedores_dados ,t1.experian_consulta_financeiras ,t1.experian_consulta_escritorios_cobranca ,t1.experian_consulta_propria_empresa ,0) = 0)::int ,		
		coalesce(neoway_check_materialidade::int,0)) as new_bem_consultado,
	t30.perc_faturamento_comprovado_medio as perc_bizcredit_faturamento_comprovado,
	t31.perc_faturamento_medio as perc_bizextrato_faturamento_comprovado,
	t1.collection_fpd_by ,
	t1.collection_spd_by ,
	t1.collection_tpd_by ,
	t1.collection_4pd_by ,
	t1.collection_is_current_fpd ,
	t1.collection_is_current_spd ,
	t1.collection_is_current_tpd ,
	t1.collection_is_current_4thpd ,
	case when (t1.neoway_is_mei = 1 or t20.name ~ '.*\sMEI$' or t20.natureza_juridica = '2135') and t20.name !~ '.*\sLTDA$ | .*\sEIRELI$ | .*\sME$' then 1 else 0 end as is_mei,
	t1.lead_informed_month_revenue,
	t1.loan_cohort_loan,
	t1.loan_is_renewal,
	(tpace.utm_source is not null)::int as caas,
	t1.collection_loan_curr_late_by,
	t1.collection_loan_fully_paid,
	tcbt.banco,
	tcbt.tipo,
	t1.lead_extrato_score_class,
	t1.lead_mesa_score_ranking,
	t1.lead_classe_bizux,
	t1.lead_biz_bizu3_class,
	t1.lead_biz_prime_matriz,
	tascc.tem_contato as cobranca_tem_contato,
	t1.neoway_company_age,
	t1.lead_mesa_score_prob,
	t1.loan_is_loan,
	t1.loan_request_status,
	t1.loan_1st_pmt_due_date,
	t1.loan_request_id,
	t1.collection_loan_amount_paid,
	t1.collection_original_ead,
	t1.collection_original_p_plus_i,
	t1.loan_gross_value,
	case when t1.lead_marketing_channel in ('estimulo2020', 'magalu', 'bom-pra-credito', 'amedigital', 'capitalemp','android', 'finpass') then 0 else 1 end as elegivel_neuro_id,
	t20.api_direto,
	(t20.outbound_loan_app_id is not null)::int as is_outbound
from data_science.lead_tape t1
/*
left join (select 
		direct_prospect_id,
		signer_street_address,
		signer_zip_code_address
	from lucas_leal.t_signers_data_full 
	where indice_socio_majoritario = 1 and signer_share_percentage > 0) t2 on t2.direct_prospect_id = t1.lead_id
*/
left join lucas_leal.t_signers_indicators_full as t3 on t3.direct_prospect_id = t1.lead_id
left join t_bizcredit_emailage_full t4 on t4.emprestimo_id = t1.loan_request_id
left join lucas_leal.t_scr_pf_get_history_data_full as t8 on t8.direct_prospect_id = t1.lead_id
left join lucas_leal.t_bizcredit_transactions_corrigida t9 on t9.loan_request_id = t1.loan_request_id --base com filtro de data para analises posteriores ao emprestimo
left join lucas_leal.t_client_addresses_full as t19 on t19.loan_request_id = t1.loan_request_id 
left join (select
		dp.direct_prospect_id,
		dp.vinculo,
		dp.natureza_juridica,
		dp."name",
		dp.trackers->>'source_api' as api_direto,
		dp.outbound_loan_app_id,
		coalesce(case upper(lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source))) 
			when 'ADWORDS' then 'adwords'
			when 'AGENT' then 'agent'
			when 'BIDU' then 'bidu'
			when 'BING' then 'Bing'
			when 'CREDITA' then 'credita'
			when 'FACEBOOK' then 'facebook'
			when 'FACEBOOK_ORG' then 'facebook'
			when 'FINANZERO' then 'finanzero'
			when 'FINPASS' then 'finpass'
			when 'GERU' then 'geru'
			when 'INSTAGRAM' then 'instagram'
			when 'JUROSBAIXOS' then 'jurosbaixos'
			when 'KONKERO' then 'konkero'
			when 'LINKEDIN' then 'linkedin'
			when 'MGM' then 'member-get-member'
			when 'ORGANICO' then 'organico'
			when 'BLOG' then 'organico'
			when 'SITE' then 'organico'
			when 'DIRECT_CHANNEL' then 'organico'
			when 'ANDROID' then 'organico'
			when 'RENEWAL' then 'organico'
			when 'EMAIL' then 'organico'
			when 'RD' then 'organico'
			when 'RD#/' then 'organico'
			when 'RDSTATION' then 'organico'
			when 'RD+STATION' then 'organico'
			when 'AMERICANAS' then 'other' 
			when 'IFOOD' then 'other' 
			when 'PEIXE-URBANO' then 'other' 
			when 'OUTBRAIN' then 'outbrain'
			when 'TABOOLA' then 'taboola'
			when 'CHARLES/' then 'agent'
			when 'MARCELOROMERA' then 'agent'
			when 'PBCONSULTORIA' then 'agent'
			when 'HMSEGUROS' then 'agent'
			when 'COMPARAONLINE' then 'agent'
			when 'CREDEXPRESS#/' then 'agent'
			else case when pp.identificacao is not null then 'agent' else dp.utm_source end
		end,'other') as lead_marketing_channel_group
	from public.direct_prospects dp
	left join (select distinct identificacao from parceiros_parceiro) pp on pp.identificacao = replace(dp.utm_source,'#/','')) as t20 on t20.direct_prospect_id = t1.lead_id
left join lucas_leal.t_bizextrato4_transactions_indicators_full as t30 on t30.loan_request_id = t1.loan_request_id 
left join lucas_leal.t_bizextrato_indicators_full as t31 on t31.loan_request_id = t1.loan_request_id 
left join data_science.t_processo_automacao_canais_excluidos tpace on tpace.utm_source = t1.lead_marketing_channel 
left join lucas_leal.t_codigo_bancos_tipo tcbt on tcbt.codigo = t1.loan_bank_id 
left join wellerson_silva.t_anexo_sem_contato_cob tascc on tascc.loan_request_id = t1.loan_request_id 
left join (select
		direct_prospect_id,
		case when thin_file is null then long_term_debt_pf_curr else 0 end long_term_debt_pf_curr
	from(SELECT t1.direct_prospect_id,
            replace(replace(replace(cc.data -> 'historico'::text -> 'Carteira de Crédito'::text -> 0 ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS long_term_debt_pf_curr,
	        coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
		FROM public.credito_coleta cc
	    join (select 
	    		dp.direct_prospect_id,
	    		max(id) as max_id,
	    		max(dp.name) as razao_social
	    	from public.credito_coleta cc
		    join public.direct_prospects dp on dp.cpf = cc.documento 
		    where dp.direct_prospect_id in ()
				and cc.documento_tipo::text = 'CPF'::text 
				and cc.tipo::text = 'SCR'::text    	
			group by 1
	    	) as t1 on t1.max_id = cc.id
    	) as t2
	) as q2 on q2.direct_prospect_id = t1.lead_id 
where t1.loan_is_loan = 1 and date_trunc('year',t1.loan_contract_date)::date >= '2019-01-01'::date


---SERVIÇO MODELO
select 
	dp.direct_prospect_id ,
	(greatest(q1.consulta_financeira,q1.consulta_cobranca,0) > 0 and greatest(q1.consulta_factoring,q1.consulta_fornecedores,q1.consulta_indefinido,q1.consulta_provedor_dado,q1.consulta_seguradora,0) = 0) as new_apenas_consulta_financeira,
	case when q2.long_term_debt_pf_curr = 0 then 3
		when q2.long_term_debt_pf_curr * 1000 / dp.month_revenue >= 0.1 then 1
		when q2.long_term_debt_pf_curr * 1000 / dp.month_revenue < 0.1 then 2
	end as new_carteira_credito_cpf,
	case when (dp.mei::int = 1 or dp.name ~ '.*\sMEI$' or dp.natureza_juridica = '2135') and dp.name !~ '.*\sLTDA$ | .*\sEIRELI$ | .*\sME$' then 1 else 0 end as is_mei,
	q1.consulta_seguradora as experian_consulta_seguradoras,
	q1.consulta_financeira as experian_consulta_financeiras,
	q1.consulta_fornecedores as experian_consulta_fornecedores,
	q3.perc_faturamento_comprovado_medio as perc_bizextrato_faturamento_comprovado,
	dp.month_revenue as lead_informed_month_revenue,
	dp.age as neoway_company_age,
	(dp.vinculo in ('Administrador','Procurador') or (dp.vinculo = 'Outros' and replace(dp.utm_source,'#/','') not in (select identificacao from public.parceiros_parceiro)))::int as new_solicitante_vinculo_admin_Admin_procurador
--	(dp.vinculo in ('Administrador','Procurador') or (dp.vinculo = 'Outros' and q4.identificacao is null))::int as new_solicitante_vinculo_admin_Admin_procurador
from public.direct_prospects dp
--CONSULTAS SERASA
join(select
        r1.direct_prospect_id,
        coalesce(count(*) filter (where consulta_proprio = 1),0) as consulta_proprio,
        coalesce(count(*) filter (where consulta_factoring = 1 and consulta_proprio = 0),0) as consulta_factoring,
        coalesce(count(*) filter (where consulta_seguradora = 1 and greatest(consulta_proprio,consulta_factoring) = 0),0) as consulta_seguradora,
        coalesce(count(*) filter (where consulta_financeira = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora) = 0),0) as consulta_financeira,	
        coalesce(count(*) filter (where consulta_cobranca = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira) = 0),0) as consulta_cobranca,
        coalesce(count(*) filter (where consulta_provedor_dado = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca) = 0),0) as consulta_provedor_dado,
        coalesce(count(*) filter (where consulta_fornecedores = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado) = 0),0) as consulta_fornecedores,
        coalesce(count(*) filter (where greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado,consulta_fornecedores) = 0),0) as consulta_indefinido
	from (select
			direct_prospect_id
		from public.direct_prospects
		where direct_prospect_id = 1177903) as r1
    left join (SELECT 
            t2.direct_prospect_id,
            t2.consultas,
            data_science.consulta_serasa_dicionario_factoring(t2.consultas) AS consulta_factoring,
            data_science.consulta_serasa_dicionario_seguradora(t2.consultas) AS consulta_seguradora,
            data_science.consulta_serasa_dicionario_cobranca(t2.consultas) AS consulta_cobranca,
            data_science.consulta_serasa_dicionario_provedor_dado(t2.consultas) AS consulta_provedor_dado,
            data_science.consulta_serasa_dicionario_financeira(t2.consultas) AS consulta_financeira,
            data_science.consulta_serasa_dicionario_fornecedores(t2.consultas) AS consulta_fornecedores,
            CASE
                WHEN ("position"(t2.razao_social,t2.consultas) > 0) THEN 1
                ELSE 0
            END AS consulta_proprio
        FROM ( SELECT t1.direct_prospect_id,
                    ((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas,
                    t1.razao_social
                FROM public.credito_coleta cc
                join (select 
                        dp.direct_prospect_id,
                        max(id) as max_id,
                        max(dp.name) as razao_social
                    from public.credito_coleta cc
                    join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
                    where dp.direct_prospect_id = 1177903
                        and cc.tipo::text in ('Serasa','RelatoSocios','RelatoBasico')
                    group by 1
                    ) as t1 on t1.max_id = cc.id
            union all
                SELECT t1.direct_prospect_id,
                    ((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas,
                    t1.razao_social
                FROM public.credito_coleta cc
                join (select 
                        dp.direct_prospect_id,
                        max(id) as max_id,
                        max(dp.name) as razao_social
                    from public.credito_coleta cc
                    join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
                    where dp.direct_prospect_id = 1161928
                        and cc.tipo::text in ('Serasa','RelatoSocios','RelatoBasico')
                    group by 1
                    ) as t1 on t1.max_id = cc.id
                ) t2
        where t2.consultas != 'CREDITLOOP CORRESPONDENTE BANCARIO' and consultas != ''
        ) as r2 on r2.direct_prospect_id = r1.direct_prospect_id
    group by 1) as q1 on q1.direct_prospect_id = dp.direct_prospect_id 
--SCR PF
join(select
		direct_prospect_id,
		case when thin_file is null then long_term_debt_pf_curr else 0 end long_term_debt_pf_curr
	from(SELECT t1.direct_prospect_id,
            replace(replace(replace(cc.data -> 'historico'::text -> 'Carteira de Crédito'::text -> 0 ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS long_term_debt_pf_curr,
	        coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
		FROM public.credito_coleta cc
	    join (select 
	    		dp.direct_prospect_id,
	    		max(id) as max_id,
	    		max(dp.name) as razao_social
	    	from public.credito_coleta cc
		    join public.direct_prospects dp on dp.cpf = cc.documento 
		    where dp.direct_prospect_id = 1161928
				and cc.documento_tipo::text = 'CPF'::text 
				and cc.tipo::text = 'SCR'::text    	
			group by 1
	    	) as t1 on t1.max_id = cc.id
    	) as t2
	) as q2 on q2.direct_prospect_id = dp.direct_prospect_id 
--COMPROVACAO FATURAMENTO
left join(
	select
		q1.direct_prospect_id ,
		sum(q1.value) filter (where q1.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace','input_manual') and movimentacao = 'positivo')::numeric / 3 / greatest(max(month_revenue),1) as perc_faturamento_comprovado_medio
	from (select
			t2.direct_prospect_id ,
			t1.month_revenue, 
			t1.value,
			t1.movimentacao,
			t1.tag
		from data_science.extratos_intermediarios t1
		join data_science.v_funil_completo_sem_duplicacao t2 on t2.loan_request_id = t1.loan_request_id
		where t2.direct_prospect_id  = 1161928
		) q1
	group by 1
	) as q3 on q3.direct_prospect_id = dp.direct_prospect_id 
--PARCEIRO
--left join (select distinct identificacao from parceiros_parceiro) q4 on q4.identificacao = replace(dp.utm_source,'#/','')
	
