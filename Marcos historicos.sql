--INDICADORES DE MARCOS
select 
	count(distinct (dp.city || dp.state)) cidades_unicas,
	count(distinct dp.cnpj) as clientes,
	count(*) as originacao_units,
	sum(li.total_payment) as originacao_valor,
	to_char(min(age((lr.loan_date || case when lr.money_plus_id is not null and (lr.loan_date || ' ' || to_char((clicksign->'document'->>'updated_at')::timestamp - interval '3 hours','HH24:MI:SS'))::timestamp <= (lr.loan_date || ' 10:30:00')::timestamp then ' 12:00:00' else ' 17:00:00' end)::timestamp,dp.opt_in_date)) filter (where coalesce(dp.previous_loans_count,0) > 0),'HH24:MI') as menor_time2loan,
	to_char(min(age((lr.loan_date || case when lr.money_plus_id is not null and (lr.loan_date || ' ' || to_char((clicksign->'document'->>'updated_at')::timestamp - interval '3 hours','HH24:MI:SS'))::timestamp <= (lr.loan_date || ' 10:30:00')::timestamp then ' 12:00:00' else ' 17:00:00' end)::timestamp,dp.opt_in_date)) filter (where coalesce(dp.previous_loans_count,0) = 0),'HH24:MI') as menor_time2loan_sem_renew,
	max(mark_days.high_unit) as recorde_units_dia,
	max(mark_days.high_value) as recorde_value_dia,
	max(mark_days.loan_date) filter (where mark_days.top_units = 1) as dia_recorde_units_dia,
	max(mark_days.loan_date) filter (where mark_days.top_values = 1) as dia_recorde_value_dia,
	max(mark_month.high_unit) as recorde_units_mes,
	max(mark_month.high_value) as recorde_value_mes,
	max(mark_month.mes) filter (where mark_month.top_units = 1) as mes_recorde_units_mes,
	max(mark_month.mes) filter (where mark_month.top_values = 1) as mes_recorde_value_mes
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
join (select loan_date,
		high_unit,
		high_value,
		rank() over (order by high_unit desc) top_units,
		rank() over (order by high_value desc)	top_values
	from(select lr.loan_date,
			count(*) as high_unit,
			sum(li.total_payment) as high_value 
		from loan_requests lr 
		join loan_infos li on li.loan_info_id = lr.loan_request_id 
		where lr.status = 'ACCEPTED' 
		group by 1) as t1) as mark_days on mark_days.loan_date = lr.loan_date
join (select mes,
		high_unit,
		high_value,
		rank() over (order by high_unit desc) top_units,
		rank() over (order by high_value desc)	top_values
	from(select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as mes,
			count(*) as high_unit,
			sum(li.total_payment) as high_value 
		from loan_requests lr 
		join loan_infos li on li.loan_info_id = lr.loan_request_id 
		where lr.status = 'ACCEPTED' 
		group by 1) as t1) as mark_month on mark_month.mes = to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy')
where lr.status = 'ACCEPTED'




select 
	loan_request_id,
	length(clicksign::text)
from public.loan_requests
order by 2
limit 5

select clicksign
from public.loan_requests
where loan_request_id = 33239


select
	loan_request_id,
	(clicksign->'document'->>'updated_at')::timestamp - interval '3 hours' as update_at,
	(clicksign->'document'->>'uploaded_at')::timestamp as upload_at,
	((clicksign->'document'->>'uploaded_at')::timestamp - (clicksign->'document'->>'updated_at')::timestamp)::interval
from public.loan_requests
where status = 'ACCEPTED'

select
	loan_request_id,
	(clicksign->'document'->>'updated_at')::timestamp - interval '3 hours' as update_at,
	to_char((clicksign->'document'->>'updated_at')::timestamp - interval '3 hours','HH24:MI:SS') as update_at_hour,
	loan_date,
--	(loan_date || (clicksign->'document'->>'updated_at')::timestamp - interval '3 hours'),
	money_plus_id,
	socinal_id
from public.loan_requests
where status = 'ACCEPTED' and clicksign is not null



select
	lr.loan_request_id,
	to_char(min(age((lr.loan_date || case when lr.money_plus_id is not null and (lr.loan_date || ' ' || to_char((clicksign->'document'->>'updated_at')::timestamp,'HH24:MI:SS'))::timestamp <= (lr.loan_date || ' 10:30:00')::timestamp then ' 12:00:00' else ' 17:00:00' end)::timestamp,dp.opt_in_date)) filter (where coalesce(dp.previous_loans_count,0) > 0),'MM DD HH24:MI') as menor_time2loan,
	to_char(min(age((lr.loan_date || case when lr.money_plus_id is not null and (lr.loan_date || ' ' || to_char((clicksign->'document'->>'updated_at')::timestamp,'HH24:MI:SS'))::timestamp <= (lr.loan_date || ' 10:30:00')::timestamp then ' 12:00:00' else ' 17:00:00' end)::timestamp,dp.opt_in_date)) filter (where coalesce(dp.previous_loans_count,0) = 0),'MM DD HH24:MI') as menor_time2loan_sem_renew
from public.direct_prospects dp
join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
join public.loan_requests lr on lr.offer_id = o.offer_id
where lr.status = 'ACCEPTED'
group by 1