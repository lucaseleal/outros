select 
	rta.atual_lead_id,
	rtu.ultimo_lead_id,
	rta.atual_collection_original_p_plus_i,
	rta.atual_collection_loan_amount_paid,
	rta.atual_collection_current_ead,
	lta.lead_classe_bizu2,
	rtu.ultimo_loan_net_value,
	rta.atual_loan_net_value,
	rtu.ultimo_loan_final_net_interest_rate,
	rta.atual_loan_final_net_interest_rate,
	rtu.ultimo_loan_original_final_term,
	rta.atual_loan_original_final_term,
	lta.lead_proposal_limit,
	rta.atual_lead_application_amount,
	rta.atual_loan_contract_date,
	case 
		when rta.atual_loan_contract_date - coalesce(dt_termino_quitacao,dt_termino_normal) <= 1 then greatest(coalesce(recompra_quitacao,recompra_normal),lr.repurchase_value)
		else case when lr.repurchase_value > 0 then lr.repurchase_value else 0 end
	end recompra
from n_renewal_tape_atual rta
join n_renewal_tape_ultimo rtu on rtu.ultimo_lead_cnpj = rta.atual_lead_cnpj
join loan_tape_full lta on lta.lead_id = rta.atual_lead_id
join loan_tape_full ltu on ltu.lead_id = rtu.ultimo_lead_id
left join (select emprestimo_id, 
	sum(fp.pago) filter (where fpp."type" = 'LIQUIDATION' and fpp.status != 'Cancelado') as recompra_quitacao,
	sum(fp.pago) filter (where numero_ajustado = 1) as recompra_normal,
	max(fp.pago_em) filter (where fpp."type" = 'LIQUIDATION' and fpp.status != 'Cancelado') as dt_termino_quitacao,
	max(fp.pago_em) as dt_termino_normal	
	from public.financeiro_planopagamento fpp
	join (select fp.*,rank() over (partition by fpp.emprestimo_id order by fp.pago_em desc,fp.vencimento_em desc) as numero_ajustado
		from public.financeiro_parcela fp
		join public.financeiro_planopagamento fpp on fpp.id = fp.plano_id) fp on fp.plano_id = fpp.id
	where fpp.status = 'Pago'
	group by 1) as fppu on fppu.emprestimo_id = ltu.loan_request_id
join public.loan_requests lr on lr.loan_request_id = lta.loan_request_id


select atual_lead_id,
atual_loan_is_renewal
from n_renewal_tape_atual rta

