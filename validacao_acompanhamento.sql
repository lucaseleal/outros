select
	lr.status,
	count(*) --amgv.*
from public.loan_requests lr 
left join bizcredit_approval_tape_full batf on lr.loan_request_id = batf.loan_request_id 
--join loa_tape_full ltf on ltf.loan_request_id = batf.loan_request_id
left join data_science.acompanhamento_modelos_g1_v2 amgv on lr.loan_request_id = amgv.loan_request_id
join public.offers o on o.offer_id = lr.offer_id 
join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id 
where batf.loan_request_id is null and amgv.loan_request_id is null
group by 1

--where dp.previous_loans_count > 0 and lead_biz_bizu2_pd is null
where dp.direct_prospect_id in (select
	legacy_target_id
from public.sync_credit_analysis sca 
where (data_output -> 'CreditOpinions' -> 0 ->> 'IsAutomatic')::bool)

- emprestimos com info faltando - pedidos com erro ou que n�o saem do rascunho n�o tem questionario preenchido; b�nus = entender se pedidos que n�o pra A1 devem participar; entender como � calculado o v20; 
solu��o: vamos ter bizu2 e bizu3 (caio); 
confirmar se o v20 � igual ao v21 preliminar (lucas);
pedidos anteriores ao endpoint de checklist buscar info de problema judicial, bem_consultado (lucas);
pedidos com all_docs_date preenchido (foram A1) sem questionario precisam ser corrigidos pra equipe de dev (Castro);
corrigir motivo: deveria ser "sem questionario" pra todo mundo;

- divis�o de classes (caio)
- diversos tipos de null - caio vai fazer tratamento
NaN, BLANK, '', 0 (extrato score)

- renovacao com info faltando - s�o poucos
- aprovados bizbot sem v21 - n�o conseguir achar esses


select ltf.loan_request_id ,dp."name" 
from loan_tape_full ltf 
join public.direct_prospects dp on dp.direct_prospect_id = ltf.lead_id
where dp."name" like 'JANE%'


select 
	t3.emprestimo_id,
	max(n_planos) as n_planos
from(select 
		fpp.emprestimo_id,
		fpp.id,
		fpp.type,
		fp.id,
		row_number () over (partition by fpp.id order by fp.vencimento_em) as indice_vcto,
		row_number () over (partition by fpp.id order by fp.pago_em,fp.vencimento_em) as indice_pgto,
		t2.n_planos
	from public.financeiro_planopagamento fpp 
	join public.financeiro_parcela fp on fp.plano_id = fpp.id 
	join(select emprestimo_id,count(*) n_planos
		from public.financeiro_planopagamento
		where status not in ('Cancelado','EmEspera') and "type" != 'ORIGINAL'
		group by 1) as t2 on t2.emprestimo_id = fpp.emprestimo_id and n_planos > 1
	where fpp.status not in ('Cancelado','EmEspera') and fpp."type" != 'ORIGINAL'
		--and fpp.emprestimo_id = 6338
) as t3
where t3.indice_vcto <> indice_pgto
group by 1