--OFERTA NEGOCIADAS
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where count_neg_oferta > 0) as "Ofertas que negociaram",
	count(*) filter (where count_neg_oferta > 0 and lr.loan_request_id is not null) as "Ofertas que negociaram e fizeram pedido",
	count(*) filter (where count_neg_oferta > 0 and (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)) as "Ofertas que negociaram e foram pra alçada",
	count(*) filter (where count_neg_oferta > 0 and lr.status = 'ACCEPTED') as "Ofertas que negociaram e fecharam",
	count(*) filter (where count_neg_pedido > 0) as "Pedidos que negociaram",
	count(*) filter (where count_neg_pedido > 0 and (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)) as "Pedidos que negociaram e foram pra alçada",
	count(*) filter (where count_neg_pedido > 0 and lr.status = 'ACCEPTED') as "Pedidos que negociaram e fecharam"
from direct_prospects dp
left join clients c on c.client_id = dp.client_id
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select o.*,count_neg_oferta - 1 as count_neg_oferta from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg_oferta from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join (select lr.*,count_neg_pedido - 1 as count_neg_pedido from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id,count(*) as count_neg_pedido from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
group by 1

--O2R TICKET MEDIO
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) ofertas,
	(count(*) filter (where o.data_ultimo_contato is not null) / count(*)::float)::decimal(10,2) as oferta_com_contato,
	(count(*) filter (where lr.loan_request_id is not null) / count(*)::float)::decimal(10,2) as oferta_com_pedido,
	(count(*) filter (where o.data_ultimo_contato is not null and lr.loan_request_id is not null) / case count(*) filter (where o.data_ultimo_contato is not null) when 0 then 1 else count(*) filter (where o.data_ultimo_contato is not null) end::float)::decimal(10,2) as oferta_com_contato_com_pedido,
	(count(*) filter (where o.data_ultimo_contato is null and lr.loan_request_id is not null) / case count(*) filter (where o.data_ultimo_contato is null) when 0 then 1 else count(*) filter (where o.data_ultimo_contato is null) end::float)::decimal(10,2) as oferta_sem_contato_com_pedido,
	avg(o.max_value) filter (where o.data_ultimo_contato is not null) as ticket_medio_com_contato,
	avg(o.max_value) filter (where o.data_ultimo_contato is null) as ticket_medio_sem_contato
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where o.direct_prospect_id is not null
group by 1

where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null

--OFERTAS POR FAIXA DE VALOR DE OFERTA (10 EM 10)
WITH ranges AS (
    SELECT (ten*5000+1)::text||'-'||(ten*5000+5000)::text AS range,
		ten*5000+1 AS r_min, ten*5000+5000 AS r_max
	FROM generate_series(0,29) AS t(ten))
SELECT r.range as "Range Ticket", 
--Dezembro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Dec/18') as "Total offers Dez",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Active offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Inactive offers Dez",
--Novembro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Nov/18') as "Total offers Nov",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Active offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Inactive offers Nov",
--Outubro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Oct/18') as "Total offers Out",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Active offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Inactive offers Out"
FROM ranges r, direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (o.max_value BETWEEN r.r_min AND r.r_max)
GROUP BY r.range, r.r_min
ORDER BY r.r_min desc

--OFERTAS POR FAIXA DE VALOR DE OFERTA
SELECT case when o.max_value > 100000 then '9100'
	when o.max_value > 80000 then '81-100'
	when o.max_value > 50000 then '51-80' 
	when o.max_value > 40000 then '41-50' 
	when o.max_value > 30000 then '31-40' 
	when o.max_value > 20000 then '21-30' 
	when o.max_value > 10000 then '11-20' 
	when o.max_value > 0 then '0-10' 
	else 'Sem valor' end as "Range Ticket", 
--Dezembro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Dec/18') as "Total offers Dez",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Active offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Inactive offers Dez",
--Novembro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Nov/18') as "Total offers Nov",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Active offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Inactive offers Nov",
--Outubro
count(*) filter (where to_char(o.date_inserted,'Mon/yy') = 'Oct/18') as "Total offers Out",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Active offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Inactive offers Out"
FROM direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select o.offer_id, t1.is_renew
	from offers o 
	join (select o.client_id, o.date_inserted, count(*) filter (where o.date_inserted > t1.request_date) as is_renew
		from offers o
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = o.date_inserted) as isrenew on isrenew.offer_id = o.offer_id
where coalesce(is_renew,0) = 0
GROUP BY 1
order by 1


--O2R TAXA CONVERSÃO
SELECT case when o.max_value > 100000 then 'a) >100'
	when o.max_value > 80000 then 'b) 81-100'
	when o.max_value > 50000 then 'c) 51-80' 
	when o.max_value > 40000 then 'd) 41-50' 
	when o.max_value > 30000 then 'e) 31-40' 
	when o.max_value > 20000 then 'f) 21-30' 
	when o.max_value > 10000 then 'g) 11-20' 
	when o.max_value > 0 then 'h) 0-10' 
	else 'i) Sem valor' end as "Range Ticket", 
--Outubro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Active offers Out",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18' and lr.loan_request_id is not null) "Active req offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Inactive offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18' and lr.loan_request_id is not null) "Inactive req offers Out",
--Novembro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Active offers Nov",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18' and lr.loan_request_id is not null) "Active req offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Inactive offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18' and lr.loan_request_id is not null) "Inactive req offers Nov",
--Dezembro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Active offers Dez",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18' and lr.loan_request_id is not null) "Active req offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Inactive offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18' and lr.loan_request_id is not null) "Inactive req offers Dez",
--Janeiro 2019
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19') "Active offers Jan",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19' and lr.loan_request_id is not null) "Active req offers Jan",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19') "Inactive offers Jan",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19' and lr.loan_request_id is not null) "Inactive req offers Jan"
FROM direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select o.*,count_neg_oferta - 1 as count_neg_oferta from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg_oferta from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join (select lr.*,count_neg_pedido - 1 as count_neg_pedido from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id,count(*) as count_neg_pedido from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select o.offer_id, t1.is_renew
	from offers o 
	join (select o.client_id, o.date_inserted, count(*) filter (where o.date_inserted > t1.request_date) as is_renew
		from offers o
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = o.date_inserted) as isrenew on isrenew.offer_id = o.offer_id
where coalesce(is_renew,0) = 0 and count_neg_oferta = 0
GROUP BY 1
order by 1

--R2AD POR FAIXA DE VALOR PEDIDO
SELECT case when o.max_value > 100000 then 'a) >100'
	when o.max_value > 80000 then 'b) 81-100'
	when o.max_value > 50000 then 'c) 51-80' 
	when o.max_value > 40000 then 'd) 41-50' 
	when o.max_value > 30000 then 'e) 31-40' 
	when o.max_value > 20000 then 'f) 21-30' 
	when o.max_value > 10000 then 'g) 11-20' 
	when o.max_value > 0 then 'h) 0-10' 
	else 'i) Sem valor' end as "Range Ticket", 
--Outubro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Active offers Out",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18' and lr.loan_request_id is not null) "Active req offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18') "Inactive offers Out",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Oct/18' and lr.loan_request_id is not null) "Inactive req offers Out",
--Novembro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Active offers Nov",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18' and lr.loan_request_id is not null) "Active req offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18') "Inactive offers Nov",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Nov/18' and lr.loan_request_id is not null) "Inactive req offers Nov",
--Dezembro 2018
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Active offers Dez",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18' and lr.loan_request_id is not null) "Active req offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18') "Inactive offers Dez",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Dec/18' and lr.loan_request_id is not null) "Inactive req offers Dez",
--Janeiro 2019
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19') "Active offers Jan",
count(*) filter (where o.data_ultimo_contato is not null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19' and lr.loan_request_id is not null) "Active req offers Jan",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19') "Inactive offers Jan",
count(*) filter (where o.data_ultimo_contato is null and to_char(o.date_inserted,'Mon/yy') = 'Jan/19' and lr.loan_request_id is not null) "Inactive req offers Jan"
FROM direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select o.*,count_neg_oferta - 1 as count_neg_oferta from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg_oferta from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join (select lr.*,count_neg_pedido - 1 as count_neg_pedido from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id,count(*) as count_neg_pedido from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select o.offer_id, t1.is_renew
	from offers o 
	join (select o.client_id, o.date_inserted, count(*) filter (where o.date_inserted > t1.request_date) as is_renew
		from offers o
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = o.date_inserted) as isrenew on isrenew.offer_id = o.offer_id
where coalesce(is_renew,0) = 0 and count_neg_oferta = 0
GROUP BY 1
order by 1

--METAS IS	
select to_date(to_char(o.date_inserted,'Mon/yy'),'Mon/yy'),
case when o.max_value > 100000 then 'a) >100'
	when o.max_value > 80000 then 'b) 81-100'
	when o.max_value > 50000 then 'c) 51-80' 
	when o.max_value > 40000 then 'd) 41-50' 
	when o.max_value > 30000 then 'e) 31-40' 
	when o.max_value > 20000 then 'f) 21-30' 
	when o.max_value > 10000 then 'g) 11-20' 
	when o.max_value > 0 then 'h) 0-10' else 'i) Sem valor' end as "Range Ticket",
	replace(o.follow_up,'Carlos Saraiva','Cadu Saraiva') as "Analista IS",
	count(*) as "Numero de ofertas mensal",
	count(*) filter (where lr.loan_request_id is not null) as "Numero de pedidos mensal",
	count(*) filter (where lr.loan_request_id is not null) / case count(*) when 0 then null else count(*) end::float as "% O2R"
	FROM direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select o.*,count_neg_oferta - 1 as count_neg_oferta from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg_oferta from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join (select lr.*,count_neg_pedido - 1 as count_neg_pedido from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id,count(*) as count_neg_pedido from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
group by 1,2
order by 1 desc


--VERIFICANDO SINTONIA ENTRE ALL DOCS ORIGINAL E ALL DOCS USANDO A TABELA DE LOG
--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','Cancelado') or ad.loan_request_id is not null or (lr.status = 'REJECTED' and analista_responsavel not in ('Felipe Viana', 'Bruno Lourenço','Rodrigo Avila')))  as AllDocs_original_count,
	sum(coalesce(lr.valor_solicitado,lr.value)) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','Cancelado') or ad.loan_request_id is not null or (lr.status = 'REJECTED' and analista_responsavel not in ('Felipe Viana', 'Bruno Lourenço','Rodrigo Avila'))) as AllDocs_original_sum,
	count(*) filter (where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null))  as AllDocs_original_count,
	sum(coalesce(lr.valor_solicitado,lr.value)) filter (where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)) as AllDocs_original_sum
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
group by 1
order by 1 desc

